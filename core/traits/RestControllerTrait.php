<?php


namespace core\traits;


use Yii;
use yii\web\BadRequestHttpException;

trait RestControllerTrait
{
    protected $filterMethods = ['POST', 'PATCH', 'PUT'];
    protected $excludedActions = [];

    private function needBody() {
        if (empty(Yii::$app->request->bodyParams)) {
            throw new BadRequestHttpException('Request body cannot be empty');
        }
    }

    public function beforeAction($action)
    {
        $method = Yii::$app->request->method;
        if (in_array($method, $this->filterMethods)) {
            if (!in_array($action->id, $this->excludedActions)) {
                $this->needBody();
            }
        }
        return parent::beforeAction($action);
    }
}