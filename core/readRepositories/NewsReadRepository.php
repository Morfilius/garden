<?php


namespace core\readRepositories;


use core\entities\News;
use DateTime;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;

class NewsReadRepository
{
    public function showAllProvider() : DataProviderInterface
    {
        return $this->getProvider(News::find() ->andWhere(['<=', 'date_start', (new DateTime())->format('Y-m-d')])->andWhere(['>=', 'date_end', (new DateTime())->format('Y-m-d')]));
    }

    private function getProvider($query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 20,
            ]
        ]);
    }
}