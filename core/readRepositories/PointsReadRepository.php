<?php


namespace core\readRepositories;


use core\entities\Catalog\Catalog;
use core\entities\Catalog\Category;
use core\entities\Land\Garden\GardenInfo;
use core\entities\Land\Points;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class PointsReadRepository
{

    public function getCategoryIdsByRecNotNull($land_map_id)
    {
        $points = (new Query())->select('id')->from(Points::tableName())->where(['lands_map_id' => $land_map_id]);
        $catalog_ids =  (new Query())->select('catalog_id')->from(GardenInfo::tableName())
            ->where(['in', 'land_points_id', $points])->andWhere(['not',['text' => '']]);
        return ArrayHelper::getColumn((new Query())->select('category_id')->from(Catalog::tableName())
            ->where(['in', 'id', $catalog_ids])->groupBy('category_id')->all(), 'category_id');
    }

    public function getCategoryIds($land_map_id)
    {
        $points = (new Query())->select('id')->from(Points::tableName())->where(['lands_map_id' => $land_map_id]);
        $catalog_ids =  (new Query())->select('catalog_id')->from(GardenInfo::tableName())
            ->where(['in', 'land_points_id', $points]);
        return ArrayHelper::getColumn((new Query())->select('category_id')->from(Catalog::tableName())
            ->where(['in', 'id', $catalog_ids])->groupBy('category_id')->all(), 'category_id');
    }
    
    public function showList($landsMapId) :?array
    {
        return $this->_showList($landsMapId);
    }

    public function showListNeedLink($landsMapId, $checked = false)
    {
        return $this->_showList($landsMapId, true, true, $checked);
    }

    public function showInfo($id) : ?GardenInfo
    {
        return GardenInfo::findOne(['land_points_id' => $id]);
    }

    public function showListWithClass($landsMapId)
    {
       return Points::find()->with('garden')->with('info')->with('catalog')->andWhere(['lands_map_id' => $landsMapId])->all();
    }

    public function showListMapCat($mapId, $categoryId)
    {
        $plantsIdsByCategory = Catalog::find()->where(['category_id' => $categoryId])->select('id')->column();
        $pointIdsByMap = Points::find()->where(['lands_map_id' => $mapId])->select('id')->column();
        if (empty($plantsIdsByCategory) || empty($pointIdsByMap)) {
            return [];
        }
        $pointsIds = GardenInfo::find()->where(['catalog_id' => $plantsIdsByCategory])->andWhere(['land_points_id' => $pointIdsByMap])->select('land_points_id')->column();
        if (empty($pointsIds)) {
            return [];
        }
        return Points::find()->with('catalog')->where(['in', 'id', $pointsIds])->all();
    }

    /**
     * @param $landsMapId
     * @param bool $disable
     * @param bool $link
     * @return string
     */

    protected function _showList($landsMapId, $disable = false, $link = false, $checked = false) :?array
    {
        return array_map(function (Points $point) use ($disable, $link, $checked) {
            return array_filter([
                'id' => $point->id,
                'garden_id' => $point->garden_id,
                'radius' => $point->radius,
                'xPx' => $point->xPx,
                'yPx' => $point->yPx,
                'xPerc' => $point->xPerc,
                'yPerc' => $point->yPerc,
                'image' => Yii::getAlias('@static/gardens/' . $point->garden->image),
                'disable' => $disable,
                'notFilled' => ($disable && !$point->isAssignCatItem()),
                'checked' => ($point->id == $checked),
                'link' => $link
            ]);
        }, Points::find()->with('garden')->andWhere(['lands_map_id' => $landsMapId])->all());
    }
}