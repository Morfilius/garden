<?php


namespace core\readRepositories;


use core\entities\Catalog\Category;

class CategoryReadRepository
{
    public function showByIds(array $ids)
    {
        return Category::find()->where(['in', 'id', $ids])->all();
    }
}