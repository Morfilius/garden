<?php


namespace core\readRepositories;


use core\entities\Notification\Notification;
use yii\data\ActiveDataProvider;

class NotificationReadRepository
{
    public function showUnreadListLimit($userId, $limit = 3)
    {
        return Notification::find()->where(['receiver' => $userId])->andWhere(['read' => Notification::UNREAD])->limit(3)->all();
    }

    public function showUnreadListProvider($userId)
    {
        return $this->getProvider(Notification::find()->where(['receiver' => $userId])->andWhere(['read' => Notification::UNREAD]));
    }

    public function showReadListProvider($userId)
    {
        return $this->getProvider(Notification::find()->where(['receiver' => $userId])->andWhere(['read' => Notification::READ]));
    }

    private function getProvider($query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 20,
            ]
        ]);
    }
}