<?php


namespace core\readRepositories;


use core\entities\Land\Land;
use yii\web\NotFoundHttpException;

class LandReadRepository
{
    public function showByUser($user_id) : ?Land
    {
        return Land::findOne(['owner_id' => $user_id]);
    }

    public function getByUser($user_id) : ?Land
    {
        if ( ! $land = $this->showByUser($user_id)) {
            throw new NotFoundHttpException('Land not found');
        }
        return $land;
    }
}