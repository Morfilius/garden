<?php


namespace core\readRepositories;



use core\entities\Land\Garden\Garden;

class GardenReadRepository
{
    /** return Garden[][]*/
    public function showList() : array
    {
        return array_reduce(Garden::find()->all(), function ($carry, Garden $item) {
                $carry[$item->category_slug][] = $item;
                return $carry;
            }, []);
    }
}