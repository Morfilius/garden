<?php


namespace core\readRepositories;


use core\entities\Dispatch;
use DateTime;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;

class DispatchReadRepository
{
    public function showAllProvider($userId) : DataProviderInterface
    {
        return $this->getProvider(Dispatch::find()->where(['recipient_id' => $userId])
            ->andWhere(['<=', 'date_start', (new DateTime())->format('Y-m-d')])->andWhere(['>=', 'date_end', (new DateTime())->format('Y-m-d')]));
    }

    private function getProvider($query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 20,
            ]
        ]);
    }
}