<?php


namespace core\helpers;


use Imagine\Image\ManipulatorInterface;
use yii\imagine\Image;

class ImageHelper
{
    public static function resizeImage($path, $newPath, $width = 100, $height = 100, $quality = 90, $mode = ManipulatorInterface::THUMBNAIL_OUTBOUND)
    {
        if (!$width || !$height) {
            $image = Image::getImagine()->open($path);
            $ratio = $image->getSize()->getWidth() / $image->getSize()->getHeight();
            if ($width) {
                $height = ceil($width / $ratio);
            } else {
                $width = ceil($height * $ratio);
            }
        }

        // Fix error "PHP GD Allowed memory size exhausted".
        ini_set('memory_limit', '512M');
        Image::thumbnail($path, $width, $height, $mode)->save($newPath, ['quality' => $quality]);
        return $newPath;
    }
}