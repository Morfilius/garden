<?php

namespace core\repositories\User;

use core\entities\Land\Land;
use core\entities\User\User;
use yii\web\NotFoundHttpException;

class UserRepository
{
    public function get($id): User
    {
        return $this->getBy(['id' => $id]);
    }

    public function getByBitrixId($id): User
    {
        return $this->getBy(['bitrix_id' => $id]);
    }

    public function getByUsername($username): User
    {
        return $this->getBy(['username' => $username]);
    }

    public function getByCalendarID($calendar_id): User
    {
        $userId = Land::find()->where(['calendar_id' => $calendar_id])->select('owner_id')->column();
        return  User::findOne($userId);
    }

    public function save(User $user): void
    {
        if (!$user->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(User $user): void
    {
        if (!$user->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): User
    {
        if (!$user = User::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('User not found.');
        }
        return $user;
    }
}