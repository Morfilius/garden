<?php

namespace core\repositories\User;

use core\entities\User\Bitrix24\EmployeeStorage;
use core\entities\User\Bitrix24\Profile;

class ProfileRepository
{
    public function get($id): Profile
    {
        return (new Profile())->get($id);
    }

    public function getOperator($id): Profile
    {
        return (new Profile())->setStorage(new EmployeeStorage())->get($id);
    }

    public function getByUserId($id): Profile
    {
        return (new Profile())->get($id);
    }

    public function save(Profile $profile): void
    {
        if (!$profile->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Profile $profile): void
    {
        if (!$profile->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}