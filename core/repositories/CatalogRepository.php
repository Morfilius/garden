<?php

namespace core\repositories;

use core\entities\Catalog\Catalog;
use yii\web\NotFoundHttpException;

class CatalogRepository
{
    public function get($id): Catalog
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(Catalog $catalog): void
    {
        if (!$catalog->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Catalog $catalog): void
    {
        if (!$catalog->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): Catalog
    {
        if (!$catalog = Catalog::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('Catalog not found.');
        }
        return $catalog;
    }
}