<?php

namespace core\repositories;

use core\entities\Dispatch;
use yii\web\NotFoundHttpException;

class DispatchRepository
{
    public function get($id): Dispatch
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(Dispatch $dispatch): void
    {
        if (!$dispatch->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Dispatch $dispatch): void
    {
        if (!$dispatch->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): Dispatch
    {
        if (!$dispatch = Dispatch::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('Dispatch not found.');
        }
        return $dispatch;
    }
}