<?php

namespace core\repositories;

use core\entities\Chat\Chat;
use core\entities\Chat\Message;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ChatRepository
{
    public function get($id): Chat
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(Chat $chat): void
    {
        if (!$chat->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function saveMessage(Message $message): void
    {
        if (!$message->save()) {
            throw new \RuntimeException('Saving message error.');
        }
    }

    public function remove(Chat $chat): void
    {
        if (!$chat->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    public function find($user_id, $type)
    {
        return Chat::find()->andWhere(['user_id' => $user_id])->andWhere(['type' => $type])->one();
    }

    public function getList($user_id)
    {
        return Chat::find()->andWhere(['user_id' => $user_id])->all();
    }

    public function showMessages($chat_id) {
        return $this->get($chat_id)->message;
    }

    public function showMessagesProvider($chat_id) {
        return new ActiveDataProvider([
            'query' => Message::find()->where(['chat_id' => $chat_id]),
            'pagination' => [
                'defaultPageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
    }

    private function getBy(array $condition): Chat
    {
        if (!$chat = Chat::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('Chat not found.');
        }
        return $chat;
    }
}