<?php

namespace core\repositories;

use core\entities\News;
use yii\web\NotFoundHttpException;

class NewsRepository
{
    public function get($id): News
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(News $news): void
    {
        if (!$news->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(News $news): void
    {
        if (!$news->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): News
    {
        if (!$news = News::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('News not found.');
        }
        return $news;
    }
}