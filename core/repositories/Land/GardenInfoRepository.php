<?php

namespace core\repositories\Land;

use core\entities\Land\Garden\GardenInfo;
use yii\web\NotFoundHttpException;

class GardenInfoRepository
{
    public function get($id): GardenInfo
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(GardenInfo $gardenInfo): void
    {
        if (!$gardenInfo->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(GardenInfo $gardenInfo): void
    {
        if (!$gardenInfo->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): GardenInfo
    {
        if (!$gardenInfo = GardenInfo::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('GardenInfo not found.');
        }
        return $gardenInfo;
    }
}