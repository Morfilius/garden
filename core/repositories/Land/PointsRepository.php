<?php

namespace core\repositories\Land;

use core\entities\Land\Points;
use yii\web\NotFoundHttpException;

class PointsRepository
{
    public function get($id): Points
    {
        return $this->getBy(['id' => $id]);
    }
    
    public function save(Points $point): void
    {
        if (!$point->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Points $point): void
    {
        if (!$point->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): Points
    {
        if (!$point = Points::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('Points not found.');
        }
        return $point;
    }
}