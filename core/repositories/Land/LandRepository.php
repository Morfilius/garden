<?php

namespace core\repositories\Land;

use core\entities\Land\Land;
use core\entities\Land\LandMap;
use yii\web\NotFoundHttpException;

class LandRepository
{
    public function get($id): Land
    {
        return $this->getBy(['id' => $id]);
    }

    public function getByUser($id): Land
    {
        return $this->getBy(['owner_id' => $id]);
    }

    public function getMap($id): LandMap
    {
        if(!$map = $this->getBy(['id' => $id])->map) {
            throw new NotFoundHttpException('Land map not found.');
        }
        return $map;
    }

    public function save(Land $land): void
    {
        if (!$land->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function saveMap(Land $land): void
    {
        if (!$land->map->save()) {
            throw new \RuntimeException('Saving map error.');
        }
    }

    public function remove(Land $land): void
    {
        if (!$land->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    private function getBy(array $condition): Land
    {
        if (!$land = Land::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('Land not found.');
        }
        return $land;
    }
}