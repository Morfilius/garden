<?php

namespace core\services\Firebase;

use core\entities\User\Token;

class Firebase
{
    private $apiKey;
    private $url = 'https://fcm.googleapis.com/fcm/send';

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public static function getInstance() : self
    {
        return \Yii::$container->get(self::class);
    }

    public function send($tokenIds, $notification, $topics = false, array $data = [], array $links = [])
    {
        $fields = $this->prepareDataToJson($tokenIds, $notification, $topics, $data, $links);
        $requestHeaders = [
            'Content-Type: application/json',
            'Authorization: key=' . $this->apiKey,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);
    }

    public static function sendByUsersIds($userIds, $notification, array $data = [], array $links = [])
    {
        $deviceIds = [];
        if (!empty($userIds)) {
            $tokenRows = Token::find() ->where(['in', 'user_id', $userIds])->all();
            if (!empty($tokenRows) && is_array($tokenRows)) {
                foreach ($tokenRows as $tokenRow) {
                    $deviceIds[$tokenRow->user_id][] = $tokenRow->device_id;
                }
            }
            if (!empty($deviceIds) && is_array($deviceIds)) {
                foreach ($deviceIds as $deviceId) {
                    self::getInstance()->send($deviceId, $notification, $data, $links);
                }
            }
        }
    }

    public static function sendByAll($to, $notification, array $data = [], array $links = [])
    {
        if(!isset($data['isShow'])) {
            $data['isShow'] = false;
        }
        self::getInstance()->send([], $notification, $to, $data, $links);
    }

    private function prepareDataToJson(array $tokenIds, $notification, $topics = false, array $data = [], array $links = [])
    {
        if (is_array($notification)) {
            $notification['sound'] = 1;
        } else {
            $notification = ['body' => $notification, 'sound' => 1];
        }

        $requestBody = [
            'notification' => $notification,
        ];
        if (!$topics) {
            $requestBody['registration_ids'] = $tokenIds;
        } else {
            $requestBody['condition'] = $topics;
        }
        if (!empty($data) && is_array($data)) {
            $requestBody['data'] = $data;
        }
        if (!empty($links)) {
            $requestBody['data'] = $links;
        }
        return json_encode($requestBody,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
}