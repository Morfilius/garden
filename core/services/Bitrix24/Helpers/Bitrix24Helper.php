<?php


namespace core\services\Bitrix24\Helpers;


use core\helpers\ImageHelper;

class Bitrix24Helper
{
    public static function formatImg($file, $uniqueId, $savePath, $width = 100, $height = 100)
    {
        if (empty($file) || !is_file($file->tempName)) return false;
        $image = ImageHelper::resizeImage($file->tempName, $savePath, $width, $height);
        $encodingFile = base64_encode(file_get_contents($image));
        if (is_file($savePath)) {
            unlink($savePath);
        }
        return ["fileData" => [$uniqueId.".jpg", $encodingFile]];
    }

    /**
     * @param $url
     * @return string base64 image
     */
    public static function base64ResponseImg($url)
    {
        return "data:image/jpg;base64," . base64_encode(file_get_contents($url));
    }
}