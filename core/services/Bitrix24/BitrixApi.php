<?php


namespace core\services\Bitrix24;


use core\services\Bitrix24\Config\BitrixConfigInterface;
use core\services\Bitrix24\Config\RestConfig;
use DomainException;
use Yii;

class BitrixApi
{
    protected $filePath = __DIR__ . '/' . 'settings.json';

    protected $url;
    protected $password;

    protected $query = [];
    protected $settings = [];

    const BATCH_COUNT    = 50;
    const TYPE_TRANSPORT = 'json';
    /**
     * @var string rest|webhook
     */
    private $method = 'webhook';
    /**
     * @var BitrixConfigInterface
     */
    private $config;

    /*todo нужно переделать класс по разные клиенты rest|webhook*/
    public function __construct(RestConfig $config)
    {
        if($config->method() === 'rest') {
            $this->setRestParams();
        }

        $this->config = $config;
    }

    public static function call($endpoint, array $query = [], array $select = [])
    {
        $result = (Yii::createObject(self::class))->setQuery($query)->setSelectFields($select)->send($endpoint);
        if (isset($result['result'])) {
            return $result['result'];
        }
        return [];
    }

    public static function callBatch(array $query, $halt = 0)
    {
        $arResult = [];
        if (is_array($query)) {
            $arDataRest = []; $i = 0;
            foreach($query as $key => $data) {
                if(! empty($data[ 'method' ])) {
                $i++; if(static::BATCH_COUNT >= $i) {
                        $arDataRest[ 'cmd' ][ $key ] = $data[ 'method' ];
                        if(!empty($data[ 'params' ])) {
                            $arDataRest[ 'cmd' ][ $key ] .= '?' . http_build_query($data[ 'params' ]);
                        }
                    }
                }
            }
            if(! empty($arDataRest)) {
                $arDataRest['halt'] = $halt;
                $arPost = [
                    'method' => 'batch',
                    'params' => $arDataRest
                ];
                $arResult = (Yii::createObject(self::class))->setQuery($arPost['params'])->send('batch');
            }
        }
        return $arResult;
    }
    
    public static function getClientUrl()
    {
        return Yii::$container->get(self::class)->getOriginalUrl();
    }

    public function getOriginalUrl()
    {
        preg_match('#https?://.*?/#',$this->url, $url);
        return $url[0] ?? $this->url;
    }
    
    public function send($endpoint)
    {
        return $this->callCurl($endpoint);
    }

    public function setSelectFields(array $select)
    {
        if (!empty($select)) {
            $this->query['select'] = $select;
        }
        return $this;
    }


    public function refreshToken()
    {
        $url = 'https://oauth.bitrix.info/oauth/token/?' . http_build_query([
            'grant_type' => 'refresh_token',
            'client_id' => $this->config->getClientId(),
            'client_secret' => $this->config->getClientSecret(),
            'refresh_token' => $this->settings['refresh_token'],
        ]);
        $this->setSettingsFile(file_get_contents($url));
    }

    private function setSettingsFile($json)
    {
        file_put_contents($this->filePath, $json);
    }

    public function setQuery(array $query)
    {
        if (!empty($query)) {
            $this->query = array_merge($this->query, $query);
        }
        return $this;
    }

    protected function callCurl($endpoint)
    {
        $queryUrl = $this->getUrl($endpoint);
        curl_setopt_array($curl = curl_init(), [
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => http_build_query($this->query),
        ]);
        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result, 1);
    }

    protected function setAuthKey()
    {
        $this->query['auth'] = $this->settings['access_token'];
        return $this;
    }

    protected function setUrl()
    {
        $this->url = $this->settings['client_endpoint'];
    }

    protected function setRestParams()
    {
        $this->method = 'rest';
        $this->setSettings();
        $this->setUrl();
        $this->setAuthKey();
    }

    protected function setSettings()
    {
        if (!is_file($this->filePath)) throw new DomainException('Settings file not found.');
        $fileContent = file_get_contents($this->filePath);
        if (empty($fileContent)) throw new DomainException('Settings file is empty.');
        $data = json_decode($fileContent, true);
        if (empty($data) || !is_array($data)) throw new DomainException('Settings file is empty.');

        return $this->settings = $data;
    }

    protected function getUrl($endpoint)
    {
        if ($this->isRest()) {
            return $this->url . $endpoint;
        } else {
            return $this->url . $this->password . '/' . $endpoint;
        }
    }

    protected function isRest()
    {
        return ($this->method === 'rest');
    }
}
//https://oauth.bitrix.info/oauth/token/grant_type=refresh_token&client_id=local.5ea578456cd836.41828512&client_secret=yQfBh5TGyYXYaL9e6VM8POfrqlMB6LjOEh6xGiPCGI4UYTMVry&refresh_token=6384d35e0046d7700046d6e4000000010000032b9dc0f02a8ee315f70f059defdcacda
//https://oauth.bitrix.info/oauth/token/?grant_type=refresh_token&client_id=local.5ea578456cd836.41828512&client_secret=yQfBh5TGyYXYaL9e6VM8POfrqlMB6LjOEh6xGiPCGI4UYTMVry&refresh_token=2a72d35e0046d7700046d6e400000001000003efcceffc3ea50e9b868648ee2abebac3