<?php


namespace core\services\Bitrix24\Config;


class RestConfig implements BitrixConfigInterface
{
    private $client_id;
    private $client_secret;

    public function method()
    {
        return 'rest';
    }

    public function __construct($client_id, $client_secret)
    {

        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->client_secret;
    }
}