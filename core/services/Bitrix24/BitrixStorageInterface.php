<?php


namespace core\services\Bitrix24;


interface BitrixStorageInterface
{
    public function showById($id);

    public function showList(array $filter = null);

    public function insert(array $array);

    public function update(array $array);
}