<?php
namespace core\forms\auth\Rest;

use yii\base\Model;

class LoginForm extends Model
{
    public $username;
    public $password;
    public $device_id;

    public function rules()
    {
        return [
            [['username', 'password', 'device_id'], 'required']
        ];
    }
}
