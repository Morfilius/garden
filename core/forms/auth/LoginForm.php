<?php
namespace core\forms\auth;

use core\readRepositories\UserReadRepository;
use Yii;
use yii\base\Model;

/**
 * @property UserReadRepository $readRepository
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $readRepository;

    public function __construct($config = [])
    {

        $this->readRepository = Yii::createObject('core\readRepositories\UserReadRepository');
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль'
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->readRepository->findActiveByUsername($this->username);
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильный логин или пароль.');
            }
        }
    }
}
