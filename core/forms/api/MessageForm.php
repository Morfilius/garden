<?php


namespace core\forms\api;


use core\behaviors\UploadedBase64;
use yii\base\Model;

class MessageForm extends Model
{
    public $message;
    public $images;

    public function rules()
    {
        return [
            ['message', 'string'],
            ['images', 'safe'],
        ];
    }

    public function beforeValidate()
    {
        $this->images = UploadedBase64::getInstances($this, 'images');
        return parent::beforeValidate();
    }
}