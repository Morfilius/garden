<?php


namespace core\forms\api;


use core\behaviors\UploadedBase64;
use yii\base\Model;

class CatalogForm extends Model
{
    public $product_id;
    public $image;
    public $title;
    public $category;
    public $features;
    public $relations;

    public function rules()
    {
        return [
            [['product_id', 'title', 'category'], 'required'],
            ['product_id', 'integer'],
            [['title', 'category'], 'string'],
            ['image', 'safe'],
            ['relations', 'each', 'rule' => ['integer']],
            ['features', 'each', 'rule' => ['each', 'rule' => ['string']]],
        ];
    }

    public function beforeValidate()
    {
        $this->image = UploadedBase64::getInstance($this, 'image');
        return parent::beforeValidate();
    }
}