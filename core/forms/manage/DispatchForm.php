<?php

namespace core\forms\manage;

use core\entities\Dispatch;
use core\entities\User\User;
use core\forms\CompositeForm;
use core\forms\manage\Image\ImageRequiredForm;
use core\forms\manage\Image\ImageWideForm;
use DateTime;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * @property integer $image_id
 * @property integer $recipient_id
 * @property string $title
 * @property string $date_start
 * @property string $date_end
 * @property string $text
 *
 * @property ImageRequiredForm $image
 * @property ImageWideForm $image_wide
 *
 */

class DispatchForm extends CompositeForm
{
    public $title;
    public $date_start;
    public $date_end;
    public $recipient_id;
    public $text;
    public $banner_show;
    public $banner_title;

    public function __construct(Dispatch $news = null, $config = [])
    {
        if ($news) {
            $this->load($news->getAttributes(), '');
        }
        $this->image = new ImageRequiredForm();
        $this->image_wide = new ImageWideForm();
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title', 'date_start', 'date_end', 'text', 'recipient_id'], 'required'],
            [['date_start', 'date_end'], 'match', 'pattern' => '/\d{2}.\d{2}.\d{4}/'],
            [['date_start', 'date_end'], 'string', 'max' => 10],
            [['title', 'banner_title'], 'string', 'max' => 255],
            ['text', 'string', 'max' => 30000],
            [['recipient_id', 'banner_show'], 'integer'],
            ['date_start', function ($attr, $param) {
                $date_start = new DateTime($this->date_start);
                $date_end = new DateTime($this->date_end);
                if ($date_start > $date_end) {
                    $this->addError('date_start', 'Дата опубликования не может быть больше даты окончания.');
                }
            }]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок рассылки',
            'date_start' => 'Дата опубликования',
            'date_end' => 'Дата окончания',
            'text' => 'Текст рассылки',
            'recipient_id' => 'Получатель',
            'banner_title' => 'Заголовок баннера',
        ];
    }

    public function beforeValidate()
    {
        if (!strlen(strip_tags($this->text))) {
            $this->text = '';
        }
        $banner_show = isset(Yii::$app->request->post($this->formName())['banner_show']);
        if ( ! $banner_show){
            $this->banner_show = 0;
        }
        return parent::beforeValidate();
    }

    protected function internalForms(): array
    {
        return  ['image', 'image_wide'];
    }

    public function recipientList()
    {
        return ArrayHelper::map(User::find()->alias('u')->with('profile')->leftJoin(Yii::$app->db->tablePrefix.'auth_assignment as role', 'role.user_id = u.id')
            ->andWhere(['role.user_id' => null])->all(),'id',function ($item) {
            return $item->profile->last_name.' '.$item->profile->first_name;
        });
    }
}