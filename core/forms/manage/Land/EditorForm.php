<?php

namespace core\forms\manage\Land;

use core\behaviors\UploadedBase64;
use yii\base\Model;
use yii\web\UploadedFile;


class EditorForm extends Model
{
    public $image;

    public function rules()
    {
        return [
            ['image', 'required'],
            ['image', 'safe']
        ];
    }

    public function beforeValidate()
    {
        $this->image = UploadedBase64::getInstance($this, 'image');
        return parent::beforeValidate();
    }
}