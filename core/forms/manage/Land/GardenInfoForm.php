<?php


namespace core\forms\manage\Land;


use core\entities\Land\Garden\GardenInfo;
use DateTime;
use yii\base\Model;

class GardenInfoForm extends Model
{
    public $catalog_id;
    public $landing_date;
    public $land_points_id;
    public $text;

    private $_model;

    public function __construct(GardenInfo $gardenInfo = null, $config = [])
    {
        if ($gardenInfo) {
            $this->load($gardenInfo->getAttributes(), '');
            $this->_model =  $gardenInfo;
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['catalog_id', 'land_points_id'], 'integer'],
            ['landing_date', 'match', 'pattern' => '/\d{2}.\d{2}.\d{4}/'],
            ['landing_date', 'string', 'max' => 10],
            ['text', 'string', 'max' => 30000]
        ];
    }

    public function attributeLabels()
    {
        return [
            'landing_date' => 'Дата посадки',
            'text' => 'Персональные рекомендации по уходу'
        ];
    }

    public function afterValidate()
    {
        $this->landing_date = (new DateTime($this->landing_date))->format('Y-m-d');
        parent::afterValidate();
    }
}