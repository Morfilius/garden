<?php

namespace core\forms\manage\Land;

use core\entities\Land\Land;
use core\entities\User\User;
use DateTime;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;


/**
 * @property string $title
 * @property string $address
 * @property string $date_last
 * @property string $date_next
 * @property int $employee_id
 * @property int $owner_id
 * @property int $land_area
 * @property int $lawn_area
 * @property int $deciduous_count
 * @property int $pine_count
 * @property int $garden_count
 * @property int $age
 * @property string $audit
 * @property string $rec
 */

class LandForm extends Model
{
    public $title;
    public $address;
    public $employee_id;
    public $owner_id;
    public $land_area;
    public $lawn_area;
    public $deciduous_count;
    public $pine_count;
    public $garden_count;
    public $age;
    public $audit;
    public $rec;
    /**
     * @var Land
     */
    private $land;


    public function __construct(Land $land = null, $config = [])
    {
        if ($land) {
            $this->load($land->getAttributes(), '');
        }
        $this->land = $land;

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title', 'owner_id', 'employee_id', 'address'], 'required'],
            [['title', 'address'], 'string', 'max' => 255],
            [['audit', 'rec'], 'string', 'max' => 30000],
            [['employee_id', 'owner_id', 'land_area', 'lawn_area', 'deciduous_count', 'pine_count', 'garden_count', 'age'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название участка',
            'address' => 'Адрес участка',
            'date_last' => 'Дата последнего визита бригады',
            'date_next' => 'Дата ближайшего визита бригады',
            'employee_id' => 'Бригадир',
            'owner_id' => 'Владелец',
            'land_area' => 'Площадь участка (соток)',
            'lawn_area' => 'Площадь газона (соток)',
            'deciduous_count' => 'Количество лиственных',
            'pine_count' => 'Количество хвойных',
            'garden_count' => 'Количество растений',
            'age' => 'Возраст сада (лет)',
            'audit' => 'Результат аудита участка',
            'rec' => 'Общие рекомендации к участку'
        ];
    }

    public function getOriginal()
    {
        return $this->land;
    }
}