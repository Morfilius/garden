<?php

namespace core\forms\manage;

use core\entities\Banners;
use core\entities\News;
use core\forms\CompositeForm;
use core\forms\manage\Image\ImageRequiredForm;
use DateTime;


/**
 * @property integer $image_id
 * @property string $title
 * @property string $date_start
 * @property string $date_end
 * @property string $text
 *
 * @property ImageRequiredForm $image
 *
 */

class BannersForm extends CompositeForm
{
    public $text;

    public function __construct(Banners $news = null, $config = [])
    {
        if ($news) {
            $this->load($news->getAttributes(), '');
        }
        $this->image = new ImageRequiredForm();
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['text',], 'required'],
            ['text', 'string', 'max' => 255]
        ];
    }

    protected function internalForms(): array
    {
        return  ['image'];
    }
}