<?php

namespace core\forms\manage\User;

use core\entities\User\User;
use core\forms\CompositeForm;


/**
 * @property ProfileForm $profile
 */

class UserEditForm extends CompositeForm
{
    public $username;
    public $password;
    /**
     * @var User
     */
    private $_user;

    public function __construct(User $user, $config = [])
    {
        $this->originalLoad($user->getAttributes(), '');
        $this->profile = new ProfileForm($user->profile);
        $this->_user = $user;

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['username'], 'required'],
            ['username', 'match', 'pattern' => '/^[a-z]\w*$/i'],
            [['username'], 'string', 'max' => 255],
            ['password', 'string', 'min' => 8, 'max' => 20],
            ['username', 'validateUsername'],
            ['password', 'validatePassword']
        ];
    }

    public function validatePassword($attribute, $params)
    {
        $pass = $this->attributes[$attribute];
        $isGoodPass = array_reduce(['A-Z', '\d', '!"#$%&\'()*+\\\,\-.:;<=>?@\[\\]^_`{|}~\/'], function ($carry, $pattern) use ($pass) {
                return $carry && preg_match('/['.$pattern.']/', $pass);
            }, true) && !preg_match('/[А-Яа-я]+/u', $pass);

        $isGoodPass ?: $this->addError($attribute, 'Пароль слишком простой. Добавьте большие буквы, цифры и спецсимволы. Запрещена кириллица.');
    }

    public function validateUsername($attribute, $params)
    {
        if($this->isUsernameExists()) {
            $this->addError($attribute, 'Такой логин уже существует.');
        }
    }
    
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'email' => 'E-mail',
        ];
    }

    private function isEmailExists()
    {
        return User::find()->where(['email' => $this->email])->andWhere(['not in', 'id', $this->_user->id])->exists();
    }

    private function isUsernameExists()
    {
        return User::find()->where(['username' => $this->username])->andWhere(['not in', 'id', $this->_user->id])->exists();
    }

    protected function internalForms(): array
    {
        return ['profile'];
    }
}