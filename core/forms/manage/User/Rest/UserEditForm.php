<?php


namespace core\forms\manage\User\Rest;


use core\behaviors\UploadedBase64;
use yii\base\Model;

class UserEditForm extends Model
{
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $image;

    public function rules(): array
    {
        return [
            [['first_name', 'last_name', 'phone', 'email'], 'required'],
            ['email', 'email'],
            [['first_name', 'last_name', 'phone'], 'string', 'max' => 255],
            ['image', 'safe']
        ];
    }

    public function beforeValidate()
    {
        $this->image = UploadedBase64::getInstance($this, 'image');
        return parent::beforeValidate();
    }
}