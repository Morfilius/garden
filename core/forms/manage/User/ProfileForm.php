<?php

namespace core\forms\manage\User;

use core\entities\User\Bitrix24\Profile;
use yii\base\Model;


/**
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $info
 */

class ProfileForm extends Model
{
    public $first_name;
    public $last_name;
    public $phone;
    public $info;
    public $email;
    public $staff;
    public $image;

    public $_phone_id;
    public $_email_id;
    /**
     * @var Profile
     */
    private $_profile;

    public function __construct(Profile $profile = null, $config = [])
    {
        if ($profile) {
            $this->load($profile->getAttributes(), '');
        }

        parent::__construct($config);
        $this->_profile = $profile;
    }

    public function rules(): array
    {
        return [
            [['first_name', 'last_name', 'phone', 'email'], 'required'],
            ['email', 'email'],
            [['first_name', 'last_name', 'phone', 'staff'], 'string', 'max' => 255],
            [['image'], 'safe'],
            [['_phone_id', '_email_id'], 'integer'],
            ['info', 'string', 'max' => 30000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'phone' => 'Телефон',
            'info' => 'Дополнительная информация',
        ];
    }

    public function afterValidate()
    {
        $this->phone = trim(str_replace(['+', ' '], '', $this->phone));
        parent::afterValidate();
    }
}