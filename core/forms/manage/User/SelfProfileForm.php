<?php

namespace core\forms\manage\User;

use core\entities\User\Bitrix24\Profile;
use core\forms\CompositeForm;
use core\forms\manage\Image\ImageRequiredForm;


/**
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $info
 * @property string $staff
 *
 * @property ImageRequiredForm $image
 */

class SelfProfileForm extends CompositeForm
{
    public $first_name;
    public $last_name;
    public $phone;
    public $info;
    public $staff;

    public $_phone_id;
    /**
     * @var Profile
     */
    private $_profile;

    public function __construct(Profile $profile = null, $config = [])
    {
        if ($profile) {
            $this->originalLoad($profile->getAttributes(), '');
            $this->image = new ImageRequiredForm();
        }
        $this->_profile = $profile;

        parent::__construct($config);
    }

    public function getAvatar()
    {
        return $this->_profile->image();
    }

    protected function internalForms(): array
    {
        return  ['image'];
    }

    public function rules(): array
    {
        return [
            [['first_name', 'last_name', 'phone'], 'required'],
            [['first_name', 'last_name', 'phone', 'staff'], 'string', 'max' => 255],
            ['_phone_id', 'integer'],
            ['info', 'string', 'max' => 30000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'phone' => 'Телефон',
            'info' => 'Должность',
            'staff' => 'Должность'
        ];
    }

    public function afterValidate()
    {
        $this->phone = trim(str_replace(['+', ' '], '', $this->phone));
        parent::afterValidate();
    }
}