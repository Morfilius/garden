<?php

namespace core\forms\manage\User;

use core\entities\User\User;
use core\forms\CompositeForm;


/**
 * @property ProfileForm profile
 */

class UserCreateForm extends CompositeForm
{
    public $username;
    public $password;
    /**
     * @var User
     */
    private $_user;

    public function __construct($config = [])
    {
        $this->profile = new ProfileForm();
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['username','password'], 'required'],
            ['username', 'match', 'pattern' => '/^[a-z]\w*$/i'],
            [['username'], 'string', 'max' => 255],
            [['username'], 'unique', 'targetClass' => User::class],
            ['password', 'string', 'min' => 8, 'max' => 20],
            ['password', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        $pass = $this->attributes[$attribute];
        $isGoodPass = array_reduce(['A-Z', '\d', '!"#$%&\'()*+\\\,\-.:;<=>?@\[\\]^_`{|}~\/'], function ($carry, $pattern) use ($pass) {
                return $carry && preg_match('/['.$pattern.']/', $pass);
        }, true) && !preg_match('/[А-Яа-я]+/u', $pass);

        $isGoodPass ?: $this->addError($attribute, 'Пароль слишком простой. Добавьте большие буквы, цифры и спецсимволы.');
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'email' => 'E-mail',
        ];
    }

    protected function internalForms(): array
    {
        return ['profile'];
    }
}