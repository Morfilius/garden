<?php

namespace core\forms\manage\Image;

use yii\base\Model;
use yii\web\UploadedFile;


/**
 * @property integer $id
 * @property string $image
 *
 */

class ImageBaseForm extends Model
{
    public $image;
    public $imageFile;

    public function rules(): array
    {
        return [
            ['image', 'safe'],
            ['image', 'image', 'skipOnEmpty' => true, 'extensions' => ['jpg', 'png']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'image' => 'Изображение'
        ];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
            $this->imageFile = UploadedFile::getInstance($this, 'image');
            return true;
        }
        return false;
    }
}