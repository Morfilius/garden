<?php

namespace core\forms\manage;

use core\entities\News;
use core\forms\CompositeForm;
use core\forms\manage\Image\ImageRequiredForm;
use DateTime;


/**
 * @property integer $image_id
 * @property string $title
 * @property string $date_start
 * @property string $date_end
 * @property string $text
 *
 * @property ImageRequiredForm $image
 *
 */

class NewsForm extends CompositeForm
{
    public $title;
    public $date_start;
    public $date_end;
    public $text;

    public function __construct(News $news = null, $config = [])
    {
        if ($news) {
            $this->load($news->getAttributes(), '');
        }
        $this->image = new ImageRequiredForm();
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title', 'date_start', 'date_end', 'text'], 'required'],
            [['date_start', 'date_end'], 'match', 'pattern' => '/\d{2}.\d{2}.\d{4}/'],
            [['date_start', 'date_end'], 'string', 'max' => 10],
            ['title', 'string', 'max' => 255],
            ['text', 'string', 'max' => 30000],
            ['date_start', function ($attr, $param) {
                $date_start = new DateTime($this->date_start);
                $date_end = new DateTime($this->date_end);
                if ($date_start > $date_end) {
                    $this->addError('date_start', 'Дата опубликования не может быть больше даты окончания.');
                }
            }]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок новости',
            'date_start' => 'Дата опубликования',
            'date_end' => 'Дата окончания',
            'text' => 'Текст новости'
        ];
    }

    public function beforeValidate()
    {
        if (!strlen(strip_tags($this->text))) {
            $this->text = '';
        }
        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        $this->date_start = (new DateTime($this->date_start))->format('Y-m-d');
        $this->date_end = (new DateTime($this->date_end))->format('Y-m-d');
        parent::afterValidate();
    }

    protected function internalForms(): array
    {
        return  ['image'];
    }
}