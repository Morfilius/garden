<?php


namespace core\behaviors;


use Yii;

class UploadImageBehavior extends \mohorev\file\UploadImageBehavior
{
    protected function getPlaceholderUrl($profile)
    {
        $path = Yii::getAlias($this->placeholder);
        $filename = basename($path);
        $url = Yii::getAlias('@static/'.$filename);
        $thumb = $this->getThumbFileName($filename, $profile);
        $thumbPath = dirname($path) . DIRECTORY_SEPARATOR . $thumb;
        $thumbUrl = dirname($url) . '/' . $thumb;

        if (!is_file($thumbPath)) {
            $this->generateImageThumb($this->thumbs[$profile], $path, $thumbPath);
        }

        return $thumbUrl;
    }
}