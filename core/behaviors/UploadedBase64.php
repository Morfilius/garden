<?php


namespace core\behaviors;


use Yii;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;

class UploadedBase64 extends UploadedFile
{
    public static function getInstance($model, $attribute)
    {
        $name = Html::getInputName($model, $attribute);
        if (static::isBase64String($model[$attribute])) {
            return static::getInstanceByBase64($name, $model[$attribute]);
        }
        return static::getInstanceByName($name);
    }

    public static function getInstances($model, $attribute)
    {
        $name = Html::getInputName($model, $attribute);
        $withBase64 = array_filter($model[$attribute], ['self', 'isBase64String']);
        if (is_array($withBase64) && !empty($withBase64)) {
            return array_map(function ($base64string) use($name) {
                return static::getInstanceByBase64($name, $base64string);
            }, $withBase64);
        }
        return static::getInstanceByName($name);
    }

    public static function getInstanceByBase64($name, $base64string)
    {
        $files = static::loadFromBase64($name, $base64string);
        return isset($files[$name]) ? new static($files[$name]) : null;
    }

    public static function isBase64String($string)
    {
        return (stristr($string,';base64,') !== false);
    }

    private static function loadFromBase64($name, $string)
    {
        if (!preg_match('#data:image/(.*?);base64,(.*)#s', $string, $matches)) {
            return false;
        }
        $ext = $matches[1];
        self::validateExt($ext);
        $content = base64_decode($matches[2]);
        $fileName =  uniqid(). '.' .$ext;
        $filePath = sys_get_temp_dir().'/'.$fileName;
        $fileSize = file_put_contents($filePath, $content);
        $output[$name] = [
            'name' => $fileName,
            'tempName' => $filePath,
            'tempResource' => [],
            'type' => 'image/'.$ext,
            'size' => $fileSize,
            'error' => 0,
        ];

        return $output;
    }

    private static function validateExt($ext)
    {
        if (! in_array($ext, ['png', 'jpg', 'jpeg'])) {
            throw new BadRequestHttpException('Invalid or empty file extension ' . $ext);
        }
    }

    protected function isUploadedFile($file)
    {
        return true;
    }
}