<?php

namespace core\useCases\auth;

use core\entities\User\User;
use core\forms\auth\SignupForm;
use core\repositories\UserRepository;

class SignupService
{
    private $users;

    public function __construct(
        UserRepository $users
    )
    {
        $this->users = $users;
    }

    public function signup(SignupForm $form): void
    {
        $user = User::signup(
            $form->username,
            $form->email,
            $form->password
        );
        $this->users->save($user);
//        $this->transaction->wrap(function () use ($user) {
//            $this->users->save($user);
//            $this->roles->assign($user->id, Rbac::ROLE_USER);
//        });
    }

}