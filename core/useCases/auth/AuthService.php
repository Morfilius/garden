<?php

namespace core\useCases\auth;

use common\auth\Identity;
use core\forms\auth\LoginForm;
use core\readRepositories\UserReadRepository;

class AuthService
{
    private $users;

    public function __construct(UserReadRepository $users)
    {
        $this->users = $users;
    }

    public function auth(LoginForm $form): Identity
    {
        $user = $this->users->findByUsernameOrEmail($form->username);
        if (!$user || !$user->isActive() || !$user->validatePassword($form->password)) {
            throw new \DomainException('Undefined user or password.');
        }
        return new Identity($user);
    }
}