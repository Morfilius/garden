<?php


namespace core\useCases\api;


use core\entities\Catalog\Catalog;
use core\entities\Catalog\CatalogFeatures;
use core\entities\Catalog\Category;
use core\forms\api\CatalogForm;
use core\repositories\CatalogRepository;

class CatalogApiService
{
    /**
     * @var CatalogRepository
     */
    private $repository;

    public function __construct(CatalogRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(CatalogForm $form)
    {
        if (! $category = Category::findOne(['name' => $form->category])) {
            $category = Category::create($form->category);
            $category->save();
        }
        if (! $catalog = Catalog::findOne(['product_id' => $form->product_id])) {
            $catalog = Catalog::create($form->product_id, $form->image, $form->title, $category->id);
        } else {
            $catalog->edit($form->image, $form->title, $category->id);
        }
        ! is_array($form->features) ?: $catalog->assignFeatures($form->features);
        ! is_array($form->relations) ?: $catalog->assignRelations($form->relations);
        $this->repository->save($catalog);
        return true;
    }
}