<?php


namespace core\useCases\api;


use core\entities\Deal\Bitrix24\Deal;
use core\entities\Deal\Bitrix24\DealStatus;
use core\repositories\DispatchRepository;
use core\repositories\User\UserRepository;
use core\useCases\NotificationService;
use yii\data\ArrayDataProvider;
use yii\web\BadRequestHttpException;

class DealApiService
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var DispatchRepository
     */
    private $dispatchRepository;

    public function __construct(UserRepository $userRepository, DispatchRepository $dispatchRepository)
    {
        $this->userRepository = $userRepository;
        $this->dispatchRepository = $dispatchRepository;
    }

    public function create($user_id, $dispatch_id)
    {
        $user = $this->userRepository->get($user_id);
        $dispatch = $this->dispatchRepository->get($dispatch_id);
        $deal = Deal::create($dispatch->title, $user->bitrix_id);
        $deal->insert();
    }

    public function showList($user_id)
    {
        $user = $this->userRepository->get($user_id);
        $deal = new Deal();
        return new ArrayDataProvider([
            'allModels' => $deal->getList($user->bitrix_id),
            'pagination' => [
                'defaultPageSize' => 20,
            ],
        ]);
    }

    public function statusFromBitrix(array $request)
    {
        if ($request['event'] != 'ONCRMDEALUPDATE') throw new BadRequestHttpException('Bad event.');
        if (! isset($request['data']['FIELDS']['ID'])) throw new BadRequestHttpException('Bad field id.');
        $dealWithUser = (new Deal())->getWithUser($request['data']['FIELDS']['ID']);
        $user = $this->userRepository->getByBitrixId($dealWithUser['user']['ID']);
        NotificationService::orderStatus($dealWithUser['deal']['TITLE'], DealStatus::getLabel($dealWithUser['deal']['STAGE_ID']), $user->id);
    }
}