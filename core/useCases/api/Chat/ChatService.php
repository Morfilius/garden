<?php


namespace core\useCases\api\Chat;


use core\entities\Chat\Chat;
use core\entities\Chat\Message;
use core\forms\api\MessageForm;
use core\repositories\ChatRepository;
use core\repositories\User\UserRepository;
use core\useCases\NotificationService;
use yii\web\BadRequestHttpException;
use yii\web\UnprocessableEntityHttpException;

class ChatService
{
    /**
     * @var ChatRepository
     */
    private $repository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var \core\services\Bitrix24\Chat\Chat
     */
    private $bitrixChat;

    public function __construct(ChatRepository $repository, UserRepository $userRepository, \core\entities\Chat\Bitrix24\Chat $bitrixChat){
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->bitrixChat = $bitrixChat;
    }

    public function createChats($user_id)
    {
        $this->createIsNotExists($user_id, 'manager');
        $this->createIsNotExists($user_id, 'plants');
    }

    public function addMessage(MessageForm $form, $chat_id, $user_id)
    {
        if (empty($form->images) && empty($form->message)) {
            throw new UnprocessableEntityHttpException('Message field cant be empty if images field is empty!');
        }
        $chat = $this->repository->get($chat_id);
        $user = $this->userRepository->get($user_id);
        $message = Message::create($form->message, $form->images, $chat->id);
        $this->repository->saveMessage($message);
        $this->sendMessageToBitrix($form->message, $user_id, $user->bitrix_id, $chat_id, $message->id, $message->image);
        return true;
    }

    public function messageFromBitrix(array $request)
    {
        if ($request['event'] != 'ONIMCONNECTORMESSAGEADD') throw new BadRequestHttpException('Bad event.');
        if ($request['data']['CONNECTOR'] != \core\services\Bitrix24\Chat\Chat::$connector_id) throw new BadRequestHttpException('Bad connector.');
        if ($request['data']['LINE'] != \core\services\Bitrix24\Chat\Chat::$line_id) throw new BadRequestHttpException('Bad line.');

        foreach ($request['data']['MESSAGES'] as $message) {
            $this->saveFromBitrix($message['message']['text'], $message['chat']['id'], $message['message']['files'] ?? []);
            \core\services\Bitrix24\Chat\Chat::sendDelivery($request['data']['CONNECTOR'], ($request['data']['LINE']), $message['im'], $message['im']['message_id'], $message['chat']['id']);
        }
    }

    private function createIsNotExists($user_id, $type) : Chat
    {
        $user = $this->userRepository->get($user_id);
        if ($chat = $this->repository->find($user->id, $type)) {
            return $chat;
        }
        $chat = Chat::create($user->id, $type);
        $this->repository->save($chat);
        $description = sprintf('Начат чат "%s"', $chat->isWithManager() ? Chat::MANAGER_DESC : Chat::PLANTS_DESC);
        $this->bitrixChat->createChat($description, $user_id, $chat->id, false, $user->bitrix_id);
        return $chat;
    }

    private function saveFromBitrix($text, $bitrix_chat_id, array $files = [])
    {
        $chat_id = (int)str_replace(\core\entities\Chat\Bitrix24\Chat::CHAT_PREFIX, '', $bitrix_chat_id);
        $chat = $this->repository->get($chat_id);
        $text = preg_replace('/\[.*\]/', '', $text);
        $message = Message::create(trim($text), $files, $chat->id, Message::EMPLOYEE_TYPE);
        $this->repository->saveMessage($message);
        NotificationService::newMessInChat($chat->user_id);
    }

    private function sendMessageToBitrix($text, $user_id, $bitrix_id, $chat_id, $message_id, $files = null)
    {
       $result = $this->bitrixChat->sendMessage($text, $user_id, $chat_id, $message_id, $bitrix_id, $files);
       NotificationService::newMessInChat($user_id);
       return $result;
    }
}