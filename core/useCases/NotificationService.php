<?php


namespace core\useCases;


use core\entities\Notification\Notification;
use core\readRepositories\NotificationReadRepository;
use core\services\Firebase\Firebase;
use DateTime;
use Yii;

class NotificationService
{
    /**
     * @var Firebase
     */
    private $firebase;
    /**
     * @var NotificationReadRepository
     */
    private $notificationReadRepository;

    public function __construct(Firebase $firebase, NotificationReadRepository $notificationReadRepository)
    {
        $this->firebase = $firebase;
        $this->notificationReadRepository = $notificationReadRepository;
    }

    public static function getInstance() : self
    {
        return Yii::$container->get(self::class);
    }

    public function notice($text, $receiver = null)
    {
        $receiver = $receiver ?? Yii::$app->user->id;
        $notification = Notification::create($text, $receiver);
        if ($notification->save()) {
            $this->firebase::sendByUsersIds($receiver, $text);
            return true;
        }
        return false;
    }

    public function unreadList($userId)
    {
        $list = $this->notificationReadRepository->showUnreadListProvider($userId);
        $models = $list->getModels();
        if (is_array($models) && !empty($models)) {
            $notifyIds = array_reduce($models, function ($result, Notification $notification) {
                $result[] = $notification->id;
                return $result;
            }, []);
            if (!empty($notifyIds)) {
                Notification::readMark($notifyIds);
            }
        }
        return $list;
    }

    public function readList($userId)
    {
        return $this->notificationReadRepository->showReadListProvider($userId);
    }
    
    public static function newMessInChat($receiver)
    {
        return self::getInstance()->notice('У вас новое сообщение в чате', $receiver);
    }

    public static function stockForYou($receiver)
    {
        return self::getInstance()->notice('Новые специальные акции для вас', $receiver);
    }

    public static function newPersonalRec($plantName, $receiver)
    {
        return self::getInstance()->notice(sprintf('Новые персональные рекомендации “%s”', $plantName), $receiver);
    }

    public static function orderStatus($orderName, $status, $receiver)
    {
        return self::getInstance()->notice(sprintf('У вашего заказа изменен статус на “%s”', $status), $receiver);
    }

    public static function newManager($receiver)
    {
        return self::getInstance()->notice('Вам назначен новый персональный менеджер', $receiver);
    }

    public static function arriveGardenerToday(DateTime $arriveTime, $receiver)
    {
        return self::getInstance()->notice(sprintf('Приезд садовника сегодня в %s', $arriveTime->format('H:i')), $receiver);
    }

    public static function arriveGardener(DateTime $arriveTime, $receiver)
    {
        return self::getInstance()->notice(sprintf('Приезд садовника %s в %s', $arriveTime->format('d.m.Y'), $arriveTime->format('H:i')), $receiver);
    }

    public static function orderStatusToday($orderName, DateTime $dateTime, $receiver)
    {
        return self::getInstance()->notice(sprintf('Ваш заказ будет доставлен сегодня в %s', $dateTime->format('H:i')), $receiver);
    }

}