<?php


namespace core\useCases\manage;

use core\entities\News;
use core\forms\manage\NewsForm;
use core\repositories\NewsRepository;
use core\services\TransactionManager;

class NewsManageService
{
    /**
     * @var TransactionManager
     */
    private $transactionManager;
    /**
     * @var NewsRepository
     */
    private $repository;

    public function __construct(TransactionManager $transactionManager, NewsRepository $repository)
    {
        $this->transactionManager = $transactionManager;
        $this->repository = $repository;
    }

    public function create(NewsForm $form)
    {
        $news = News::create(
            $form->title,
            $form->date_start,
            $form->date_end,
            $form->text,
            $form->image->imageFile
        );
        $this->repository->save($news);
    }

    public function edit($id, NewsForm $form)
    {
        $news = $this->repository->get($id);
        $news->edit(
            $form->title,
            $form->date_start,
            $form->date_end,
            $form->text,
            $form->image->imageFile
        );
        $this->repository->save($news);
    }
}