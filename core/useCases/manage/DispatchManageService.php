<?php


namespace core\useCases\manage;

use core\entities\Dispatch;
use core\forms\manage\DispatchForm;
use core\repositories\DispatchRepository;
use core\services\TransactionManager;
use core\useCases\NotificationService;

class DispatchManageService
{
    /**
     * @var TransactionManager
     */
    private $transactionManager;
    /**
     * @var DispatchRepository
     */
    private $repository;

    public function __construct(TransactionManager $transactionManager, DispatchRepository $repository)
    {
        $this->transactionManager = $transactionManager;
        $this->repository = $repository;
    }

    public function create(DispatchForm $form)
    {
        $news = Dispatch::create(
            $form->title,
            $form->banner_title,
            $form->banner_show,
            $form->recipient_id,
            $form->date_start,
            $form->date_end,
            $form->text,
            $form->image->imageFile,
            $form->image_wide->imageFile
        );
        $this->repository->save($news);
        NotificationService::stockForYou($news->recipient_id);
    }

    public function edit($id, DispatchForm $form)
    {
        $news = $this->repository->get($id);
        $news->edit(
            $form->title,
            $form->banner_title,
            $form->banner_show,
            $form->recipient_id,
            $form->date_start,
            $form->date_end,
            $form->text,
            $form->image->imageFile,
            $form->image_wide->imageFile
        );
        $this->repository->save($news);
    }
}