<?php


namespace core\useCases\manage;


use core\entities\User\Bitrix24\Employee;
use core\entities\User\Bitrix24\Profile;
use core\entities\User\User;
use core\forms\manage\User\SelfProfileForm;
use core\forms\manage\User\UserCreateForm;
use core\forms\manage\User\UserEditForm;
use core\readRepositories\UserReadRepository;
use core\repositories\User\ProfileRepository;
use core\repositories\User\UserRepository;
use core\services\TransactionManager;
use core\useCases\NotificationService;
use yii\web\BadRequestHttpException;

class UserManageService
{
    /**
     * @var TransactionManager
     */
    private $transactionManager;
    /**
     * @var UserReadRepository
     */
    private $userRepository;
    /**
     * @var ProfileRepository
     */
    private $profileRepository;

    public function __construct(TransactionManager $transactionManager, UserRepository $userRepository, ProfileRepository $profileRepository)
    {
        $this->transactionManager = $transactionManager;
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
    }

    public function create(UserCreateForm $form)
    {
        $user =  User::signup(
            $form->username,
            $form->password
        );
        $profile = Profile::createNew(
            $form->profile->first_name,
            $form->profile->last_name,
            $form->profile->phone,
            $form->profile->email,
            $form->profile->info
        );
        $this->transactionManager->wrap(function () use($user, $profile, $form) {
            $this->userRepository->save($user);
            $user->assignProfile($profile->insert());
            $this->userRepository->save($user);
        });
    }

    public function edit($id, UserEditForm $form)
    {
        $user = $this->userRepository->get($id);
        $user->edit(
            $form->username,
            $form->password
        );

        $profile = $user->profile;
        $profile->edit(
            $form->profile->first_name,
            $form->profile->last_name,
            $form->profile->phone,
            $form->profile->email,
            $form->profile->info,
            $form->profile->_phone_id,
            $form->profile->_email_id
        );

        $this->transactionManager->wrap(function () use($user, $profile) {
            $this->userRepository->save($user);
            $profile->update();
        });

    }

    public function editProfile($id, SelfProfileForm $form)
    {
        $profile = $this->profileRepository->getOperator($id);
        $profile->editOperator(
            $form->first_name,
            $form->last_name,
            $form->phone,
            $form->info,
            ''
//            $form->image->imageFile
        );
        $profile->updateEmployee();
    }

    public function managerFromBitrix(array $request)
    {
        if ($request['event'] != 'ONCRMCONTACTUPDATE') throw new BadRequestHttpException('Bad event.');
        if (! isset($request['data']['FIELDS']['ID'])) throw new BadRequestHttpException('Bad field id.');
        $bitrixId = $request['data']['FIELDS']['ID'];
        $manager = (new Profile())->get($bitrixId);
        $user = $this->userRepository->getByBitrixId($bitrixId);
        if ($user->isManagerChange($manager->manager_id)) {
            $user->managerChange($manager->manager_id);
            $this->userRepository->save($user);
            NotificationService::newManager($user->id);
        }
    }
}