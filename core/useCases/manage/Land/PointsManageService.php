<?php


namespace core\useCases\manage\Land;


use core\entities\Land\Garden\GardenInfo;
use core\entities\Land\LandMap;
use core\entities\Land\Points;
use core\forms\manage\Land\GardenInfoForm;
use core\readRepositories\PointsReadRepository;
use core\repositories\Land\GardenInfoRepository;
use core\repositories\Land\LandRepository;
use core\useCases\NotificationService;
use yii\helpers\Json;

class PointsManageService
{
    /**
     * @var LandRepository
     */
    private $repository;

    /**
     * @var GardenInfoRepository
     */
    private $gardenInfoRepository;
    /**
     * @var PointsReadRepository
     */
    private $pointsReadRepository;

    public function __construct(LandRepository $repository,
                                GardenInfoRepository $gardenInfoRepository,
                                PointsReadRepository $pointsReadRepository)
    {
        $this->repository = $repository;
        $this->gardenInfoRepository = $gardenInfoRepository;
        $this->pointsReadRepository = $pointsReadRepository;
    }

    public function save($id, string $points)
    {
        $map = $this->repository->getMap($id);
        $map->assignPoints(Json::decode($points));
        $map->save();
    }

    public function saveInfo(GardenInfoForm $form)
    {
        if ($gardenInfo = $this->pointsReadRepository->showInfo($form->land_points_id)) {
            $gardenInfo->edit(
                $form->catalog_id,
                $form->landing_date,
                $form->text
            );
            $isNewRec = $gardenInfo->isNewRec($form->text);
            $this->gardenInfoRepository->save($gardenInfo);
            if ($isNewRec) {
                NotificationService::newPersonalRec($gardenInfo->catalog->title, $gardenInfo->land->owner_id);
            }
        } else {
            $gardenInfo = GardenInfo::create(
                $form->land_points_id,
                $form->catalog_id,
                $form->landing_date,
                $form->text
            );
            $this->gardenInfoRepository->save($gardenInfo);
            if (!empty($form->text) && !empty($form->catalog_id)) {
                NotificationService::newPersonalRec($gardenInfo->catalog->title, $gardenInfo->land->owner_id);
            }
        }
    }
}