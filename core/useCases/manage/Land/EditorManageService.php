<?php

namespace core\useCases\manage\Land;


use core\forms\manage\Land\EditorForm;
use core\repositories\Land\LandRepository;

class EditorManageService
{
    /**
     * @var LandRepository
     */
    private $landRepository;

    public function __construct(LandRepository $landRepository)
    {
        $this->landRepository = $landRepository;
    }

    public function create($id, EditorForm $form)
    {
        $land = $this->landRepository->get($id);
        $land->assignMap($form->image);
        $this->landRepository->saveMap($land);
    }
}