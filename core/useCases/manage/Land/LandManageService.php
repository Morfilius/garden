<?php
namespace core\useCases\manage\Land;

use core\entities\Calendar\Bitrix24\Calendar;
use core\entities\Land\GardenCount;
use core\entities\Land\Land;
use core\forms\manage\Land\LandForm;
use core\repositories\Land\LandRepository;
use core\services\TransactionManager;

class LandManageService
{
    /**
     * @var LandRepository
     */
    private $repository;
    /**
     * @var TransactionManager
     */
    private $transactionManager;

    public function __construct(LandRepository $repository, TransactionManager $transactionManager)
    {
        $this->repository = $repository;
        $this->transactionManager = $transactionManager;
    }

    public function create(LandForm $form)
    {
        $land = Land::create(
            $form->title,
            $form->address,
            $form->employee_id,
            $form->owner_id,
            $form->land_area,
            $form->lawn_area,
            $form->age,
            $form->audit,
            $form->rec,
            new GardenCount(
                $form->deciduous_count,
                $form->pine_count,
                $form->garden_count
            )
        );

        $calendar = Calendar::create($form->title);

        $this->transactionManager->wrap(function () use($land, $calendar) {
            $land->assignCalendar($calendar->insert($land->title));
            $this->repository->save($land);
        });
        return $land;
    }

    public function edit($id, LandForm $form)
    {
        $land = $this->repository->get($id);

        $land->edit(
            $form->title,
            $form->address,
            $form->employee_id,
            $form->owner_id,
            $form->land_area,
            $form->lawn_area,
            $form->age,
            $form->audit,
            $form->rec,
            new GardenCount(
                $form->deciduous_count,
                $form->pine_count,
                $form->garden_count
            )
        );

        $this->repository->save($land);
    }
}