<?php


namespace core\useCases;


use core\entities\Deal\Bitrix24\Deal;
use core\entities\Notification\DeliveryNotified;
use core\repositories\User\UserRepository;
use DateTime;

class ProductDeliveryService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function notify()
    {
        $deal = new Deal();
        $list = $deal->getListByDate(new DateTime());
        if (is_array($list) && !empty($list)) {
            foreach ($list as $item) {
                $user = $this->userRepository->getByBitrixId($item['contact_id']);
                if(! $user) continue;
                if (! DeliveryNotified::isNotifiedToday($user->id, $item['id'])) {
                    $notified = DeliveryNotified::today($user->id, $item['id']);
                    $notified->save();
                    NotificationService::orderStatusToday($item['title'], new DateTime($item['delivery_time']), $user->id);
                } else {
                    continue;
                }
            }
        }
    }
}