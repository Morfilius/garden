<?php


namespace core\useCases;


use core\entities\Calendar\Bitrix24\Events;
use core\entities\Notification\GardenerNotified;
use core\repositories\User\UserRepository;
use DateTime;

class ArriveGardenerService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function today()
    {
        $event = new Events();
        $results = $event->getAllListByDate(new DateTime());
        if (! empty($results) && is_array($results)) {
            foreach ($results as $event) {
                $user = $this->userRepository->getByCalendarID($event['SECTION_ID']);
                if(empty($user)) continue;
                if (! GardenerNotified::isNotifiedToday($user->id, $event['ID'])) {
                    $notified = GardenerNotified::today($user->id, $event['ID']);
                    $notified->save();
                    NotificationService::arriveGardenerToday(new DateTime($event['DATE_FROM']), $user->id);
                } else {
                    continue;
                }
            }
        }
    }

    public function tomorrow()
    {
        $event = new Events();
        $results = $event->getAllListByDate(new DateTime('tomorrow'));
        if (! empty($results) && is_array($results)) {
            foreach ($results as $event) {
                $user = $this->userRepository->getByCalendarID($event['SECTION_ID']);
                if(empty($user)) continue;
                if (! GardenerNotified::isNotifiedTomorrow($user->id, $event['ID'])) {
                    $notified = GardenerNotified::tomorrow($user->id, $event['ID']);
                    $notified->save();
                    NotificationService::arriveGardener(new DateTime($event['DATE_FROM']), $user->id);
                } else {
                    continue;
                }
            }
        }
    }
}