<?php


namespace core\entities\Chat;


use core\entities\User\User;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property string type
 * @property integer created_at
 * @property integer updated_at
 *
 * @property Message[] message
 * @property User owner
 *
*/

class Chat extends ActiveRecord
{
    const MANAGER_DESC = 'Чат с менеджером';
    const PLANTS_DESC = 'Чат с садовником';

    public static function create($user_id, $type)
    {
        $self = new self();
        $self->user_id = $user_id;
        $self->type = $type;
        return $self;
    }
    
    public function assignMessage($message, $images)
    {
        $this->message = array_merge($this->message, [Message::create($message, $images)]);
    }

    public function isWithManager()
    {
        return ($this->type === 'manager');
    }

    public function getDefaultImage()
    {
        return Yii::getAlias('@static/garden-chat.png');
    }

    public function getLastMessage() : ?Message
    {
        return $this->getMessage()->orderBy('id DESC')->limit(1)->one();
    }

    public static function tableName()
    {
        return '{{%chats}}';
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => [
                    'message',
                ],
            ],
        ];
    }

    public function getMessage()
    {
        return $this->hasMany(Message::class, ['chat_id' => 'id'])->with('image');
    }

    public function getOwner()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}