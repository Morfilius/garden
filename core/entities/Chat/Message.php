<?php


namespace core\entities\Chat;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * @property integer id
 * @property integer chat_id
 * @property string text
 * @property string type
 * @property string created_at
 * @property string updated_at
 *
 * @property Image[] image
 * */

class Message extends ActiveRecord
{
    const CLIENT_TYPE = 'client';
    const EMPLOYEE_TYPE = 'employee';

    public $images;

    public static function create($text, $images, $chat_id = null, $type = self::CLIENT_TYPE)
    {
        $self = new self();
        $self->text = $text;
        $self->type = $type;
        !$chat_id ?: $self->chat_id = $chat_id;
        $self->images = $images;
        return $self;
    }
    
    public static function tableName()
    {
        return '{{%chats_messages}}';
    }

    public function isUserMessage()
    {
        return $this->type === 'client';
    }
    
    public function isEmployee()
    {
        return $this->type === self::EMPLOYEE_TYPE;
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getImage()
    {
        return $this->hasMany(Image::class, ['message_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(is_array($this->images)) {
            $this->images = array_filter($this->images);
            if (!empty($this->images)) {
                foreach ($this->images as $image) {
                    if ($image instanceof UploadedFile) {
                        (Image::create($this->id, $image))->save();
                    } else {
                        (Image::createWithUrl($this->id, $image['link']))->save();
                    }
                }
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }
}