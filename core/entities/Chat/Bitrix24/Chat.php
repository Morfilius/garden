<?php


namespace core\entities\Chat\Bitrix24;


use core\entities\Chat\Image;
use core\services\Bitrix24\BitrixApi;

class Chat
{
    private $connector_id;
    private $line_id;

    const CHAT_PREFIX = 'chat_imperial_garden'.'42342342342343242342342342342342342318';
    const USER_PREFIX = 'user_imperial_garden'.'42342342342343242342342342342342342318';

    public function __construct($connector_id, $line_id)
    {
        $this->connector_id = $connector_id;
        $this->line_id = $line_id;
    }

    public function createChat($text, $user_id, $chat_id, $message_id, $bitrix_id)
    {
        return self::sendMessage($text, $user_id, $chat_id, $message_id, $bitrix_id);
    }

    public function sendMessage($text, $user_id, $chat_id, $message_id, $bitrix_id, $files = null)
    {
        $chat_bitrix_id = self::CHAT_PREFIX . $chat_id;
        $user_bitrix_id = self::USER_PREFIX . $user_id;
        $query = [
            'user_list' => [
                'method' => 'crm.contact.get',
                'params' => [
                    'ID' => $bitrix_id,
                ]
            ],
            'send_messages' => [
                'method' => 'imconnector.send.messages',
                'params' => [
                    'CONNECTOR' => $this->connector_id,
                    'LINE' => $this->line_id,
                    'MESSAGES' => [
                        [
                            'user' => array_filter([
                                'id' => $user_bitrix_id,
                                'last_name' => '$result[user_list][LAST_NAME]',
                                'name' => '$result[user_list][NAME]',
                                'phone' => '$result[user_list][PHONE][0][VALUE]',
                                'email' => '$result[user_list][EMAIL][0][VALUE]',
                            ]),
                            'message' => [
                                'id' => $message_id,
                                'date' => time(),
                                'text' => $text,
                                'files' => $files ? array_map(function (Image $image) {
                                    return ['url' => $image->getUploadUrl('path')];
                                }, $files) : false
                            ],
                            'chat' => [
                                'id' => $chat_bitrix_id,
                            ],
                        ]
                    ],
                ]
            ],
        ];
        return BitrixApi::callBatch($query);
    }
}