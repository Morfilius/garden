<?php


namespace core\entities\Chat;


use core\behaviors\UploadImageBehavior;
use Imagine\Image\ManipulatorInterface;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * @property integer id
 * @property integer message_id
 * @property string path
*/

class Image extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%message_images}}';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['link'] = ['path'];
        return $scenarios;
    }

    public static function create($message_id, UploadedFile $image)
    {
        $self = new self();
        $self->path = $image;
        $self->message_id = $message_id;
        return $self;
    }

    public static function createWithUrl($message_id, $imageUrl)
    {
        $self = new self();
        $self->path = $imageUrl;
        $self->message_id = $message_id;
        $self->scenario = 'link';
        return $self;
    }

    public function getThumbnail($size)
    {
        if ($this->isOuterImage()) {
            return $this->path;
        }
        return $this->getThumbUploadUrl('path', $size);
    }

    private function isOuterImage()
    {
        return preg_match('/^http/', $this->path);
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'path',
                'createThumbsOnSave' => false,
                'createThumbsOnRequest' => true,
                'deleteTempFile' => false,
                'scenarios' => ['default'],
                'placeholder' => '@staticRoot/default.png',
                'path' => '@staticRoot/origin/chat/{message_id}',
                'url' => '@static/origin/chat/{message_id}',
                'thumbPath' => '@staticRoot/cache/chat/{message_id}',
                'thumbUrl' => '@static/cache/chat/{message_id}',
                'thumbs' => [
                    'small' => ['width' => 650, 'height' => 1000, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                ],
            ],
        ];
    }
}