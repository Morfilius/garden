<?php


namespace core\entities\Deal\Bitrix24;


use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\BitrixStorageInterface;
use DomainException;

class DealStorage implements BitrixStorageInterface
{

    public function showById($id)
    {
        return BitrixApi::call('crm.deal.get', [
            'id' => $id
        ]);
    }

    public function showList(array $filter = null, array $select = null)
    {
        return BitrixApi::call('crm.deal.list', array_filter([
                'SELECT' => $select,
                'FILTER' => $filter,
            ])
        );
    }

    public function insert(array $array)
    {
        if ( ! BitrixApi::call('crm.deal.add', $array)) {
            throw new DomainException('Deal insert in bitrix error');
        }
    }

    public function update(array $array)
    {
        // TODO: Implement update() method.
    }
}