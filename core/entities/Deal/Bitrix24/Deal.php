<?php


namespace core\entities\Deal\Bitrix24;


use core\entities\BaseBitrix24;
use core\services\Bitrix24\BitrixApi;
use DateTime;

class Deal extends BaseBitrix24
{
    public $title;
    public $bitrix_id;

    const DATE_FIELD_NAME = 'UF_CRM_1590586841162';

    public function __construct($config = [])
    {
        $this->setStorage(new DealStorage());
        parent::__construct($config);
    }

    public static function create($title, $bitrix_id)
    {
        $self = new self();
        $self->title = $title;
        $self->bitrix_id = $bitrix_id;
        return $self;
    }

    public function getWithUser($id)
    {
        $query = [
            'deal' => [
                'method' => 'crm.deal.get',
                'params' => [
                    'ID' => $id,
                ]
            ],
            'user' => [
                'method' => 'crm.contact.get',
                'params' => [
                    'ID' => '$result[deal][CONTACT_ID]',
                ]
            ]
        ];
        $result = BitrixApi::callBatch($query);
        return [
            'deal' => $result['result']['result']['deal'] ?? '',
            'user' => $result['result']['result']['user'] ?? '',
        ];
    }

    public function get($id)
    {
        return $this->storage->showById($id);
    }

    public function insert()
    {
        $this->storage->insert([
            'fields' => [
                'TITLE' => $this->title,
                'CONTACT_ID' => $this->bitrix_id,
                'UTM_SOURCE' => 'app'
            ]
        ]);
    }

    public function getList($bitrix_id)
    {
        return array_map(function (array $deal) {
            return [
                'title' => $deal['TITLE'],
                'stage' => DealStatus::getLabel($deal['STAGE_ID']),
                'date' => (new DateTime($deal['DATE_CREATE']))->format('d.m.Y'),
                'price' => $deal['OPPORTUNITY'] ? $deal['OPPORTUNITY'].' руб.' : '-'
            ];
        }, $this->storage->showList([
            'CONTACT_ID' => $bitrix_id
        ]));
    }

    public function getListByDate(DateTime $dateTime)
    {
        return array_map(function (array $deal) {
            return [
                'id' => $deal['ID'],
                'title' => $deal['TITLE'],
                'contact_id' => $deal['CONTACT_ID'],
                'delivery_time' => $deal[self::DATE_FIELD_NAME]
            ];
        }, $this->storage->showList([
            '>' . self::DATE_FIELD_NAME => sprintf('%sT00:00:00+03:00', $dateTime->format('Y-m-d')),
            '<' . self::DATE_FIELD_NAME => sprintf('%sT23:59:00+03:00', $dateTime->format('Y-m-d'))
        ], [
            self::DATE_FIELD_NAME, 'TITLE', 'CONTACT_ID'
        ]));
    }


}