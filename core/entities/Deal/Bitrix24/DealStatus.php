<?php


namespace core\entities\Deal\Bitrix24;


class DealStatus
{
    const PAY_WAIT = 1;
    const PAYED = 2;
    const DELIVERY = 3;
    const DELIVERED = 'WON';

    const DEFAULT = 'Обработка заказа';


    public static function labels()
    {
        return [
            self::PAY_WAIT => 'Ожидание платежа',
            self::PAYED => 'Оплачен',
            self::DELIVERY => 'Доставка',
            self::DELIVERED => 'Доставлен',
        ];
    }

    public static function getLabel($status)
    {
        $labels = self::labels();
        return isset($labels[$status]) ? $labels[$status] : self::DEFAULT;
    }
}