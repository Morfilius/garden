<?php


namespace core\entities\Notification;


use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer event_id
 * @property string type
*/

class DeliveryNotified extends ActiveRecord
{
    const TODAY = 'today';

    public static function tableName()
    {
        return '{{%delivery_notified}}';
    }

    public static function today($userId, $eventId)
    {
        $self = new self();
        $self->user_id = $userId;
        $self->event_id = $eventId;
        $self->type = self::TODAY;
        return $self;
    }

    public static function isNotifiedToday($userId, $eventId)
    {
        return self::find()->where(['user_id' => $userId])->andWhere(['event_id' => $eventId])
            ->andWhere(['type' => self::TODAY])->exists();
    }
}