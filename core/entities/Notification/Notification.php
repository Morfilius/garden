<?php


namespace core\entities\Notification;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer receiver
 * @property string text
 * @property integer created_at
 * @property integer updated_at
 * @property integer read
*/

class Notification extends ActiveRecord
{
    const READ = 1;
    const UNREAD = 0;

    public static function create($text, $receiver)
    {
        $self = new self();
        $self->text = $text;
        $self->receiver = $receiver;
        return $self;
    }

    public static function readMark(array $notifyIds)
    {
        self::updateAll(['read' => 1], ['in', 'id', $notifyIds]);
    }
    
    public static function tableName()
    {
        return '{{%notifications}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
}