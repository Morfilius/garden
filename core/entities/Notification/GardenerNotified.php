<?php


namespace core\entities\Notification;


use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer event_id
 * @property string type
*/

class GardenerNotified extends ActiveRecord
{
    const TODAY = 'today';
    const TOMORROW = 'tomorrow';

    public static function tableName()
    {
        return '{{%gardener_notified}}';
    }

    public static function tomorrow($userId, $eventId)
    {
        $self = new self();
        $self->user_id = $userId;
        $self->event_id = $eventId;
        $self->type = self::TOMORROW;
        return $self;
    }

    public static function today($userId, $eventId)
    {
        $self = new self();
        $self->user_id = $userId;
        $self->event_id = $eventId;
        $self->type = self::TODAY;
        return $self;
    }

    public static function isNotifiedTomorrow($userId, $eventId)
    {
        return self::find()->where(['user_id' => $userId])->andWhere(['event_id' => $eventId])
            ->andWhere(['type' => self::TOMORROW])->exists();
    }

    public static function isNotifiedToday($userId, $eventId)
    {
        return self::find()->where(['user_id' => $userId])->andWhere(['event_id' => $eventId])
            ->andWhere(['type' => self::TODAY])->exists();
    }
}