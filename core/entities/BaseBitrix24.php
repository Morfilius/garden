<?php


namespace core\entities;


use core\services\Bitrix24\BitrixStorageInterface;
use yii\base\Model;

/**
 * @property BitrixStorageInterface storage
 * */

class BaseBitrix24 extends Model
{
    protected $storage;

    public function setStorage(BitrixStorageInterface $storage)
    {
        $this->storage = $storage;
        return $this;
    }
}