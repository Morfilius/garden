<?php
namespace core\entities;

use core\behaviors\UploadImageBehavior;
use Imagine\Image\ManipulatorInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * News model
 *
 * @property integer $id
 * @property integer $image_id
 * @property string $title
 * @property string $date_start
 * @property string $date_end
 * @property string $text
 * @property string $image
 * @property integer $created_at
 * @property integer $updated_at
 *
 */

class News extends ActiveRecord
{
    public static function create($title, $date_start, $date_end, $text, $image)
    {
        $news = new self();
        $news->title = $title;
        $news->date_start = $date_start;
        $news->date_end = $date_end;
        $news->text = $text;
        $news->image = $image;
        return $news;
    }

    public function edit($title, $date_start, $date_end, $text, $image)
    {
        $this->title = $title;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->text = $text;
        $this->image = $image;
    }

    public static function tableName()
    {
        return '{{%news}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'image',
                'createThumbsOnSave' => false,
                'createThumbsOnRequest' => true,
                'scenarios' => ['default'],
                'placeholder' => '@staticRoot/default.png',
                'path' => '@staticRoot/origin/news',
                'url' => '@static/origin/news',
                'thumbPath' => '@staticRoot/cache/news',
                'thumbUrl' => '@static/cache/news',
                'thumbs' => [
                    'grid' => ['width' => 100, 'height' => 64, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'wide' => ['width' => 999, 'height' => 186, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                ],
            ],
        ];
    }
}
