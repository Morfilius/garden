<?php


namespace core\entities\Calendar\Bitrix24;


use core\entities\BaseBitrix24;
use core\entities\Calendar\Bitrix24\CalendarStorage;


class Calendar extends BaseBitrix24
{
    public $name;

    public function __construct($config = [])
    {
        $this->setStorage(new CalendarStorage());
        parent::__construct($config);
    }

    public static function create($name)
    {
        $self = new self();
        $self->name = $name;
        return $self;
    }

    public function insert($name)
    {
        return $this->storage->insert(['name' => $name]);
    }
}