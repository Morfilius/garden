<?php


namespace core\entities\Calendar\Bitrix24;


use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\BitrixStorageInterface;
use DomainException;
use yii\web\NotFoundHttpException;

class CalendarStorage implements BitrixStorageInterface
{

    public function showById($id)
    {
        throw new NotFoundHttpException('Method not found');
    }

    public function showList(array $filter = null)
    {
        throw new NotFoundHttpException('Method not found');
    }

    public function insert(array $array)
    {
        if ($response = BitrixApi::call('calendar.section.add', array_merge([
            'type' => 'user',
            'ownerId' => 1
        ], $array))) {
            return $response;
        }
        throw new DomainException('Insert error');
    }

    public function update(array $array)
    {
        throw new NotFoundHttpException('Method not found');
    }
}