<?php


namespace core\entities\Calendar\Bitrix24;


use core\services\Bitrix24\BitrixStorageInterface;
use DateTime;
/**
* @property EventsStorage storage
 */
class Events
{
    private $calendar_id;
    private $current_time;
    private $timestamps;
    private $currentKey;
    private $list = [];


    protected $storage;

    public function __construct($calendar_id = null)
    {
        $this->setStorage(new EventsStorage());
        $this->current_time = time();
        $this->calendar_id = $calendar_id;
        $this->timestamps = $this->sortWithCurrent($this->call());

        $this->currentKey = array_search($this->current_time, $this->timestamps);
    }

    public function getList()
    {
        if (empty($this->list)) {
            $this->list = $this->storage->showList([
                'type' => 'user',
                'ownerId' => 1,
                'section' => [$this->calendar_id]
            ]);
        }
        return $this->list;
    }

    public function getAllListByDate(DateTime $dateTime)
    {
        return $this->list = $this->storage->showList([
            'type' => 'user',
            'ownerId' => 1,
            'from' => $dateTime->format('Y-m-d'),
      		'to'=> $dateTime->format('Y-m-d')
        ]);
    }

    public function getTotalStatistic()
    {
        $inThisMonth = $this->inThisMonth();
        return array_reduce($inThisMonth, function ($result, $item) {
            $data = array_map('trim', explode('|', $item['NAME']));
            $result['visits_per_month'] += 1;
            $result['work_per_month'] += isset($data[1]) ? count(explode(',', $data[1])) : 0;
            $result['price_per_month'] += isset($data[2]) ? (int)$data[2] : 0;
            return $result;
        }, ['visits_per_month' => 0, 'work_per_month' => 0, 'price_per_month' => 0]);
    }

    public function getWorkList()
    {
        $inThisMonth = $this->inThisMonth();
        return array_reduce($inThisMonth, function ($result, $event) {
            $data = array_map('trim', explode('|', $event['NAME']));
            $title = $data[0] ?? '';
            $gardener = preg_replace('/\(.*?\)/', '', $title);
            preg_match('/\((.*?)\)/', $title, $match);
            $staff = $match[1] ?? '';
            $result[] = [
                'work' => $data[1] ?? '-',
                'price' => isset($data[2]) ? (int)$data[2] : '-',
                'date' => (new DateTime($event['DATE_FROM']))->format('d.m.Y'),
                'gardener' => trim($gardener) ?? '',
                'staff' => $staff,
            ];
            return $result;
        }, []);
    }

    private function inThisMonth()
    {
        $list = $this->getList();
        $thisMonth = new DateTime();
        return array_filter($list, function ($item) use($thisMonth) {
            $eventDate = new DateTime($item['DATE_FROM']);
            $isPrevThisDay = ($eventDate < $thisMonth);
            $isThisMonth = ($eventDate->format('mY') === $thisMonth->format('mY'));
            return $isPrevThisDay && $isThisMonth;
        });
    }

    public function getListPerMonthWithInfo() : ?array
    {
        $perMonth = $this->getListPerMonth();
        return array_reduce($perMonth, function ($result, $month) {
            $current = reset($month);
            $result[] = [
                'month' => (int)(new DateTime($current[0]['DATE_FROM']))->format('m'),
                'days' => array_reduce($month, function ($result, $day) {
                    $result[] = [
                        'day' => (int)(new DateTime($day[0]['DATE_FROM']))->format('d'),
                        'events' => array_map(function ($event) {
                            $data = array_map('trim', explode('|', $event['NAME']));
                            $from = new DateTime($event['DATE_FROM']);
                            $to = new DateTime($event['DATE_TO']);
                            $title = $data[0] ?? '';

                            $gardener = preg_replace('/\(.*?\)/', '', $title);
                            preg_match('/\((.*?)\)/', $title, $match);
                            $staff = $match[1] ?? '';
                            return [
                                'gardener' => trim($gardener) ?? '',
                                'staff' => $staff,
                                'work' => $data[1] ?? '',
                                'price' => isset($data[2]) ? (int)$data[2] : 0,
                                'date_from' => [
                                    'hour' => (int)$from->format('H'),
                                    'minute' => (int)$from->format('i')
                                ],
                                'date_to' => [
                                    'hour' => (int)$to->format('H'),
                                    'minute' => (int)$to->format('i')
                                ],
                            ];
                        }, $day)
                    ];
                    return $result;
                }, [])
            ];
            return $result;
        }, []);
    }

    public function getListPerMonth($eventList = null)
    {
        $list = $eventList ?? $this->getList();
        $perMonth = array_reduce($list, function ($prev, $event) {
            $date = new DateTime($event['DATE_FROM']);
            $prev[$date->format('m')][$date->format('d')][] = $event;
            return $prev;
        }, []);
        ksort($perMonth);

        return array_map(function ($item) {
            ksort($item);
            return $item;
        }, $perMonth);
    }

    public function getListNextDates()
    {
        $now = new DateTime();
        return array_filter($this->getList(), function (array $event) use($now) {
            $date = new DateTime($event['DATE_FROM']);
            return ($now < $date);
        });
    }
    
    public function getPrev()
    {
        return isset($this->timestamps[$this->currentKey - 1]) ? date('d.m.Y', $this->timestamps[$this->currentKey - 1]) : null;
    }

    public function getNext()
    {
        return isset($this->timestamps[$this->currentKey + 1]) ? date('d.m.Y', $this->timestamps[$this->currentKey + 1]) : null;
    }

    private function call()
    {
        return array_map(function ($event) {
            return (new DateTime($event['DATE_TO']))->getTimestamp();
        }, $this->getList());
    }

    public function setStorage(BitrixStorageInterface $storage)
    {
        $this->storage = $storage;
        return $this;
    }

    private function sortWithCurrent(array $timestamps)
    {
        $timestamps[] = $this->current_time;
        sort($timestamps);
        return $timestamps;
    }
}