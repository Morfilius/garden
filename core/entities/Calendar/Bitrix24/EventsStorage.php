<?php


namespace core\entities\Calendar\Bitrix24;


use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\BitrixStorageInterface;

class EventsStorage implements BitrixStorageInterface
{

    public function showById($id)
    {
        // TODO: Implement showById() method.
    }

    public function showList(array $filter = null)
    {

       return BitrixApi::call('calendar.event.get', $filter);
    }

    public function insert(array $array)
    {
        // TODO: Implement insert() method.
    }

    public function update(array $array)
    {
        // TODO: Implement update() method.
    }
}