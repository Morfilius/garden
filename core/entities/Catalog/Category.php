<?php


namespace core\entities\Catalog;

use yii\db\ActiveRecord;
use yii\helpers\BaseInflector;

/**
 * @property int id
 * @property string name
 * @property string kwd
 * @property string image
 *
*/

class Category extends ActiveRecord
{
    public static function create($name, $image = null)
    {
        $self = new self();
        $self->image = $image;
        $self->name = $name;
        $self->kwd = BaseInflector::transliterate($name);
        return $self;
    }

    public static function tableName()
    {
        return '{{%category}}';
    }
}