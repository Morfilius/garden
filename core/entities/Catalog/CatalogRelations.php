<?php


namespace core\entities\Catalog;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int id
 * @property int product_id
 * @property int relation_id
 *
 * @property Catalog catalog
*/

class CatalogRelations extends ActiveRecord
{
    public static function create($productId)
    {
        $self = new self();
        $self->product_id = $productId;
        return $self;
    }
    
    public static function tableName()
    {
        return '{{%catalog_relations}}';
    }

    public function getCatalog() : ActiveQuery
    {
        return $this->hasOne(Catalog::class, ['product_id' => 'product_id'])->limit(1);
    }
}