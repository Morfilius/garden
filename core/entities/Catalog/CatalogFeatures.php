<?php


namespace core\entities\Catalog;


use yii\db\ActiveRecord;

/**
 * @property int id
 * @property int catalog_id
 * @property string name
 * @property string value
*/

class CatalogFeatures extends ActiveRecord
{
    public static function create($name, $value)
    {
        $self = new self();
        $self->name = $name;
        $self->value = $value;
        return $self;
    }

    public static function tableName()
    {
        return '{{%catalog_features}}';
    }
}