<?php


namespace core\entities\Catalog;


use core\behaviors\UploadImageBehavior;
use Imagine\Image\ManipulatorInterface;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int id
 * @property int product_id
 * @property string image
 * @property string title
 * @property integer category_id
 *
 * @property CatalogFeatures[] features
 * @property CatalogRelations[] rels
 *
*/

class Catalog extends ActiveRecord
{
    public static function create($product_id, $image, $title,  $category)
    {
        $self = new self();
        $self->product_id = $product_id;
        $self->image = $image;
        $self->title = $title;
        $self->category_id = $category;

        return $self;
    }

    public function edit($image, $title,  $category)
    {
        $image ? $this->image = $image : $this->updateAttributes(['image' => null]);
        $this->title = $title;
        $this->category_id = $category;;
    }

    /**
     * @param array $features[]['name','value']
     */
    public function assignFeatures(array $features)
    {
        $features = array_map(function ($item) {
            return CatalogFeatures::create($item['name'], $item['value']);
        }, $features);
        $this->features = $features;
    }

    public function assignRelations(array $relations)
    {
        $relations = array_map(function (int $item) {
            return CatalogRelations::create($item);
        }, $relations);
        $this->rels = $relations;
    }

    public static function tableName()
    {
        return '{{%catalog}}';
    }

    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => [
                    'features',
                    'rels',
                ],
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'image',
                'createThumbsOnSave' => false,
                'createThumbsOnRequest' => true,
                'deleteTempFile' => false,
                'scenarios' => ['default'],
                'placeholder' => '@staticRoot/default.png',
                'path' => '@staticRoot/origin/catalog',
                'url' => '@static/origin/catalog',
                'thumbPath' => '@staticRoot/cache/catalog',
                'thumbUrl' => '@static/cache/catalog',
                'thumbs' => [
                    'thumb' => ['width' => 650, 'height' => 650, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'grid' => ['width' => 232, 'height' => 92, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'wide' => ['width' => 434, 'height' => 178, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                ],
            ]
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function getFeatures() : ActiveQuery
    {
        return $this->hasMany(CatalogFeatures::class, ['catalog_id' => 'id']);
    }

    public function getRels() : ActiveQuery
    {
        return $this->hasMany(CatalogRelations::class, ['catalog_id' => 'id']);
    }

    public function getCategory() : ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }
}