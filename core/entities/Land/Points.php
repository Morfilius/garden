<?php


namespace core\entities\Land;


use core\entities\Catalog\Catalog;
use core\entities\Land\Garden\Garden;
use core\entities\Land\Garden\GardenInfo;
use core\repositories\Land\PointsRepository;
use DomainException;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * @property int id
 * @property int lands_map_id
 * @property int garden_id
 * @property int radius
 * @property int xPx
 * @property int yPx
 * @property int xPerc
 * @property int yPerc
 *
 * @property Garden garden
 * @property GardenInfo info
 * @property Catalog catalog
 * @property Land land
 * @property LandMap map
 *
 */

class Points extends ActiveRecord
{
    public static function create($garden_id, $radius, $xPx, $yPx, $xPerc, $yPerc, $lands_map_id = null)
    {
        $self = new self();
        $self->lands_map_id = $lands_map_id;
        $self->garden_id = $garden_id;
        $self->radius = $radius;
        $self->xPx = $xPx;
        $self->yPx = $yPx;
        $self->xPerc = $xPerc;
        $self->yPerc = $yPerc;
        return $self;
    }

    public static function editById($id, $garden_id, $radius, $xPx, $yPx, $xPerc, $yPerc)
    {
        $points = Yii::createObject(PointsRepository::class)->get($id);
        $points->edit($garden_id, $radius, $xPx, $yPx, $xPerc, $yPerc);
        return $points;
    }

    public function edit($garden_id, $radius, $xPx, $yPx, $xPerc, $yPerc)
    {
        $this->garden_id = $garden_id;
        $this->radius = $radius;
        $this->xPx = $xPx;
        $this->yPx = $yPx;
        $this->xPerc = $xPerc;
        $this->yPerc = $yPerc;
    }

    public function isAssignCatItem()
    {
        return (isset($this->info) && !empty($this->info) && $this->info->catalog_id);
    }
    
    public static function tableName()
    {
        return '{{%land_points}}';
    }

    public function getGarden(): ActiveQuery
    {
        return $this->hasOne(Garden::class, ['id' => 'garden_id']);
    }

    public function getInfo(): ActiveQuery
    {
        return $this->hasOne(GardenInfo::class, ['land_points_id' => 'id']);
    }

    public function getCatalog(): ActiveQuery
    {
        return $this->hasOne(Catalog::class, ['id' => 'catalog_id'])->via('info');
    }

    public function getLand(): ActiveQuery
    {
        return $this->hasOne(Land::class, ['id' => 'land_id'])->via('map');
    }

    public function getMap(): ActiveQuery
    {
        return $this->hasOne(LandMap::class, ['id' => 'lands_map_id']);
    }

    public function getCatalogOrDie(): Catalog
    {
        if (!$this->catalog) {
            throw new DomainException('Catalog item not found');
        }
        return $this->catalog;
    }

    public function getInfoOrDie(): GardenInfo
    {
        if (!$this->info) {
            throw new DomainException('Catalog info not found');
        }
        return $this->info;
    }

    public function getLandOrDie(): Land
    {
        if (!$this->land) {
            throw new DomainException('Land info not found');
        }
        return $this->land;
    }
}