<?php
namespace core\entities\Land;


use core\entities\Calendar\Bitrix24\Calendar;
use core\entities\Calendar\Bitrix24\Events;
use core\entities\User\Bitrix24\EmployeeStorage;
use core\entities\User\Bitrix24\Profile;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


/**
 * @property int $id
 * @property string $title
 * @property string $address
 * @property int $employee_id
 * @property int $owner_id
 * @property int $land_area
 * @property int $lawn_area
 * @property int $deciduous_count
 * @property int $pine_count
 * @property int $garden_count
 * @property int $age
 * @property int $calendar_id
 * @property string $audit
 * @property string $rec
 *
 * @property LandMap $map
 * @property Profile $gardener
 * @property Events $events
 */

class Land extends ActiveRecord
{
    private $event;

    public static function create($title, $address, $employee_id, $owner_id,
                                  $land_area, $lawn_area, $age, $audit, $rec, GardenCount $gardenCount)
    {
        $land = new self();

        $land->title = $title;
        $land->address = $address;
        $land->employee_id = $employee_id;
        $land->owner_id = $owner_id;
        $land->land_area = $land_area;
        $land->lawn_area = $lawn_area;
        $land->age = $age;
        $land->audit = $audit;
        $land->rec = $rec;
        $land->deciduous_count = $gardenCount->deciduous_count;
        $land->pine_count = $gardenCount->pine_count;
        $land->garden_count = $gardenCount->garden_count;

        return $land;
    }

    public function edit($title, $address, $employee_id, $owner_id,
                         $land_area, $lawn_area, $age, $audit, $rec, GardenCount $gardenCount)
    {
        $this->title = $title;
        $this->address = $address;
        $this->employee_id = $employee_id;
        $this->owner_id = $owner_id;
        $this->land_area = $land_area;
        $this->lawn_area = $lawn_area;
        $this->age = $age;
        $this->audit = $audit;
        $this->rec = $rec;
        $this->deciduous_count = $gardenCount->deciduous_count;
        $this->pine_count = $gardenCount->pine_count;
        $this->garden_count = $gardenCount->garden_count;
    }

    public static function tableName()
    {
        return '{{%lands}}';
    }

    public function assignMap(UploadedFile $image)
    {
        if ($this->map) {
            $this->map->edit($image);
        } else {
            $this->populateRelation('map', LandMap::create($image, $this->id));
        }
    }

    public function getTotalArea()
    {
        return $this->land_area + $this->lawn_area;
    }

    public function getTotalAreaWithWord()
    {
        return Yii::$app->i18n->format('{n, plural, =0 {# соток} =1 {# сотка} one {# сотка} few{# сотки} many{# соток} other{# соток}}', ['n' => $this->getTotalArea()], 'ru_RU');
    }

    public function getLawnAreaWithWord()
    {
        return Yii::$app->i18n->format('{n, plural, =0 {# соток} =1 {# сотка} one {# сотка} few{# сотки} many{# соток} other{# соток}}', ['n' => $this->lawn_area], 'ru_RU');
    }

    public function getAgeWithWord()
    {
        return Yii::$app->i18n->format('{n, plural, =0 {# лет} =1 {# год} one {# год} few{# года} many{# лет} other{# лет}}', ['n' => $this->age], 'ru_RU');
    }

    public function getTotalPlants()
    {
        return $this->deciduous_count + $this->garden_count + $this->pine_count;
    }

    public function assignCalendar($calendar_id)
    {
        $this->calendar_id = $calendar_id;
    }

    public function getMapOrDie(): ?LandMap
    {
        if ( ! $this->map) {
            throw new NotFoundHttpException('Map not found');
        }
        return $this->map;
    }

    public function getEvents()
    {
        if (!$this->event) {
            $this->event = new Events($this->calendar_id);
        }

        return $this->event;
    }

    public function getMap(): ActiveQuery
    {
        return $this->hasOne(LandMap::class, ['land_id' => 'id']);
    }

    public function getGardener()
    {
        return (new Profile())->setStorage(new EmployeeStorage())->get($this->employee_id);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

}
