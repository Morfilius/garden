<?php


namespace core\entities\Land;


class GardenCount
{
    public $deciduous_count;
    public $pine_count;
    public $garden_count;

    public function __construct($deciduous_count, $pine_count, $garden_count)
    {
        $this->deciduous_count = $deciduous_count;
        $this->pine_count = $pine_count;
        $this->garden_count = $garden_count;
    }
}