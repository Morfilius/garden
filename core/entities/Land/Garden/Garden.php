<?php


namespace core\entities\Land\Garden;

use yii\db\ActiveRecord;

/**
 * @property integer $id;
 * @property string $name;
 * @property string $category;
 * @property string $category_slug;
 * @property string $image;
 *
*/

class Garden extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%gardens}}';
    }
}