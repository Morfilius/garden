<?php


namespace core\entities\Land\Garden;


use core\entities\Catalog\Catalog;
use core\entities\Land\Land;
use core\entities\Land\LandMap;
use core\entities\Land\Points;
use DateTime;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer catalog_id
 * @property integer land_points_id
 * @property string landing_date
 * @property string text
 *
 * @property Catalog catalog
 * @property Points point
 * @property LandMap map
 * @property Land land
*/

class GardenInfo extends ActiveRecord
{
    public static function create($land_points_id, $catalog_id, $landing_date, $text)
    {
        $self = new self();
        $self->land_points_id = $land_points_id;
        $self->catalog_id = $catalog_id;
        $self->landing_date = $landing_date;
        $self->text = $text;

        return $self;
    }

    public function edit($catalog_id, $landing_date, $text)
    {
        $this->catalog_id = $catalog_id;
        $this->landing_date = $landing_date;
        $this->text = $text;
    }

    public static function tableName()
    {
        return '{{%gardens_info}}';
    }

    public function getAge()
    {
        $date = new DateTime($this->landing_date);
        $interval = $date->diff(new DateTime());
        $interval = $interval->days > 0 ? str_replace('.0', '', round($interval->days/365, 1)) : 0;
        return Yii::$app->i18n->format('{n, plural, =0 {# лет} =1 {# год} one {# год} few{# года} many{# лет} other{# лет}}', ['n' => $interval], 'ru_RU');
    }

    public function isNewRec($text)
    {
        return $this->getOldAttribute('text') != $text;
    }
    
    public function getCatalog()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'catalog_id']);
    }

    public function getPoint()
    {
        return $this->hasOne(Points::className(), ['id' => 'land_points_id']);
    }

    public function getMap(): ActiveQuery
    {
        return $this->hasOne(LandMap::class, ['id' => 'lands_map_id'])->via('point');
    }

    public function getLand(): ActiveQuery
    {
        return $this->hasOne(Land::class, ['id' => 'land_id'])->via('map');
    }
}