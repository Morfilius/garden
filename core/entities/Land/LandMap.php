<?php
namespace core\entities\Land;

use Imagine\Image\ManipulatorInterface;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use mohorev\file\UploadImageBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * @property int $id
 * @property string $image
 * @property integer $land_id
 *
 * @property Land $land
 * @property Points $points
 */


class LandMap extends ActiveRecord
{
    public static function create(UploadedFile $image, $land_id)
    {
        $landMap = new static();
        $landMap->image = $image;
        $landMap->land_id = $land_id;
        return $landMap;
    }

    public function edit(UploadedFile $image)
    {
        $this->image = $image;
    }

    public function assignPoints($points)
    {
        $this->points = array_map(function ($point) {
            if (isset($point['id'])) {
                return Points::editById($point['id'], $point['garden_id'], $point['radius'], $point['xPx'], $point['yPx'], $point['xPerc'], $point['yPerc']);
            }
            return Points::create($point['garden_id'], $point['radius'], $point['xPx'], $point['yPx'], $point['xPerc'], $point['yPerc']);
        }, $points);
    }
    
    public static function tableName()
    {
        return '{{%lands_map}}';
    }

    public function rules()
    {
        return [['image', 'safe']];
    }

    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => [
                    'points',
                ],
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'image',
                'deleteTempFile' => false,
                'createThumbsOnRequest' => true,
                'scenarios' => ['default'],
                'placeholder' => '@staticRoot/default.png',
                'path' => '@staticRoot/origin/map',
                'url' => '@static/origin/map',
                'thumbPath' => '@staticRoot/cache/map',
                'thumbUrl' => '@static/cache/map',
                'thumbs' => [
                    'grid' => ['width' => 100, 'height' => 64, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'report' => ['width' => 190, 'height' => 130, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                ],
            ],
        ];
    }


    public function getImageSize()
    {
        if (! is_file(Yii::getAlias('@staticRoot/origin/map/'.$this->image)))
            return [];

        $size = getimagesize(Yii::getAlias('@staticRoot/origin/map/'.$this->image));
        return ['width' => $size[0], 'height' =>$size[1]];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }


    public function getLand(): ActiveQuery
    {
        return $this->hasOne(Land::class, ['id' => 'land_id']);
    }

    public function getPoints(): ActiveQuery
    {
        return $this->hasMany(Points::class, ['lands_map_id' => 'id']);
    }
}
