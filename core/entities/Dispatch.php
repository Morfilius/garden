<?php
namespace core\entities;

use core\entities\User\Bitrix24\Profile;
use core\entities\User\User;
use DateTime;
use Imagine\Image\ManipulatorInterface;
use mohorev\file\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * News model
 *
 * @property integer $id
 * @property integer $image_id
 * @property integer $recipient_id
 * @property string $title
 * @property string $date_start
 * @property string $date_end
 * @property string $text
 * @property string $banner_title
 * @property string $banner_show
 * @property string $image
 * @property string $image_wide
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */

class Dispatch extends ActiveRecord
{
    static $cache = [];

    public static function create($title, $banner_title, $banner_show, $recipient_id, $date_start, $date_end, $text, $image, $image_wide)
    {
        $dispatch = new self();
        $dispatch->title = $title;
        $dispatch->banner_title = $banner_title;
        $dispatch->banner_show = $banner_show;
        $dispatch->recipient_id = $recipient_id;
        $dispatch->date_start = (new DateTime($date_start))->format('Y-m-d');
        $dispatch->date_end = (new DateTime($date_end))->format('Y-m-d');
        $dispatch->text = $text;
        $dispatch->image = $image;
        $dispatch->image_wide = $image_wide;
        return $dispatch;
    }

    public function edit($title, $banner_title, $banner_show, $recipient_id, $date_start, $date_end, $text, $image, $image_wide)
    {
        $this->title = $title;
        $this->banner_title = $banner_title;
        $this->banner_show = $banner_show;
        $this->recipient_id = $recipient_id;
        $this->date_start = (new DateTime($date_start))->format('Y-m-d');
        $this->date_end = (new DateTime($date_end))->format('Y-m-d');
        $this->text = $text;
        $this->image = $image;
        $this->image_wide = $image_wide;
    }

    public static function tableName()
    {
        return '{{%dispatch}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'image',
                'createThumbsOnSave' => false,
                'createThumbsOnRequest' => true,
                'scenarios' => ['default'],
                'placeholder' => '@staticRoot/default.png',
                'path' => '@staticRoot/origin/dispatch',
                'url' => '@static/origin/dispatch',
                'thumbPath' => '@staticRoot/cache/dispatch',
                'thumbUrl' => '@static/cache/dispatch',
                'thumbs' => [
                    'grid' => ['width' => 100, 'height' => 64, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'wide' => ['width' => 999, 'height' => 186, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'small' => ['width' => 240, 'height' => 117, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'inner' => ['width' => 434, 'height' => 186, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'popup' => ['width' => 400, 'height' => 195, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                ],
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'image_wide',
                'createThumbsOnSave' => false,
                'createThumbsOnRequest' => true,
                'scenarios' => ['default'],
                'placeholder' => '@staticRoot/default.png',
                'path' => '@staticRoot/origin/dispatch',
                'url' => '@static/origin/dispatch',
                'thumbPath' => '@staticRoot/cache/dispatch',
                'thumbUrl' => '@static/cache/dispatch',
                'thumbs' => [
                    'grid' => ['width' => 100, 'height' => 64, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'wide' => ['width' => 999, 'height' => 186, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'small' => ['width' => 240, 'height' => 117, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'inner' => ['width' => 434, 'height' => 186, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'popup' => ['width' => 400, 'height' => 195, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                ],
            ],
        ];
    }

    /**
     * Устанавливаем кэш вида Profile[], необходим для оптимизации запросов к bitrix24 (2 запроса/сек)
     * @param  $array Profile[]
     */
    public static function cache($array = null)
    {
        if ($array && is_array($array)) {
            self::$cache = ArrayHelper::merge(self::$cache, ArrayHelper::index($array, 'id'));
        }
    }
    
    public static function cacheByBitrixId(array $ids)
    {
        self::cache(Profile::listByIds($ids));
    }

    public function getUserFromCache($bitrix_id)
    {
        return self::$cache[$bitrix_id];
    }
    
    ///////////////////////

    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'recipient_id']);
    }
}
