<?php
namespace core\entities\User;

use core\entities\Land\Land;
use core\entities\User\Bitrix24\Employee;
use core\repositories\User\UserRepository;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use core\entities\User\Bitrix24\Profile;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $bitrix_id
 * @property integer $manager_id
 * @property string $password write-only password
 *
 * @property Profile $profile
 * @property Token $token
 * @property Token[] $tokens
 * @property Land $land
 *
 */
class User extends ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    static $cache = [];

    public static function signup(string $username, string $password) : self
    {
        $user = new self();
        $user->username = $username;
        $user->setPassword($password);
        $user->status = self::STATUS_ACTIVE;
        $user->generateAuthKey();
        return $user;
    }

    /**
     * Устанавливаем кэш вида Profile[], необходим для оптимизации запросов к bitrix24 (2 запроса/сек)
     * @param  $array Profile[]
     */
    public static function cache($array = null)
    {
        if ($array && is_array($array)) {
            self::$cache = ArrayHelper::merge(self::$cache, ArrayHelper::index($array, 'id'));
        }
    }

    public static function cacheByBitrixId(array $ids)
    {
        self::cache(Profile::listByIds($ids));
    }

    public function edit(string $username, string $password)
    {
        $this->username = $username;
        !$password ?: $this->setPassword($password);
    }

    public function assignProfile($bitrix_id)
    {
        $this->bitrix_id = $bitrix_id;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public static function restLogin($login, $password, $deviceId)
    {
        /** @var UserRepository $userRepo*/
        $userRepo = Yii::$container->get(UserRepository::class);
        $user = $userRepo->getByUsername($login);
        $user->validatePassOrDie($password);
        return $user->generateToken($deviceId);
    }

    public function generateToken($deviceId)
    {
        $token = Token::create($this->id, $deviceId);
        $token->saveOrDie();
        return $token->getToken();
    }
    
    public function findActiveByUsername($username): ?User
    {
        return User::findOne(['username' => $username, 'status' => User::STATUS_ACTIVE]);
    }

    public function getManager() : Employee
    {
        return isset(self::$cache[$this->manager_id]) ? self::$cache[$this->manager_id] : (new Employee())->get($this->manager_id);
    }

    public function isManagerChange(int $manager_id)
    {
        return $this->manager_id != $manager_id;
    }

    public function managerChange($manager_id)
    {
        return $this->manager_id = $manager_id;
    }

    ///////////////////////
    
    public function getProfile()
    {
        return isset(self::$cache[$this->bitrix_id]) ? self::$cache[$this->bitrix_id] : (new Profile)->get($this->bitrix_id);
    }

    public function getToken()
    {
        return $this->hasOne(Token::className(), ['user_id' => 'id']);
    }

    public function getLand()
    {
        return $this->hasOne(Land::class, ['owner_id' => 'id']);
    }

    public function getLandOrFail() : ?Land
    {
        if (empty($this->land)) {
            throw new NotFoundHttpException('Land not found');
        }
        return $this->land;
    }

    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }

    ///////////////////////

    public static function tableName()
    {
        return '{{%users}}';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => [
                    'profile',
                ],
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    private function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function validatePassOrDie($password)
    {
        if (!$return = Yii::$app->security->validatePassword($password, $this->password_hash)) {
            throw new UnauthorizedHttpException('Username or Password is not valid');
        }
        return $return;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    private function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
}
