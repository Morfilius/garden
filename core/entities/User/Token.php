<?php


namespace core\entities\User;


use Yii;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;

/**
 * @property string $device_id
 * @property string $token
 * @property integer $user_id
 *
*/

class Token extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%tokens}}';
    }

    public static function create($user_id, $device_id)
    {
        self::deleteAll(['device_id' => $device_id]);
        $self = new self();
        $self->device_id = $device_id;
        $self->token = Yii::$app->security->generateRandomString();
        $self->user_id = $user_id;
        return $self;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function saveOrDie()
    {
        if (!$this->save()) throw new ServerErrorHttpException('Token save error.');
    }
}