<?php


namespace core\entities\User\Bitrix24;


use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\Helpers\Bitrix24Helper;

class ProfileResponse
{
    public $id;
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $image;
    public $info;
    public $phone_id;
    public $email_id;
    public $manager_id;

    public function __construct(array $response)
    {
        $this->id = $response['ID'];
        $this->first_name = $response['NAME'];
        $this->last_name = $response['LAST_NAME'];
        $this->phone = $response['PHONE'][0]['VALUE'] ?? '';
        $this->email = $response['EMAIL'][0]['VALUE'] ?? '';
        $this->info = $response['COMMENTS'] ?? '';
        $this->phone_id = $response['PHONE'][0]['ID'] ?? '';
        $this->email_id = $response['EMAIL'][0]['ID'] ?? '';
        $this->manager_id = $response['ASSIGNED_BY_ID'] ?? '';
        $this->image = isset($response['PHOTO']['downloadUrl']) ? Bitrix24Helper::base64ResponseImg(rtrim(BitrixApi::getClientUrl(), '/') . $response['PHOTO']['downloadUrl']) : '';
    }
}