<?php


namespace core\entities\User\Bitrix24;


use core\entities\BaseBitrix24;

/**
 * @property EmployeeStorage $storage;
 * */

class Employee extends BaseBitrix24
{
    public $first_name;
    public $last_name;
    public $image;

    private $cache = [];

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->setStorage(new EmployeeStorage());
    }

    private static function create($first_name, $last_name, $image)
    {
        $self = new self();
        $self->first_name = $first_name;
        $self->last_name = $last_name;
        $self->image = $image;
        return $self;
    }

    public static function getList()
    {
        return array_map(function ($item) {
            return [$item['ID'] => $item['NAME'] . ' ' . $item['LAST_NAME']];
        }, (new self())->list());
    }
    
    public function list()
    {
        return $this->storage->showList();
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function get($id)
    {
        if (empty($this->cache[$id])) {
            $response = $this->storage->showById($id);
            $this->cache[$id] = (!empty($response)) ? self::create($response->first_name, $response->last_name, $response->image) : new self();
        }
        return $this->cache[$id];
    }
}