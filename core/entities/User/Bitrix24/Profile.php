<?php


namespace core\entities\User\Bitrix24;


use core\entities\BaseBitrix24;
use core\entities\User\User;
use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\Helpers\Bitrix24Helper;
use DomainException;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Profile extends BaseBitrix24
{
    public $id;
    public $first_name;
    public $last_name;
    public $phone;
    public $image;
    public $email;
    public $info;
    public $manager_id;

    public $_phone_id;
    public $_email_id;

    private $cache = [];

    public function __construct($config = [])
    {
        $this->setStorage(new ProfileStorage());
        parent::__construct($config);
    }

    public static function createNew($first_name, $last_name, $phone, $email, $info = null, $image = null) :self
    {
        $self = new self();
        $self->first_name = $first_name;
        $self->last_name = $last_name;
        $self->phone = $phone;
        $self->email = $email;
        $self->image = $image;
        $self->info = $info;
        return $self;
    }

    public static function create($first_name, $last_name, $phone, $email, $id, $manager_id,
                                  $phone_id = null, $email_id = null, $image = null, $info = null) :self
    {
        $self = self::createNew($first_name, $last_name, $phone, $email, $info, $image);
        $self->id = $id;
        $self->manager_id = $manager_id;
        $self->_phone_id = $phone_id;
        $self->_email_id = $email_id;
        return $self;
    }

    public function edit($first_name, $last_name, $phone, $email, $info,
                         $phone_id = null, $email_id = null, $image = null)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->phone = $phone;
        $this->email = $email;
        $this->info = $info;
        $this->image = $image ? Bitrix24Helper::formatImg($image, $this->id, Yii::getAlias('@staticRoot/bitrix/' . $this->id . '.jpg'), 124, 124) : null;
        $this->_phone_id = $phone_id;
        $this->_email_id = $email_id;
    }

    public static function editRest($bitrix_id, $first_name, $last_name, $phone, $email, $image = null)
    {
        $self = new self();
        $self->id = $bitrix_id;
        $self->first_name = $first_name;
        $self->last_name = $last_name;
        $self->phone = $phone;
        $self->email = $email;
        $self->image = $image ? Bitrix24Helper::formatImg($image, $bitrix_id, Yii::getAlias('@staticRoot/bitrix/' . $bitrix_id . '.jpg'), 124, 124) : null;
        return $self;
    }
    
    public function editOperator($first_name, $last_name, $phone, $staff, $phone_id = null, $image = null)
    {
        $this->setStorage(new EmployeeStorage());
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->phone = $phone;
        $this->image = $image ? Bitrix24Helper::formatImg($image, $this->id, Yii::getAlias('@staticRoot/bitrix/' . $this->id . '.jpg'), 124, 124) : null;
        $this->info = $staff;
    }

    public function get($id)
    {
        if (empty($this->cache[$id])) {
            $response  = $this->storage->showById($id);
            $this->cache[$id] = (!empty($response)) ? self::create($response->first_name, $response->last_name, $response->phone,
                $response->email, $response->id, $response->manager_id, $response->phone_id, $response->email_id, $response->image, $response->info) : new self();
        }
        return $this->cache[$id];
    }

    public function list(array $filter = null)
    {
        return $this->storage->showList($filter);
    }

    public static function listByIds(array $ids)
    {
        return (new self())->list(['ID' => $ids]);
    }

    public static function clientList()
    {
        $query = (new Query())->select(['id', 'bitrix_id'])->from(User::tableName())
            ->leftJoin('auth_assignment', 'auth_assignment.user_id = users.id')->where(['auth_assignment.user_id' => null])->all();
        $userIdByClientId = ArrayHelper::index($query, 'bitrix_id');
        $client_ids = ArrayHelper::getColumn((new Query())->select(['id', 'bitrix_id'])->from(User::tableName())->leftJoin('auth_assignment', 'auth_assignment.user_id = users.id')->where(['auth_assignment.user_id' => null])->all(), 'bitrix_id');

        return array_map(function ($client) use($userIdByClientId) {
            $userId = $userIdByClientId[$client->id]['id'];
            return [$userId => $client->first_name . ' ' . $client->last_name];
        }, self::listByIds($client_ids));
    }

    public function insert()
    {
        return $this->storage->insert(['ID' => $this->id, 'fields' => [
            'NAME' => $this->first_name,
            'LAST_NAME' => $this->last_name,
            'EMAIL' => $this->email ? [["VALUE" => $this->email, "VALUE_TYPE" => "WORK" ]] : false,
            'PHONE' => $this->phone ? [["ID" => $this->_phone_id, "VALUE" => $this->phone, "VALUE_TYPE" => "WORK" ]] : false,
            'COMMENTS' => $this->info ?? false,
        ]]);
    }

    public function update()
    {
        $query = [
            'contact' => [
                'method' => 'crm.contact.get',
                'params' => [
                    'ID' => $this->id,
                ]
            ],
            'update' => [
                'method' => 'crm.contact.update',
                'params' => [
                    'ID' => $this->id,
                    'fields' => array_filter([
                        'NAME' => $this->first_name,
                        'LAST_NAME' => $this->last_name,
                        'EMAIL' => ($this->email) ? [["ID" => '$result[contact][EMAIL][0][ID]', "VALUE" => $this->email, "VALUE_TYPE" => "WORK" ]] : false,
                        'PHONE' => ($this->phone) ? [["ID" => '$result[contact][PHONE][0][ID]', "VALUE" => $this->phone, "VALUE_TYPE" => "WORK" ]] : false,
                        'COMMENTS' => $this->info ?? false,
                        'PHOTO' => $this->image ?? false
                    ])
                ]
            ],
        ];
        BitrixApi::callBatch($query);
    }

    public function updateEmployee()
    {
        $query = [
            'contact' => [
                'method' => 'user.get',
                'params' => [
                    'ID' => $this->id,
                ]
            ],
            'update' => [
                'method' => 'user.update',
                'params' => [
                    'ID' => $this->id,
                    'NAME' => $this->first_name,
                    'LAST_NAME' => $this->last_name,
                    'WORK_PHONE' => $this->phone,
                    'WORK_POSITION' => $this->info ?? false,
                ]
            ],
        ];
        BitrixApi::callBatch($query);
    }

    public function image()
    {
        return  $this->image ?? Yii::getAlias('@static/default.png');
    }
}