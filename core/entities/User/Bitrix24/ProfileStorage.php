<?php


namespace core\entities\User\Bitrix24;


use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\BitrixStorageInterface;
use DomainException;

class ProfileStorage implements BitrixStorageInterface
{
    public function showById($id)
    {
        $response = BitrixApi::call('crm.contact.get', ['ID' => $id]);
        if ($response && is_array($response)) {
            return new ProfileResponse($response);
        }
        return false;
    }

    public function showList(array $filter = null)
    {
        $responses = BitrixApi::call('crm.contact.list', array_filter([
            'SELECT' => ['ID', 'NAME', 'LAST_NAME', 'PHONE', 'EMAIL', 'COMMENTS'],
            'FILTER' => $filter,
            ])
        );
        if ($responses && is_array($responses)) {
            return array_map(function ($response) {
                return new ProfileResponse($response);
            }, $responses);
        }
        return false;
    }

    public function insert(array $array)
    {
        if ($response = BitrixApi::call('crm.contact.add', $array)) {
            return $response;
        }
        throw new DomainException('Insert error');
    }

    public function update(array $array)
    {
        if ($response = BitrixApi::call('crm.contact.update', $array)) {
            return $response;
        }
        throw new DomainException('Update error');
    }
}