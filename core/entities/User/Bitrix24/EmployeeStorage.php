<?php


namespace core\entities\User\Bitrix24;


use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\BitrixStorageInterface;
use DomainException;

class EmployeeStorage implements BitrixStorageInterface
{
    static $work_position = 'Бригадир';

    public function showById($id)
    {
        $response = BitrixApi::call('user.get', ['ID' => $id]);
        if ($response && is_array($response)) {
            return new EmployeeResponse(isset($response[0]) ? $response[0] : $response);
        }
        return false;
    }

    public function showList(array $filter = null)
    {
        return BitrixApi::call('user.get', array_merge([
            'WORK_POSITION' => self::$work_position,
        ], $filter ?? []));
    }

    public function insert(array $array)
    {
        // TODO: Implement insert() method.
    }

    public function update(array $array)
    {
        $request = [
            'ID' => $array['ID'],
            'NAME' => $array['fields']['NAME'],
            'LAST_NAME' => $array['fields']['LAST_NAME'],
            'WORK_PHONE' => $array['fields']['PHONE'][0]['VALUE'] ?? false,
            'WORK_POSITION' => $array['fields']['COMMENTS'] ?? false,
            'PERSONAL_PHOTO' => $array['fields']['PHOTO'] ?? false
        ];

        if ($response = BitrixApi::call('user.update', array_filter($request))) {
            return $response;
        }
        throw new DomainException('Update error');
    }
}