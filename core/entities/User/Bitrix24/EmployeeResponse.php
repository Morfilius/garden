<?php


namespace core\entities\User\Bitrix24;


use Yii;

class EmployeeResponse extends ProfileResponse
{
    public function __construct(array $response)
    {
        $this->id = $response['ID'];
        $this->first_name = $response['NAME'];
        $this->last_name = $response['LAST_NAME'];
        $this->phone = $response['WORK_PHONE'];
        $this->email = $response['EMAIL'];
        $this->image = $response['PERSONAL_PHOTO'] ?? Yii::getAlias('@static/default.png');
        $this->info = $response['WORK_POSITION'];
    }
}