<?php
namespace core\entities;

use core\behaviors\UploadImageBehavior;
use Imagine\Image\ManipulatorInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * News model
 *
 * @property integer $id
 * @property string $number
 * @property string $text
 * @property string $image
 * @property integer $created_at
 * @property integer $updated_at
 *
 */

class Banners extends ActiveRecord
{
    public static function create($text, $number,  $image)
    {
        $news = new self();
        $news->text = $text;
        $news->image = $image;
        $news->number = $number;
        return $news;
    }

    public function edit($text, $image)
    {
        $this->text = $text;
        $this->image = $image;
    }

    public function hasBanner($number)
    {
        return self::find()->where(['number' => $number])->exists();
    }

    public static function tableName()
    {
        return '{{%banners}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'image',
                'createThumbsOnSave' => false,
                'createThumbsOnRequest' => true,
                'scenarios' => ['default'],
                'placeholder' => '@staticRoot/default.png',
                'path' => '@staticRoot/origin/banners',
                'url' => '@static/origin/banners',
                'thumbPath' => '@staticRoot/cache/banners',
                'thumbUrl' => '@static/cache/banners',
                'thumbs' => [
                    'grid' => ['width' => 100, 'height' => 64, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'home' => ['width' => 480, 'height' => 170, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'slider' => ['width' => 1000, 'height' => 272, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                    'banner' => ['width' =>  617, 'height' => 107, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                ],
            ],
        ];
    }
}
