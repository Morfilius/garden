<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;

AppAsset::register($this);
$cookie = $_COOKIE;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=1500">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body <?= (isset($cookie['collapse']) && $cookie['collapse'] === 'true') ? 'class = "sidebar-collapse"' : ''?>>
<?php $this->beginBody() ?>

<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <a href="/" class="i-mini-logo">
            <img src="<?= Url::to('@web/img/mini-logo.svg')?>" alt="" class="i-mini-logo__img">
        </a>
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link burger-menu-link" data-widget="pushmenu" href="#"><span class="burger-menu"></span></a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav align-items-center ml-auto">
            <li class="nav-item top-nav-item">
                    <?= Html::a('<span class="top-menu-link__img" style="background-image: url('.Url::to('@web/img/exit.svg').')"></span>', ['/auth/logout'], [
                        'class' => 'top-menu-link',
                        'data' => [
                            'confirm' => 'Вы действительно хотите выйти?',
                            'method' => 'post',
                        ],
                    ])?>
            </li>
            <li class="nav-item top-nav-item">
                <a class="top-menu-link top-menu-link--avatar" href="<?= Url::to('/profile/update') ?>">
                    <span class="top-menu-link__img person" style="background-image: url(<?= Url::to('@web/img/person.svg')?>)"></span>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar i-sidebar">
        <!-- Brand Logo -->
        <a href="/" class="brand-link i-brand">
            <img src="<?= Url::to('@web/img/logo-sidebar.svg')?>" alt="" class="brand-image i-brand__img">
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav>
                <?= Menu::widget([
                    'items' => [
                        [
                            'label' => 'Новости и акции',
                            'url' => ['/news'],
                            'active' => in_array($this->context->route,['news/index']),
                            'template' => '<a href="{url}" class="nav-link i-nav-link">
                                                <span class="menu-icon" style="background-image: url('. Url::to('@web/img/sales.svg').')"></span>
                                                <span class="menu-text">
                                                    {label}
                                                </span>
                                            </a>'
                        ],
                        [
                            'label' => 'Рассылки',
                            'url' => ['/dispatch'],
                            'active' => in_array($this->context->route,['dispatch/index']),
                            'template' => '<a href="{url}" class="nav-link i-nav-link">
                                                <span class="menu-icon" style="background-image: url('. Url::to('@web/img/map.svg').')"></span>
                                                <span class="menu-text">
                                                    {label}
                                                </span>
                                            </a>'
                        ],
                        [
                            'label' => 'База пользователей',
                            'url' => ['/user'],
                            'active' => in_array($this->context->route,['user/index']),
                            'template' => '<a href="{url}" class="nav-link i-nav-link">
                                                <span class="menu-icon" style="background-image: url('. Url::to('@web/img/users.svg').')"></span>
                                                <span class="menu-text">
                                                    {label}
                                                </span>
                                            </a>'
                        ],
                        [
                            'label' => 'Карты участков',
                            'url' => ['/land/info'],
                            'active' => in_array($this->context->route,['land/info/index']),
                            'template' => '<a href="{url}" class="nav-link i-nav-link">
                                                <span class="menu-icon" style="background-image: url('. Url::to('@web/img/map-menu.svg').')"></span>
                                                <span class="menu-text">
                                                    {label}
                                                </span>
                                            </a>'
                        ],
                        [
                            'label' => 'Баннеры',
                            'url' => ['/banner/index'],
                            'active' => in_array($this->context->route,['banner/index']),
                            'template' => '<a href="{url}" class="nav-link i-nav-link">
                                                <span class="menu-icon" style="background-image: url('. Url::to('@web/img/banner.svg').')"></span>
                                                <span class="menu-text">
                                                    {label}
                                                </span>
                                            </a>'
                        ],
                    ],
                    'itemOptions' => [
                        'class' => 'nav-item'
                    ],
                    'options' => [
                        'class' => 'nav nav-pills nav-sidebar flex-column',
                        'data-widget' => 'treeview',
                        'data-accordion' => 'false',
                    ],
                    'activeCssClass'=>'active'
                ])?>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
            <?= $content ?>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
