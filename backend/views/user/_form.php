<?
/* @var $this View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;


$form = ActiveForm::begin();
 ?>
    <div class="save-btn-wrap">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-block btn-default btn-red btn-save']) ?>
    </div>
    <div class="row">
        <div class="col-7">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model->profile,'first_name')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Введите имя',

                    ]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model->profile,'phone', [
                        'inputOptions' => ['class' => 'form-control phone']
                    ])->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Введите телефон',
                        'value' => $model->profile->phone ? substr($model->profile->phone, 1) : ''
                    ]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model->profile,'last_name')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Введите фамилию',

                    ]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model->profile,'email')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Введите E-mail',
                    ]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model,'username')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Введите логин',
                    ]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model,'password')->passwordInput([
                        'maxlength' => true,
                        'placeholder' => 'Введите пароль',
                    ]) ?>
                </div>
                <div class="col-12">
                    <?= $form->field($model->profile,'info')->textarea([
                        'rows' => 6,
                        'maxlength' => true,
                    ]) ?>
                </div>
                <?= $form->field($model->profile, '_phone_id', ['template' => '{input}'])->hiddenInput()?>
                <?= $form->field($model->profile, '_email_id', ['template' => '{input}'])->hiddenInput()?>
            </div>
        </div>
    </div>
<? ActiveForm::end()?>