<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Пользователи';
?>
<section class="content">
    <div class="container-fluid position-relative">
        <div class="publish-btn-wrap">
            <?= $this->render('_search', ['searchModel' => $searchModel])?>
            <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-block btn-default btn-red btn-user'])?>
        </div>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}<div class="grid-bottom">{summary}<nav class="pagination-wrap">{pager}</nav></div>',
            'tableOptions' => [
                    'class' => 'main-grid users-grid'
            ],
            'headerRowOptions' => [
                'class' => 'main-grid__row-head'
            ],
            'rowOptions' => [
                'class' => 'main-grid__row',
            ],
            'pager' => [
                'linkContainerOptions' => [
                    'class' => 'page-item'
                ],
                'linkOptions' => [
                    'class' => 'page-link'
                ],
                'disabledPageCssClass' => 'page-link'
            ],
            'columns' => [
                [
                    'attribute' => 'profile.last_name',
                    'label' => 'Фамилия',
                    'enableSorting' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                ],
                [
                    'attribute' => 'profile.first_name',
                    'label' => 'Имя',
                    'enableSorting' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                ],
                [
                    'attribute' => 'username',
                    'label' => 'Логин',
                    'enableSorting' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                ],
                [
                    'format' => 'raw',
                    'enableSorting' => false,
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td vam'],
                    'value' => function ($model) {
                        return '<span class="main-grid__actions">'.
                            Html::a('', ['update', 'id' => $model->id], [
                                'class' => 'main-grid__action',
                                'style' => 'background-image: url('.Url::to('@web/img/edit.svg').')',
                            ]).
                            Html::a('', ['delete', 'id' => $model->id], [
                                'class' => 'main-grid__action',
                                'style' => 'background-image: url('.Url::to('@web/img/trash.svg').')',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить пользователя?',
                                    'method' => 'post',
                                ]]).'
                                </span>';
                    }
                ],
            ],
        ]); ?>
    </div>
</section>