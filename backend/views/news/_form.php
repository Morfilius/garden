<?
/**@var NewsForm $model*/

use core\forms\manage\NewsForm;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin();?>
    <div class="save-btn-wrap">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-block btn-default btn-red btn-save']) ?>
    </div>
    <div class="row">
        <div class="col-2">
            <?= $form->field($model, 'date_start', [
                'template' => "{label}\n<div class=\"datepicker-wrap\">{input}</div>\n{hint}\n{error}",
            ])->textInput([
                'value' => $model->date_start ? (new DateTime($model->date_start))->format('d.m.Y') : '',
                'maxlength' => true,
                'class' => 'form-control date',
                'placeholder' => 'дд.мм.гггг',
            ]) ?>
        </div>
        <div class="col-2">
            <?= $form->field($model, 'date_end', [
                'template' => "{label}\n<div class=\"datepicker-wrap\">{input}</div>\n{hint}\n{error}"
            ])->textInput([
                'value' => $model->date_end ? (new DateTime($model->date_end))->format('d.m.Y') : '',
                'maxlength' => true,
                'class' => 'form-control date',
                'placeholder' => 'дд.мм.гггг',
            ]) ?>
        </div>
        <div class="col-11">
            <?= $form->field($model, 'title')->textInput([
                'maxlength' => true,
                'placeholder' => 'Заголовок новости',
            ]) ?>
        </div>
        <div class="col-10">
            <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder',['height' => 400]),
            ])->textarea([
                'maxlength' => true,
                'rows' => 7
            ]);?>
        </div>
    </div>
<?= $form->field($model->image, 'image', [
    'options' => [
        'class' => 'file-upload'
    ],
    'template' => '<a href="#" class="btn btn-red btn-upload btn-round">Загрузить фотографию с компьютера 999x186</a>{input}{error}'
])->fileInput() ?>
<? ActiveForm::end() ?>