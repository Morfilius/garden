<?/* @var $searchModel backend\forms\NewsSearch */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm; ?>
<? $form = ActiveForm::begin([
    'method' => 'get',
    'action' => [Url::current(['search' => null])],
    'options' => [
        'class' => 'form-search mr0'
    ]
])?>
    <?= $form->field($searchModel, 'search', [
        'template' => "{input}",
])-> textInput([
    'class' => 'form-control form-search__input',
    'placeholder' => 'Поиск по растениям',
])?>
    <?= Html::submitButton('', ['class' => 'form-search__btn']) ?>
<? ActiveForm::end()?>