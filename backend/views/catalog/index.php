<?php

use core\entities\News;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $searchModel \backend\forms\CatalogSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var integer $landId  */
/* @var integer $point_id  */

$this->title = 'Каталог растений';
$this->params['backUrl'] = Url::to(['land/editor/garden', 'id' => $landId]);
?>

<div class="inner-content garden-inner-content">
    <div class="publish-btn-wrap garden-search">
        <?= $this->render('_search', [
            'searchModel' =>$searchModel
        ])?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}<div class="grid-bottom">{summary}<nav class="pagination-wrap">{pager}</nav></div>',
        'tableOptions' => [
            'class' => 'main-grid'
        ],
        'headerRowOptions' => [
            'class' => 'main-grid__row-head'
        ],
        'rowOptions' => [
            'class' => 'main-grid__row',
        ],
        'pager' => [
            'linkContainerOptions' => [
                'class' => 'page-item'
            ],
            'linkOptions' => [
                'class' => 'page-link'
            ],
            'disabledPageCssClass' => 'page-link'
        ],
        'columns' => [
            [
                'attribute' => 'image',
                'label' => 'Изображение',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions' => ['class' => 'main-grid__th'],
                'contentOptions' => ['class' => 'main-grid__td'],
                'value' => function($model) {
                    /** @var News $model*/
                    return '<div class="main-grid__img-wrap">
                                   <img src="'. ($model->image ? $model->getUploadUrl('image') : Url::to('@static/default.png')) .'" class="main-grid__img" alt="">
                                </div>';
                }
            ],
            [
                'attribute' => 'title',
                'label' => 'Название растения',
                'enableSorting' => false,
                'filter' => false,
                'headerOptions' => ['class' => 'main-grid__th'],
                'contentOptions' => ['class' => 'main-grid__td'],
            ],
            [
                'format' => 'raw',
                'enableSorting' => false,
                'filter' => false,
                'headerOptions' => ['class' => 'main-grid__th'],
                'contentOptions' => ['class' => 'main-grid__td vam tar'],
                'value' => function($model) use($landId, $point_id) {
                    return '<a href="'. Url::to(['land/editor/garden', 'id' => $landId, 'catalog_id' => $model->id, 'point_id' => $point_id]) .'" class="btn btn-default btn-red btn-save">Выбрать</a>';
                }
            ],

        ],
    ]);?>
</div>
