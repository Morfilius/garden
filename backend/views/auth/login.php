<?php

use core\forms\auth\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model \core\forms\auth\LoginForm */
/* @var $model LoginForm */

?>
<div class="login-container">
    <div class="login-logo-wrap">
        <img src="<?= Yii::getAlias('@web/img/logo-big.svg') ?>" class="logo-login" alt="">
    </div>
    <div class="login-form-wrap">

        <? $form = ActiveForm::begin([
            'options' => ['class' => 'form-login']
        ]) ?>
        <form action="/" class="form-login">
            <div class="tac form-login__title-wrap">
                <span class="form-login__title">Вход</span>
            </div>
            <div class="form-group form-login__group">
                <?= $form->field($model, 'username')->textInput([
                    'placeholder' => 'Введите логин',
                ]) ?>
            </div>
            <div class="form-group form-login__group">
                <?
                $eye = Yii::getAlias('@web/img/eye.svg');
                echo $form->field($model, 'password', [
                    'template' => "{label}\n
                         <div class=\"pwd-eye-wrap\">
                            {input}
                            <span class=\"pwd-eye\"><img src=\"$eye\" alt=\"\" class=\"pwd-eye__img\"></span>
                         </div>\n
                        {hint}\n
                        {error}"
                ])->passwordInput([
                    'placeholder' => 'Введите пароль',
                ]) ?>
            </div>
            <div class="form-login__btn-wrap">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-block btn-default btn-red']) ?>
            </div>
            <? ActiveForm::end() ?>
    </div>
</div>