<?php
/* @var $this View */
use core\forms\manage\User\UserCreateForm;
use yii\helpers\Url;
use yii\web\View;

/* @var  $model UserCreateForm*/
$this->title = 'Редактирование пользователя';
$this->params['backUrl'] = Url::to(['/banner']);
?>

<div class="inner-content inner-bg">
    <?= $this->render('_form', [
            'model' => $model
    ]) ?>
</div>
