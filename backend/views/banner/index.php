<?php
/* @var $this View */

use core\entities\Banners;
use yii\web\View;

/* @var $model Banners */
/* @var $bannersList Banners[] */
$this->title = 'Баннеры';
?>
<section class="content">
    <div class="container-fluid position-relative">
        <div id="w0" class="grid-view">
            <table class="main-grid">
                <thead>
                    <tr class="main-grid__row-head">
                        <th class="main-grid__th">Изображение</th>
                        <th class="main-grid__th">Заголовок раздела и размер баннера</th>
                        <th class="main-grid__th">Дата изменения</th>
                        <th class="main-grid__th"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="main-grid__row" data-key="1">
                        <td class="main-grid__td">
                            <div class="main-grid__img-wrap">
                                <img src="<?= $bannersList[1] ? $bannersList[1]->getThumbUploadUrl('image', 'grid') : '' ?>" class="main-grid__img" alt="">
                            </div>
                        </td>
                        <td class="main-grid__td">Главная / Персональные предложения (480х170)</td>
                        <td class="main-grid__td"><?= $bannersList[1] ? date('d.m.Y', $bannersList[1]->updated_at) : '-' ?></td>
                        <td class="main-grid__td vam">
                            <span class="main-grid__actions">
                                <a class="main-grid__action" href="<?= $model->hasBanner(1) ? '/banner/update/1' : '/banner/create' ?>" style="background-image: url(/img/edit.svg)"></a>
                            </span>
                        </td>
                    </tr>
                    <tr class="main-grid__row" data-key="1">
                        <td class="main-grid__td">
                            <div class="main-grid__img-wrap">
                                <img src="<?= $bannersList[2] ? $bannersList[2]->getThumbUploadUrl('image', 'grid') : '' ?>" class="main-grid__img" alt="">
                            </div>
                        </td>
                        <td class="main-grid__td">Главная / Новости и акции (480х170)</td>
                        <td class="main-grid__td"><?= $bannersList[2] ? date('d.m.Y', $bannersList[2]->updated_at) : '-' ?></td>
                        <td class="main-grid__td vam">
                            <span class="main-grid__actions">
                                <a class="main-grid__action" href="<?= $model->hasBanner(2) ? '/banner/update/2' : '/banner/create' ?>" style="background-image: url(/img/edit.svg)"></a>
                            </span>
                        </td>
                    </tr>
                    <!--tr class="main-grid__row" data-key="1">
                        <td class="main-grid__td">
                            <div class="main-grid__img-wrap">
                                <img src="<?= $bannersList[41] ? $bannersList[41]->getThumbUploadUrl('image', 'grid') : '' ?>" class="main-grid__img" alt="">
                            </div>
                        </td>
                        <td class="main-grid__td">Персональные предложения (баннер 4.1)</td>
                        <td class="main-grid__td"><?= $bannersList[41] ? date('d.m.Y', $bannersList[41]->updated_at) : '-' ?></td>
                        <td class="main-grid__td vam">
                            <span class="main-grid__actions">
                                <a class="main-grid__action" href="<?= $model->hasBanner(41) ? '/banner/update/41' : '/banner/create' ?>" style="background-image: url(/img/edit.svg)"></a>
                            </span>
                        </td>
                    </tr>
                    <tr class="main-grid__row" data-key="1">
                        <td class="main-grid__td">
                            <div class="main-grid__img-wrap">
                                <img src="<?= $bannersList[42] ? $bannersList[42]->getThumbUploadUrl('image', 'grid') : '' ?>" class="main-grid__img" alt="">
                            </div>
                        </td>
                        <td class="main-grid__td">Персональные предложения (баннер 4.2)</td>
                        <td class="main-grid__td"><?= $bannersList[42] ? date('d.m.Y', $bannersList[42]->updated_at) : '-' ?></td>
                        <td class="main-grid__td vam">
                            <span class="main-grid__actions">
                                <a class="main-grid__action" href="<?= $model->hasBanner(42) ? '/banner/update/42' : '/banner/create' ?>" style="background-image: url(/img/edit.svg)"></a>
                            </span>
                        </td>
                    </tr>
                    <tr class="main-grid__row" data-key="1">
                        <td class="main-grid__td">
                            <div class="main-grid__img-wrap">
                                <img src="<?= $bannersList[43] ? $bannersList[43]->getThumbUploadUrl('image', 'grid') : '' ?>" class="main-grid__img" alt="">
                            </div>
                        </td>
                        <td class="main-grid__td">Персональные предложения (баннер 4.3)</td>
                        <td class="main-grid__td"><?= $bannersList[43] ? date('d.m.Y', $bannersList[43]->updated_at) : '-' ?></td>
                        <td class="main-grid__td vam">
                            <span class="main-grid__actions">
                                <a class="main-grid__action" href="<?= $model->hasBanner(43) ? '/banner/update/43' : '/banner/create' ?>" style="background-image: url(/img/edit.svg)"></a>
                            </span>
                        </td>
                    </tr>
                    <tr class="main-grid__row" data-key="1">
                        <td class="main-grid__td">
                            <div class="main-grid__img-wrap">
                                <img src="<?= $bannersList[44] ? $bannersList[44]->getThumbUploadUrl('image', 'grid') : '' ?>" class="main-grid__img" alt="">
                            </div>
                        </td>
                        <td class="main-grid__td">Персональные предложения (баннер 4.4)</td>
                        <td class="main-grid__td"><?= $bannersList[44] ? date('d.m.Y', $bannersList[44]->updated_at) : '-' ?></td>
                        <td class="main-grid__td vam">
                            <span class="main-grid__actions">
                                <a class="main-grid__action" href="<?= $model->hasBanner(44) ? '/banner/update/44' : '/banner/create' ?>" style="background-image: url(/img/edit.svg)"></a>
                            </span>
                        </td>
                    </tr-->
                    <tr class="main-grid__row" data-key="1">
                        <td class="main-grid__td">
                            <div class="main-grid__img-wrap">
                                <img src="<?= $bannersList[7] ? $bannersList[7]->getThumbUploadUrl('image', 'grid') : '' ?>" class="main-grid__img" alt="">
                            </div>
                        </td>
                        <td class="main-grid__td">Карта участка / Аудит сада (617х107)</td>
                        <td class="main-grid__td"><?= $bannersList[7] ? date('d.m.Y', $bannersList[7]->updated_at) : '-' ?></td>
                        <td class="main-grid__td vam">
                            <span class="main-grid__actions">
                                <a class="main-grid__action" href="<?= $model->hasBanner(7) ? '/banner/update/7' : '/banner/create' ?>" style="background-image: url(/img/edit.svg)"></a>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>