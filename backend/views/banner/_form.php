<?
/**@var NewsForm $model*/

use core\forms\manage\NewsForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin();?>
    <div class="save-btn-wrap">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-block btn-default btn-red btn-save']) ?>
    </div>
    <div class="row">
        <div class="col-11">
            <?= $form->field($model, 'text')->label('Текст на баннере')->textarea([
                'maxlength' => true
            ]) ?>
        </div>
    </div>
<?= $form->field($model->image, 'image', [
    'options' => [
        'class' => 'file-upload'
    ],
    'template' => '<a href="#" class="btn btn-red btn-upload btn-round">Загрузить фотографию с компьютера</a>{input}{error}'
])->fileInput() ?>
<? ActiveForm::end() ?>