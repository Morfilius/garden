<?
/* @var $this View */
/**@var DispatchForm $model*/

use core\entities\User\Bitrix24\Profile;
use core\forms\manage\DispatchForm;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin();?>
    <div class="save-btn-wrap">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-block btn-default btn-red btn-save']) ?>
    </div>
    <div class="row">
        <div class="col-2">
            <?= $form->field($model, 'date_start', [
                'template' => "{label}\n<div class=\"datepicker-wrap\">{input}</div>\n{hint}\n{error}",
            ])->textInput([
                'value' => $model->date_start ? (new DateTime($model->date_start))->format('d.m.Y') : '',
                'maxlength' => true,
                'class' => 'form-control date',
                'placeholder' => 'дд.мм.гггг',
            ]) ?>
        </div>
        <div class="col-2">
            <?= $form->field($model, 'date_end', [
                'template' => "{label}\n<div class=\"datepicker-wrap\">{input}</div>\n{hint}\n{error}"
            ])->textInput([
                'value' => $model->date_end ? (new DateTime($model->date_end))->format('d.m.Y') : '',
                'maxlength' => true,
                'class' => 'form-control date',
                'placeholder' => 'дд.мм.гггг',
            ]) ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'recipient_id')->listBox(Profile::clientList(),[
                'maxlength' => true,
                'class' => 'form-control select2'
            ]) ?>
        </div>
        <div class="col-11">
            <?= $form->field($model, 'title')->textInput([
                'maxlength' => true,
                'placeholder' => 'Заголовок новости',
            ]) ?>
        </div>
        <div class="col-9">
            <?= $form->field($model, 'banner_title')->textInput([
                'maxlength' => true,
                'placeholder' => 'Заголовок баннера',
            ]) ?>
        </div>
        <div class="col-3 d-flex">
            <div class="slide-checkbox settings-item m-auto" style="margin-left: 13px!important; padding-top: 10px">
                <input id="q1" name="<?= $model->formName()?>[banner_show]" value="1" <?= $model->banner_show ? 'checked': ''?> type="checkbox" class="slide-checkbox__input">
                <label for="q1" class="slide-checkbox__label"><span class="slide"></span><span class="slide-checkbox__text">Показывать баннер</span></label>
            </div>
        </div>

        <div class="col-10">
            <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder',['height' => 400]),
            ])->textarea([
                'maxlength' => true,
                'rows' => 7
            ]);?>
        </div>
    </div>
<?= $form->field($model->image, 'image', [
    'options' => [
        'class' => 'file-upload'
    ],
    'template' => '<a href="#" class="btn btn-red btn-upload btn-round">Загрузить фотографию с компьютера 434x168</a>{input}{error}'
])->fileInput() ?>
<?= $form->field($model->image_wide, 'image', [
    'options' => [
        'class' => 'file-upload'
    ],
    'template' => '<a href="#" class="btn btn-red btn-upload btn-round">Загрузить фотографию с компьютера 999x272</a>{input}{error}'
])->fileInput() ?>
<? ActiveForm::end() ?>