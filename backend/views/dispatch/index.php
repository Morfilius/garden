<?php
/* @var $this View */

use core\entities\Dispatch;
use core\entities\News;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $searchModel backend\forms\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Рассылки';
?>
<section class="content">
    <div class="container-fluid position-relative">
        <div class="publish-btn-wrap">
            <?= Html::a('Создать рассылку', ['create'], ['class' => 'btn btn-block btn-default btn-red btn-user'])?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => '{items}<div class="grid-bottom">{summary}<nav class="pagination-wrap">{pager}</nav></div>',
            'tableOptions' => [
                'class' => 'main-grid'
            ],
            'headerRowOptions' => [
                'class' => 'main-grid__row-head'
            ],
            'rowOptions' => [
                'class' => 'main-grid__row',
            ],
            'pager' => [
                'linkContainerOptions' => [
                    'class' => 'page-item'
                ],
                'linkOptions' => [
                    'class' => 'page-link'
                ],
                'disabledPageCssClass' => 'page-link'
            ],
            'columns' => [
                [
                    'attribute' => 'image.path',
                    'label' => 'Изображение',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                    'value' => function($model) {
                        /** @var News $model*/
                        return '<div class="main-grid__img-wrap">
                                    <img src="'.$model->getThumbUploadUrl('image', 'grid').'" class="main-grid__img" alt="">
                                </div>';
                    }
                ],
                [
                    'attribute' => 'title',
                    'label' => 'Заголовок рассылки',
                    'enableSorting' => false,
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата создания',
                    'format' => ['date', 'php:d.m.Y'],
                    'enableSorting' => false,
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                ],
                [
                    'attribute' => 'recipient_id',
                    'label' => 'Получатель',
                    'enableSorting' => false,
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                    'value' => function (Dispatch $model) {
                        $profile = $model->getUserFromCache($model->user->bitrix_id);
                        return $profile->last_name .' '. $profile->first_name;
                    },
                ],
                [
                    'format' => 'raw',
                    'enableSorting' => false,
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td vam'],
                    'value' => function ($model) {
                        return '<span class="main-grid__actions">'.
                                    Html::a('', ['update', 'id' => $model->id], [
                                        'class' => 'main-grid__action',
                                        'style' => 'background-image: url('.Url::to('@web/img/edit.svg').')',
                                    ]).
                                    Html::a('', ['delete', 'id' => $model->id], [
                                        'class' => 'main-grid__action',
                                        'style' => 'background-image: url('.Url::to('@web/img/trash.svg').')',
                                        'data' => [
                                            'confirm' => 'Вы действительно хотите удалить данную рассылку?',
                                            'method' => 'post',
                                    ]]).'
                                </span>';
                    }
                ],
            ],
        ]);?>
    </div>
</section>