<?php
/* @var $this View */
/* @var $model NewsForm */
use core\forms\manage\NewsForm;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Создание рассылки';
$this->params['backUrl'] = Url::to(['index']);
?>
<div class="inner-content inner-bg">
    <?= $this->render('_form', [
            'model' => $model
    ]) ?>
</div>
