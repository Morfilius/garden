<?
/* @var $this View */
/* @var $model SelfProfileForm */

use core\forms\manage\User\SelfProfileForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
 ?>
<div class="profile">

    <? $form = ActiveForm::begin([
            'options' => [
                    'class' => 'profile-form',
                    'enctype' => 'multipart/form-data'
            ]
        ]
    ); ?>
        <div class="profile-logo">
            <img src="<?= $model->getAvatar(); ?>" alt="" class="profile-logo__img">
<!--            <button class="profile-logo__btn btn-upload"></button>-->
            <input type="file" name="<?= $model->image->formName().'[image]'?>" class="profile-logo__input">
            <input type="hidden" name="<?= $model->image->formName().'[image]'?>" value="" class="profile-logo__input">
        </div>
        <div class="profile-form__inner">
            <div class="profile-form__left">
                <?= $form->field($model, 'first_name')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Введите имя',
                ]) ?>
                <?= $form->field($model, 'info')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Введите должность',
                ]) ?>
            </div>
            <div class="profile-form__right">
                <?= $form->field($model, 'last_name')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Введите фамилию',
                ]) ?>
                <?= $form->field($model, 'phone', [
                    'inputOptions' => ['class' => 'form-control phone'],
                ])->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Введите телефон',
                    'value' => $model->phone ? substr($model->phone, 1) : ''
                ]) ?>
            </div>
            <?= $form->field($model, '_phone_id', ['template' => '{input}'])->hiddenInput()?>
            <div class="profile-form__btn-wrap">
                <?= Html::submitButton('Сохранить и продолжить', ['class' => 'btn profile-form__btn btn-block btn-default btn-red'])?>
            </div>
        </div>
    <? ActiveForm::end() ?>
</div>