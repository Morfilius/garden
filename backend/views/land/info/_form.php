<?
/**@var LandForm $model*/

use core\entities\User\Bitrix24\Employee;
use core\entities\User\Bitrix24\Profile;
use core\forms\manage\Land\LandForm;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$isNew = empty($model->getOriginal());
?>

<? $form = ActiveForm::begin();?>
    <div class="top-spike">
        <div class="steps"></div>
        <div class="next-btn-wrap">
            <?= Html::submitButton($isNew ? 'Добавить карту участка' : 'Редактировать карту участка', ['class' => 'btn btn-block btn-default btn-red next-btn', 'id' => 'create-map']) ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-block btn-default btn-red next-btn']) ?>
        </div>
    </div>
    <div class="form-content inner-bg info-content">
        <div class="row">
            <?php if(!$isNew):?>
                <div class="col-2">
                    <div class="form-group field-landform-date_last has-success">
                        <label class="control-label" for="landform-date_last">Дата последнего визита бригады</label>
                        <input type="text" id="landform-date_last" disabled class="form-control date" value="<?= $model->getOriginal()->getEvents()->getPrev()?>" maxlength="255" placeholder="дд.мм.гггг" aria-invalid="false">
                    </div>
                </div>
            <?php endif;?>
            <div class="col-4">
                <?= $form->field($model, 'employee_id')->listBox(Employee::getList(), [
                    'maxlength' => true,
                    'class' => 'form-control select2'
                ]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'land_area')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Площадь участка (соток)',
                ]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'lawn_area')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Площадь газона (соток)',
                ]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'deciduous_count')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Количество лиственных',
                ]) ?>
            </div>
        </div>
        <div class="row">
            <?php if(!$isNew):?>
                <div class="col-2">
                    <div class="form-group field-landform-date_next has-success">
                        <label class="control-label" for="landform-date_next">Дата ближайшего визита бригады</label>
                        <input type="text" id="landform-date_next" disabled class="form-control date" value="<?= $model->getOriginal()->getEvents()->getNext()?>" maxlength="255" placeholder="дд.мм.гггг" aria-invalid="false">
                    </div>
                </div>
            <?php endif;?>
            <div class="col-4">
                <?= $form->field($model, 'owner_id')->listBox(Profile::clientList(), [
                    'maxlength' => true,
                    'class' => 'form-control select2'
                ]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'age')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Возраст сада (лет)',
                ]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'garden_count')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Количество растений',
                ]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'pine_count')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Количество хвойных',
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-10">
                <?= $form->field($model, 'title')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Название участка',
                ]) ?>
            </div>
            <div class="col-10">
                <?= $form->field($model, 'address')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Адрес участка',
                ]) ?>
            </div>
            <div class="col-10">
                <?= $form->field($model, 'audit')->widget(CKEditor::className(), [
                    'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder',['height' => 400]),
                ])->textarea([
                    'maxlength' => true,
                    'rows' => 7
                ]);?>
            </div>
            <div class="col-10">
                <?= $form->field($model, 'rec')->widget(CKEditor::className(), [
                    'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder',['height' => 400]),
                ])->textarea([
                    'maxlength' => true,
                    'rows' => 7
                ]);?>
            </div>
        </div>
    </div>
<? ActiveForm::end()?>



