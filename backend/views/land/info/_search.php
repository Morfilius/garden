<?/* @var $searchModel LandSearch */

use backend\forms\Land\LandSearch;
use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>
<? $form = ActiveForm::begin([
    'method' => 'get',
    'action' => ['index'],
    'options' => [
        'class' => 'form-search'
    ]
])?>
    <?= $form->field($searchModel, 'search', [
        'template' => "{input}",
])-> textInput([
    'class' => 'form-control form-search__input',
    'placeholder' => 'Поиск по участкам',
])?>
    <?= Html::submitButton('', ['class' => 'form-search__btn']) ?>
<? ActiveForm::end()?>