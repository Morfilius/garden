<?php
/* @var $this View */
/* @var $model NewsForm */

use core\forms\manage\NewsForm;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Редактирование общей информации об участке';
$this->params['backUrl'] = Url::to(['index']);
?>
<div class="inner-content info-inner-content">
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
</div>
