<?php
/* @var $this View */

use backend\forms\Land\LandSearch;
use core\entities\Land\Land;
use core\entities\Land\LandMap;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $searchModel LandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var Land $model*/
$this->title = 'Участки';
?>
<section class="content">
    <div class="container-fluid position-relative">
        <div class="publish-btn-wrap">
            <?= $this->render('_search', [
                'searchModel' =>$searchModel
            ])?>
            <?= Html::a('Добавить участок', ['create'], ['class' => 'btn btn-block btn-default btn-red btn-user'])?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => '{items}<div class="grid-bottom">{summary}<nav class="pagination-wrap">{pager}</nav></div>',
            'tableOptions' => [
                'class' => 'main-grid'
            ],
            'headerRowOptions' => [
                'class' => 'main-grid__row-head'
            ],
            'rowOptions' => [
                'class' => 'main-grid__row',
            ],
            'pager' => [
                'linkContainerOptions' => [
                    'class' => 'page-item'
                ],
                'linkOptions' => [
                    'class' => 'page-link'
                ],
                'disabledPageCssClass' => 'page-link'
            ],
            'columns' => [
                [
                    'attribute' => 'image.path',
                    'label' => 'Изображение',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                    'value' => function($model) {
                        $url = Url::to(['/land/editor/paint', 'id' => $model->id]);
                        return '<div class="main-grid__img-wrap">
                                    <a href="'.$url.'"><img src="'.($model->map ?? new LandMap())->getThumbUploadUrl('image', 'grid').'" class="main-grid__img" alt=""></a>
                                </div>';
                    }
                ],
                [
                    'attribute' => 'title',
                    'label' => 'Название участка',
                    'enableSorting' => false,
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата создания',
                    'format' => ['date', 'php:d.m.Y'],
                    'enableSorting' => false,
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td'],
                ],
                [
                    'format' => 'raw',
                    'enableSorting' => false,
                    'filter' => false,
                    'headerOptions' => ['class' => 'main-grid__th'],
                    'contentOptions' => ['class' => 'main-grid__td vam'],
                    'value' => function($model) {
                        return '<span class="main-grid__actions">'.
                            Html::a('', ['update', 'id' => $model->id], [
                                'class' => 'main-grid__action',
                                'style' => 'background-image: url('.Url::to('@web/img/edit.svg').')',
                            ]).
                            Html::a('', ['delete', 'id' => $model->id], [
                                'class' => 'main-grid__action',
                                'style' => 'background-image: url('.Url::to('@web/img/trash.svg').')',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить данную новость (акцию)?',
                                    'method' => 'post',
                                ]]).'
                                </span>';
                    }
                ],
            ],
        ]);?>
    </div>
</section>