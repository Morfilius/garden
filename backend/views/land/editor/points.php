<?php

use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this \yii\web\View
 * @var $gardens Garden[][]
 * @var $points string
 */
$this->title = 'Добавление участка';
$this->params['backUrl'] = Url::to(['/land/editor/paint', 'id' => $model->id]);
$imageSize = $model->map->getImageSize();
$imageWrapStyle = '';
if (!empty($imageSize) && is_array($imageSize)) {
    $imageWrapStyle = sprintf('style="width:%dpx"', $imageSize['width']);
}
?>
<script>
    <?= $points ? 'var points = '. Json::encode($points).';' : '' ?>
    var saveUrl = '<?= Url::to(Url::to(['/land/editor/points-save', 'id' => $model->id]))?>'
</script>
<div class="inner-content">
    <div class="top-spike">
        <div class="steps">
            <a href="<?= $this->params['backUrl'] ?>" class="steps__item"><span>Шаг 1 «Создание участка»</span></a>
            <span class="steps__item active">Шаг 2 «Добавление растений»</span>
        </div>
        <div class="next-btn-wrap">
            <a href="<?= Url::to(['/land/editor/garden', 'id' => $model->id]) ?>" class="btn btn-block btn-default btn-red next-btn">Далее</a>
        </div>
    </div>
    <div class="plants-content">
        <div class="plants-editor">
            <div class="workbench-container">
                <div class="workbench-container__left">
                    <div class="resize-indicator-container">
                           <span class="resize-indicator resize-indicator--vertical">
                               <span class="resize-indicator__fill"></span>
                               <span class="resize-indicator__round"></span>
                           </span>
                        <span class="resize-indicator resize-indicator--horizontal">
                               <span class="resize-indicator__fill"></span>
                               <span class="resize-indicator__round"></span>
                           </span>
                        <div class="workbench-container__outer">
                            <div class="workbench-container__inner" <?= $imageWrapStyle ?>>
                                <img src="<?= $model->map->getUploadUrl('image') ?>" class="image" alt="">
                                <div id="workbench" class="workbench dropzone"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="workbench-container__right">
                    <div class="preview-container">
                        <div class="preview">
                            <span class="preview__title">Виды растений</span>
                            <ul class="preview-list">
                                <? if (is_array($gardens) && !empty($gardens)): ?>
                                    <? foreach($gardens as $gardens_list): ?>
                                        <li class="preview-list__item">
                                            <span class="preview-list__item-title"><?= $gardens_list[0]->category ?></span>
                                            <div class="preview-list__content">
                                                <ul class="plant-list">
                                                    <? foreach($gardens_list as $garden): ?>
                                                        <li class="plant-list__item">
                                                            <div class="preview-prototype-container">
                                                                <div data-id="<?= $garden->id ?>" class="round preview-prototype" style="background-image: url(<?= Yii::getAlias('@static/gardens/'.$garden->image) ?>)"></div>
                                                            </div>
                                                            <span class="plant-list__text"><?= $garden->name ?></span>
                                                        </li>
                                                    <? endforeach; ?>
                                                </ul>
                                            </div>
                                        </li>
                                    <? endforeach; ?>
                                <? endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
