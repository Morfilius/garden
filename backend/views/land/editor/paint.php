<?php
/* @var $this \yii\web\View */

use yii\helpers\Url;

$this->title = 'Добавление участка';
$this->params['backUrl'] = Url::to(['/land/info']);
?>
<script>
    var saveUrl = '<?= Url::to(Url::to(['/land/editor/paint-save', 'id' => $model->id]))?>'
</script>
<div class="inner-content">
    <div class="top-spike">
        <div class="steps">
            <span class="steps__item active">Шаг 1 «Создание участка»</span>
            <a href="<?= Url::to(['/land/editor/points', 'id' => $model->id]) ?>" class="steps__item"><span>Шаг 2 «Добавление растений»</span></a>
        </div>
        <div class="next-btn-wrap">
            <a href="<?= Url::to(['land/editor/points', 'id' => $model->id]) ?>" class="btn btn-block btn-default btn-red next-btn">Далее</a>
        </div>
    </div>
    <script>
    <?= isset($model->map) ? "var img = '".$model->map->getUploadUrl('image')."';" : '' ?>
    </script>
    <div class="editor-content">
        <div id="editor" class="editor"></div>
    </div>
</div>
