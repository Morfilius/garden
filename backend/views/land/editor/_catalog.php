<?
use core\entities\Catalog\Catalog;
use yii\helpers\Url;

/* @var $this \yii\web\View
 *  @var $catalog Catalog
 */
?>

<div id="editor-info">
    <div class="editor-info__section">
        <div class="editor-info__img-wrap-outer">
            <div class="editor-info__img-wrap">
                <img src="<?= ($catalog->image ? $catalog->getUploadUrl('image') : Url::to('@static/default.png')) ?>" alt="" class="editor-info__img">
            </div>
        </div>
    </div>
    <span class="editor-info__title"><?= $catalog->title ?></span>
        <? if ($catalog->features) : ?>
            <ul class="features">
                <?php foreach($catalog->features as $feature):?>
                    <li class="features__item">
                        <span class="features__title"><?= $feature->name ?></span> <?= $feature->value ?>
                    </li>
                <?php endforeach;?>
            </ul>
        <? endif;?>
        <? if ($catalog->rels) : ?>
            <div class="garden-relations editor-info__section">
                <span class="garden-relations__title">Аналоги <?= $catalog->title ?>
                </span>
                <div class="row">
                    <? $i = 0;  foreach($catalog->rels as $rel) : if (empty($rel->catalog)) continue; $i++?>
                        <div class="col-6">
                            <div class="cart">
                                <div class="cart__img-wrap-outer">
                                    <div class="cart__img-wrap">
                                        <img src="<?= ($rel->catalog->image ? $rel->catalog->getUploadUrl('image') : Url::to('@static/default.png')) ?>" alt="" class="cart__img">
                                    </div>
                                </div>
                                <p class="cart__title">
                                    <?= $rel->catalog->title ?>
                                </p>
                            </div>
                        </div>
                    <? if ($i >= 2) break; endforeach;?>
                </div>
            </div>
        <? endif;?>
</div>

