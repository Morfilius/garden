<?php

use core\entities\Catalog\Catalog;
use core\forms\manage\Land\GardenInfoForm;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this \yii\web\View
 * @var $points array
 * @var $gardenInfoModel GardenInfoForm
 * @var $pointId integer
 * @var $catalog Catalog
 */
$this->title = 'Добавление участка';
$this->params['backUrl'] = Url::to(['/land/editor/points', 'id' => $model->id]);
$imageSize = $model->map->getImageSize();
$imageWrapStyle = '';
if (!empty($imageSize) && is_array($imageSize)) {
    $imageWrapStyle = sprintf('style="width:%dpx"', $imageSize['width']);
}
?>
<script>
    <?= $points ? 'var points = '. Json::encode(array_map(function ($item) use($model) {
            $item['link'] = (isset($item['link']) && $item['link']) ? Url::to(['land/editor/garden', 'id' => $model->id, 'point_id' => $item['id']]) : false;
            return $item;
        }, $points)) : '' ?>
</script>
<div class="inner-content">
    <div class="top-spike">
        <div class="steps">
            <a href="<?= $this->params['backUrl'] ?>" class="steps__item"><span>Шаг 1 «Создание участка»</span></a>
            <span class="steps__item active">Шаг 2 «Добавление растений»</span>
        </div>
        <div class="next-btn-wrap">
            <a href="<?= Url::to(['land/info'])?>" class="btn btn-block btn-default btn-red next-btn">Готово</a>
        </div>
    </div>
    <div class="select-editor">
        <div class="select-editor__left">
            <div class="resize-indicator-container">
                           <span class="resize-indicator resize-indicator--vertical">
                               <span class="resize-indicator__fill"></span>
                               <span class="resize-indicator__round"></span>
                           </span>
                <span class="resize-indicator resize-indicator--horizontal">
                               <span class="resize-indicator__fill"></span>
                               <span class="resize-indicator__round"></span>
                           </span>
                <div class="workbench-container__outer">
                    <div class="workbench-container__inner" <?= $imageWrapStyle ?>>
                        <img src="<?= $model->map->getUploadUrl('image') ?>" class="image" alt="">
                        <div id="workbench" class="workbench dropzone"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="select-editor__right">
            <div class="editor-info-wrap">
                <? if ($pointId) {
                        echo $this->render('_info', [
                            'model' => $model,
                            'gardenInfoModel' => $gardenInfoModel,
                            'pointId' => $pointId,
                            'catalog' => $catalog
                        ]);
                    } else {
                        echo  '<div class="editor-info__top">
                                    <span class="editor-info__title">Выберите растение на карте для редактирования информации</span>
                                </div>';
                    }

                ?>
            </div>
        </div>
    </div>
</div>
