<?
/* @var $this \yii\web\View
 * @var $gardenInfoModel GardenInfoForm
 * @var $pointInfo GardenInfo
 * @var $pointId integer
 *  @var $catalog Catalog
 * @var $model Land
 */

use core\entities\Catalog\Catalog;
use core\entities\Land\Garden\GardenInfo;
use core\entities\Land\Land;
use core\forms\manage\Land\GardenInfoForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
        'options' => ['class' => 'editor-info'],
        'action' => Url::to(['land/editor/garden', 'id' => $model->id])
    ])?>
    <div class="editor-info__top">
        <span class="editor-info__title">Общая информация о растении</span>
        <a href="<?= Url::to(['catalog/index', 'land_id' => $model->id, 'point_id' => $pointId])?>" class="editor-info__nav"><?= $catalog ? 'Выбрать другое растение' : 'Выбрать из каталога'?></a>
    </div>
    <p class="editor-info__txt">Выберите растение из каталога, чтобы увидеть информацию</p>
    <? if ($catalog) echo $this->render('_catalog', ['catalog' => $catalog]) ?>
    <div class="editor-info__section">
        <span class="editor-info__title mb16">Информация по данному растению</span>
        <div class="row">
            <div class="col-6">
                <?= $form->field($gardenInfoModel, 'landing_date')->textInput([
                    'class' => 'form-control date',
                    'maxlength' => true,
                    'placeholder' => 'дд.мм.гггг',
                    'value' => $gardenInfoModel->landing_date ? (new DateTime($gardenInfoModel->landing_date))->format('d.m.Y') : '',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="editor-info__section">
        <span class="editor-info__title mb18">Персональные рекомендации по уходу</span>
        <?= $form->field($gardenInfoModel, 'text', [
            'template' => '{input}{hint}'
        ])->textarea([
            'maxlength' => true,
            'rows' => 5,
            'class' => 'form-control textarea',
            'placeholder' => 'Персональные рекомендации по уходу'
        ]) ?>
    </div>
    <?= $form->field($gardenInfoModel, 'land_points_id', ['template' => '{input}'])->hiddenInput(['value' => $pointId])?>
    <? if ($catalog) echo  $form->field($gardenInfoModel, 'catalog_id', ['template' => '{input}'])->hiddenInput(['value' => $catalog->id])?>
    <div class="row">
        <div class="col-6">
            <?= Html::submitButton('Сохранить и продолжить', ['class' => 'btn btn-block btn-default btn-red']) ?>
        </div>
        <div class="col-6">
            <?php if ($catalog) : ?>
                <button type="button" id="cancel" class="btn btn-block btn-default">Отменить изменения</button>
            <?php endif; ?>
        </div>
    </div>
<? ActiveForm::end() ?>