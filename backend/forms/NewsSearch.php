<?php

namespace backend\forms;

use core\entities\News;
use yii\data\ActiveDataProvider;

class NewsSearch extends BaseSearch
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = News::find()->alias('n');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 9,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', 'n.title', $this->search]);


        return $dataProvider;
    }
}
