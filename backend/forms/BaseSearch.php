<?php


namespace backend\forms;


use yii\base\Model;

abstract class BaseSearch extends Model
{
    public $search;

    public function rules()
    {
        return [['search', 'string', 'max' => 255]];
    }

    public function formName()
    {
        return '';
    }
}