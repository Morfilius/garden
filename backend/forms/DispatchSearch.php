<?php

namespace backend\forms;

use core\entities\Dispatch;
use core\entities\User\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class DispatchSearch extends BaseSearch
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Dispatch::find()->with('user')->alias('d');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 9,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'd.title', $this->search]);

        if (!empty($dataProvider->getModels())) {
            Dispatch::cacheByBitrixId(array_reduce($dataProvider->getModels(), function ($result, Dispatch $item) {
                $result[] = $item->user->bitrix_id;
                return $result;
            }, []));
        }
        return $dataProvider;
    }
}
