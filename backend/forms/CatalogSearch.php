<?php

namespace backend\forms;

use core\entities\Catalog\Catalog;
use yii\data\ActiveDataProvider;

class CatalogSearch extends BaseSearch
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Catalog::find()->alias('c');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 9,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', 'c.title', $this->search]);


        return $dataProvider;
    }
}
