<?php

namespace backend\forms;

use core\entities\User\Bitrix24\Profile;
use core\entities\User\User;
use core\services\Bitrix24\BitrixApi;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class UserSearch extends BaseSearch
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = User::find()->alias('u')->leftJoin('auth_assignment', 'u.id = auth_assignment.user_id')->andWhere(['auth_assignment.user_id' => null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 15,
            ],

        ]);

        $this->load($params, '');
        $query = $this->searchInBitrix($query);


        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($dataProvider->getModels())) {
            User::cacheByBitrixId(ArrayHelper::getColumn($dataProvider->getModels(), 'bitrix_id'));
        }
        return $dataProvider;
    }

    public function searchInBitrix(ActiveQuery $query)
    {
        if (!empty($this->search)) {
            $fullName = explode(' ', $this->search);

            $result = BitrixApi::call('crm.contact.list', [
                'filter' => array_filter([
                    'LAST_NAME' => $fullName[0],
                    'NAME' => $fullName[1] ?? false,
                ])
            ]);
            if (!empty($result)) {
                $query->andWhere(['in', 'bitrix_id', ArrayHelper::getColumn($result, 'ID')]);
            }
        }

        return $query;
    }
}
