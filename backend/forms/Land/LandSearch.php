<?php

namespace backend\forms\Land;

use backend\forms\BaseSearch;
use core\entities\Land\Land;
use yii\data\ActiveDataProvider;

class LandSearch extends BaseSearch
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Land::find()->alias('l');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 9,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', 'l.title', $this->search]);


        return $dataProvider;
    }
}
