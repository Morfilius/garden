<?php


namespace backend\controllers;


use backend\forms\NewsSearch;
use core\entities\Banners;
use core\entities\News;
use core\forms\manage\BannersForm;
use core\forms\manage\NewsForm;
use core\useCases\manage\NewsManageService;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BannerController extends Controller
{
    /**
     * @var NewsManageService
     */
    private $service;

    public function __construct($id, $module, NewsManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex()
    {
        $model = new Banners();

        $bannersList = [
            1 => Banners::findOne(['number' => 1]),
            2 => Banners::findOne(['number' => 2]),
            41 => Banners::findOne(['number' => 41]),
            42 => Banners::findOne(['number' => 42]),
            43 => Banners::findOne(['number' => 43]),
            44 => Banners::findOne(['number' => 44]),
            7 => Banners::findOne(['number' => 7]),
        ];

        return $this->render('index', [
            'model' => $model,
            'bannersList' => $bannersList,
        ]);
    }

    public function actionCreate()
    {
        $form = new BannersForm();
        $this->layout = 'inner';
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $banners = Banners::create($form->text, 1, $form->image->imageFile);
                $banners->save();
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $form = new BannersForm($this->findModel($id));
        $this->layout = 'inner';
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $model = $this->findModel($id);
                $model->edit($form->text, $form->image->imageFile);
                $model->save();
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Banners::findOne(['number' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}