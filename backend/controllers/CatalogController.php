<?php


namespace backend\controllers;


use backend\forms\CatalogSearch;
use Yii;
use yii\web\Controller;

class CatalogController extends Controller
{
    public $layout = 'inner';

    public function actionIndex($land_id, $point_id)
    {
        $searchModel = new CatalogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'landId' => $land_id,
            'point_id' => $point_id
        ]);
    }
}