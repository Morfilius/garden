<?php


namespace backend\controllers;

use core\forms\manage\User\ProfileForm;
use core\forms\manage\User\SelfProfileForm;
use core\readRepositories\UserReadRepository;
use core\repositories\User\ProfileRepository;
use core\repositories\User\UserRepository;
use core\services\Bitrix24\BitrixApi;
use core\useCases\manage\UserManageService;
use Yii;
use yii\debug\models\search\User;
use yii\web\Controller;

class ProfileController extends Controller
{
    /**
     * @var UserManageService
     */
    private $service;
    /**
     * @var UserReadRepository
     */
    private $repository;
    /**
     * @var BitrixApi
     */
    private $bitrixApi;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct($id, $module,
                                UserManageService $service,
                                ProfileRepository $repository,
                                UserRepository $userRepository,
                                BitrixApi $bitrixApi,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->repository = $repository;
        $this->bitrixApi = $bitrixApi;
        $this->userRepository = $userRepository;
    }

    public function actionUpdate()
    {
        $id = $this->userRepository->get(Yii::$app->user->id)->bitrix_id;
        $form = new SelfProfileForm($this->repository->getOperator($id));
        $this->layout = 'inner';

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->editProfile($id, $form);
                return $this->redirect(['/']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }
}