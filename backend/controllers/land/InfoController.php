<?php

/** todo  уточнить редактирование выезда бригад в участке
*/
namespace backend\controllers\land;


use backend\forms\Land\LandSearch;
use core\entities\Land\Land;
use core\forms\manage\Land\LandForm;
use core\useCases\manage\Land\LandManageService;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class InfoController extends Controller
{

    /**
     * @var LandManageService
     */
    private $service;

    public function __construct($id, $module, LandManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex()
    {
        $searchModel = new LandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $form = new LandForm();
        $this->layout = 'inner';

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                if ($land = $this->service->create($form)) {
                    if (Yii::$app->request->post('create_map')) {
                        return $this->redirect(['/land/editor/paint', 'id' => $land->id]);
                    }
                }
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $form = new LandForm($this->findModel($id));
        $this->layout = 'inner';

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($id, $form);
                if ($this->isCreateMap()) {
                    return $this->redirect(['/land/editor/paint', 'id' => $id]);
                }
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    private function isCreateMap()
    {

        return (Yii::$app->request->post('create_map') == 1);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Land::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}