<?php


namespace backend\controllers\land;


use core\entities\Catalog\Catalog;
use core\forms\manage\Land\EditorForm;
use core\forms\manage\Land\GardenInfoForm;
use core\readRepositories\GardenReadRepository;
use core\readRepositories\PointsReadRepository;
use core\repositories\Land\LandRepository;
use core\useCases\manage\Land\EditorManageService;
use core\useCases\manage\Land\PointsManageService;
use Yii;
use yii\base\DynamicModel;
use yii\web\Controller;

class EditorController extends Controller
{
    public $layout = 'inner';
    public $enableCsrfValidation = false;
    /**
     * @var EditorManageService
     */
    private $service;
    /**
     * @var \core\repositories\Land\LandRepository
     */
    private $landRepository;
    /**
     * @var PointsManageService
     */
    private $pointsManageService;
    /**
     * @var GardenReadRepository
     */
    private $gardenReadRepository;
    /**
     * @var PointsReadRepository
     */
    private $pointsReadRepository;

    public function __construct($id, $module,
                                EditorManageService $service,
                                LandRepository $landRepository,
                                PointsManageService $pointsManageService,
                                GardenReadRepository $gardenReadRepository,
                                PointsReadRepository $pointsReadRepository,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->landRepository = $landRepository;
        $this->pointsManageService = $pointsManageService;
        $this->gardenReadRepository = $gardenReadRepository;
        $this->pointsReadRepository = $pointsReadRepository;
    }

    public function actionPaint($id)
    {
        $land = $this->landRepository->get($id);
        return $this->render('paint', [
            'model' => $land
        ]);
    }
    
    public function actionPoints($id)
    {
        $land = $this->landRepository->get($id);
        $gardens = $this->gardenReadRepository->showList();
        $points = $this->pointsReadRepository->showList($land->map->id);
        return $this->render('points', [
            'model' => $land,
            'gardens' => $gardens,
            'points' => $points
        ]);
    }

    public function actionGarden($id, $catalog_id = null, $point_id = null)
    {
        $form = new GardenInfoForm($this->pointsReadRepository->showInfo($point_id));

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->pointsManageService->saveInfo($form);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $land = $this->landRepository->get($id);
        $points = $this->pointsReadRepository->showListNeedLink($land->map->id, $point_id);
        $catalog = $catalog_id ? Catalog::findOne($catalog_id) : Catalog::findOne($form->catalog_id);

        return $this->render('garden', [
            'model' => $land,
            'points' => $points,
            'gardenInfoModel' => $form,
            'pointId' => $point_id,
            'catalog' => $catalog
        ]);
    }

    public function actionPointsSave($id)
    {
        if ($points = Yii::$app->request->post('points')) {
            $form = DynamicModel::validateData(compact('points'), [
                ['points', 'required'],
                ['points', 'string'],
            ]);
            if (!$form->hasErrors()) {
                $this->pointsManageService->save($id, $points);
            }
        }

    }
    
    public function actionPaintSave($id)
    {
        $form = new EditorForm();

        if ($form->load(Yii::$app->request->bodyParams, '') && $form->validate()) {
            try {
                $this->service->create($id, $form);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                return $e->getMessage();
            }
        }

        return 'OK';
    }
}