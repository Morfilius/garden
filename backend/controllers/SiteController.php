<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionLogin()
    {

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
