<?php
return [
    'id' => 'app-backend-tests',
    'bootstrap' => [
        'backend\tests\app\setup\SetUp'
    ],
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
//        'urlManager' => [
//            'showScriptName' => true,
//        ],
//        'request' => [
//            'cookieValidationKey' => 'test',
//        ],
    ],
];
