<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'ImperialGarden',
    'name' => 'ImperialGarden',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@staticRoot' => $params['staticPath'],
        '@static'   => $params['staticHostInfo'],
    ],
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => [
        'common\bootstrap\SetUp',
        'log'
    ],
    'modules' => [],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['@'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'baseUrl'=>'@static',
                    'basePath'=>'@staticRoot',
                    'path' => 'editor',
                    'name' => 'Изображения'
                ]
            ],
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\auth\Identity',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl' => ['auth/login']
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'catalog/land/<land_id:\d+>/point/<point_id:\d+>/page/<page:\d+>' => 'catalog/index',
                'catalog/land/<land_id:\d+>/point/<point_id:\d+>' => 'catalog/index',

                'land/<c_>/<a_>/<id:\d+>/catalog/<catalog_id:\d+>/point/<point_id:\d+>' => 'land/<c_>/<a_>',
                'land/<c_>/<a_>/<id:\d+>/catalog/<catalog_id:\d+>' => 'land/<c_>/<a_>',
                'land/<c_>/<a_>/<id:\d+>/point/<point_id:\d+>' => 'land/<c_>/<a_>',

                '<c_>/<a_>/page/<page:\d+>' => '<c_>/<a_>',
                '<namespace>/<c_>/<a_>/<id:\d+>' => '<namespace>/<c_>/<a_>',
                '<c_>/<a_>/<id:\d+>' => '<c_>/<a_>',
                '<c_>' => '<c_>/index',
            ],
        ],

    ],
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'except' => ['auth/login', 'site/error'],
        'rules' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
    'params' => $params,
];
