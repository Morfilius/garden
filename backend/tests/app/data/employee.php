<?php
return [
    10 => array (
    'result' =>
        array (
            0 =>
                array (
                    'ID' => '10',
                    'ACTIVE' => true,
                    'EMAIL' => 'admin@admin.com',
                    'NAME' => 'Админ',
                    'LAST_NAME' => 'Админович',
                    'SECOND_NAME' => '',
                    'PERSONAL_GENDER' => '',
                    'PERSONAL_PROFESSION' => NULL,
                    'PERSONAL_WWW' => '',
                    'PERSONAL_BIRTHDAY' => '',
                    'PERSONAL_PHOTO' => 'https://cdn-ru.bitrix24.ru/b13799034/main/96b/96bece72d48e8c31e0e99db922d96210/kot_seryj_polosatyj_149407_1920x1080.jpg',
                    'PERSONAL_ICQ' => NULL,
                    'PERSONAL_PHONE' => '',
                    'PERSONAL_FAX' => NULL,
                    'PERSONAL_MOBILE' => '',
                    'PERSONAL_PAGER' => NULL,
                    'PERSONAL_STREET' => NULL,
                    'PERSONAL_CITY' => '',
                    'PERSONAL_STATE' => NULL,
                    'PERSONAL_ZIP' => NULL,
                    'PERSONAL_COUNTRY' => NULL,
                    'WORK_COMPANY' => NULL,
                    'WORK_POSITION' => 'Администратор',
                    'WORK_PHONE' => '73242342341',
                    'UF_DEPARTMENT' =>
                        array (
                            0 => 16,
                        ),
                    'UF_INTERESTS' => NULL,
                    'UF_SKILLS' => NULL,
                    'UF_WEB_SITES' => NULL,
                    'UF_XING' => NULL,
                    'UF_LINKEDIN' => NULL,
                    'UF_FACEBOOK' => NULL,
                    'UF_TWITTER' => NULL,
                    'UF_SKYPE' => NULL,
                    'UF_DISTRICT' => NULL,
                    'UF_PHONE_INNER' => NULL,
                    'USER_TYPE' => 'employee',
                ),
        ),
        'total' => 1,
        'time' =>
            array (
                'start' => 1591259667.215469,
                'finish' => 1591259667.383491,
                'duration' => 0.16802215576171875,
                'processing' => 0.029180049896240234,
                'date_start' => '2020-06-04T11:34:27+03:00',
                'date_finish' => '2020-06-04T11:34:27+03:00',
            ),
    )
];