<?php

namespace backend\tests\app\setup;

use backend\tests\app\mocks\BitrixApiMock;
use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\Config\RestConfig;
use yii\base\BootstrapInterface;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;
        $container->setSingleton(BitrixApi::class, function () use ($app) {
            return new BitrixApiMock(new RestConfig('local.5ea578456cd836.41828512', 'yQfBh5TGyYXYaL9e6VM8POfrqlMB6LjOEh6xGiPCGI4UYTMVry'));
        });
    }
}