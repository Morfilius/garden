<?php


namespace backend\tests\app\mocks;


use core\services\Bitrix24\BitrixApi;


class BitrixApiMock extends BitrixApi
{
    protected $employee = [];

    protected function callCurl($endpoint)
    {
        $this->fillData();
        $mock = $this->mock($endpoint);
        //return $mock;
        $queryUrl = $this->getUrl($endpoint);
        curl_setopt_array($curl = curl_init(), [
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => http_build_query($this->query),
        ]);
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, 1);
        return json_decode($result, 1);
    }

    protected function mock($endpoint)
    {
        switch ($endpoint) {
            case ('user.get'): {

            }
        }
    }


    protected function fillData() {
        $dataPath = __DIR__.'/../data/';
        $this->employee = include $dataPath . 'employee.php';
    }
}