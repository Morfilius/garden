<?php
namespace backend\tests;

use common\fixtures\UserFixture;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    /**
     * Define custom actions here
     */
    public function login($name, $password)
    {
        $I = $this;
        // if snapshot exists - skipping login
        if ($I->loadSessionSnapshot('login')) {
            return;
        }
        // logging in
        $I->amOnPage('/auth/login');
        $I->submitForm('#w0', [
            'LoginForm[username]' => $name,
            'LoginForm[password]' => $password
        ]);
        // saving snapshot
        $I->wait(2);
        $I->seeElement('.brand-image');
        $I->saveSessionSnapshot('login');
    }
}
