<?php

return [
    [
        'username' => 'admin',
        'auth_key' => 'iwTNae9t34OmnK6l4vT4IeaTk-YWI2Rv',
        'password_hash' => '$2y$13$OR.w07CKOamt9u355N5.o.Tkvm1aMNhijF23hm08k7ku77Jyr/l1q',
        'password_reset_token' => 't5GU9NwpuGYSfb7FEZMAxqtuz2PkEvv_' . time(),
        'created_at' => '1391885313',
        'updated_at' => '1391885313',
        'bitrix_id' => '10',
        'manager_id' => '1',
    ],
];
