<?php
namespace frontend\tests\acceptance;

use backend\tests\AcceptanceTester;
use common\fixtures\UserFixture;

class AdminProfileCest
{
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
        ];
    }

    function _before(AcceptanceTester $I)
    {
        $I->login('admin', 'Admin123#');
    }

    public function success(AcceptanceTester $I)
    {
        $I->amOnPage('/profile/update');
        $I->seeInField('SelfProfileForm[first_name]', 'Админ');
        $I->seeInField('SelfProfileForm[last_name]', 'Админович');
        $I->seeInField('SelfProfileForm[info]', 'Администратор');
        $I->seeInField('SelfProfileForm[phone]', '+7 324 234 23 41');
    }
}
