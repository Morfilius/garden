<?php
namespace frontend\tests\acceptance;

use backend\tests\AcceptanceTester;
use common\fixtures\UserFixture;

class AuthCest
{
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
        ];
    }

    function _before(AcceptanceTester $I)
    {
        //$I->login('admin', 'Admin123#');
    }

    public function success(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->submitForm('#w0', [
            'LoginForm[username]' => 'admin',
            'LoginForm[password]' => 'Admin123#'
        ]);
        $I->wait(2);
        $I->seeElement('.brand-image');
    }

    public function errorLogin(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->submitForm('#w0', [
            'LoginForm[username]' => 'admin1',
            'LoginForm[password]' => 'Admin123#'
        ]);
        $I->wait(1);
        $I->see('Неправильный логин или пароль.', '.help-block');
    }
}
