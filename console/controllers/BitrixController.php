<?php


namespace console\controllers;


use core\services\Bitrix24\BitrixApi;
use yii\console\Controller;
use yii\helpers\Json;

class BitrixController extends Controller
{
    /**
     * @var BitrixApi
     */
    private $bitrixApi;

    public function __construct($id, $module, BitrixApi $bitrixApi, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->bitrixApi = $bitrixApi;
    }

    public function actionRefresh()
    {
        $this->bitrixApi->refreshToken();
    }
}