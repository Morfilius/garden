<?php


namespace console\controllers;

use core\entities\User\User;
use Exception;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

//php yii migrate --migrationPath=@yii/rbac/migrations
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $adminPanel = $auth->createPermission('admin-panel');
        $auth->add($adminPanel);

        $employee = $auth->createPermission('employee');
        $auth->add($employee);

        $operator1 = $auth->createRole('operator1');
        $auth->add($operator1);
        $auth->addChild($operator1, $adminPanel);
        $auth->addChild($operator1, $employee);

        $operator2 = $auth->createRole('operator2');
        $auth->add($operator2);
        $auth->addChild($operator2, $adminPanel);
        $auth->addChild($operator2, $employee);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $operator1);
        $auth->addChild($admin, $operator2);
    }

    public function actionAssign()
    {
        $username = $this->prompt('Username:', ['required' => true]);
        $user = $this->findModel($username);
        $roleName = $this->select('Role:', ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'));
        $authManager = Yii::$app->getAuthManager();
        $role = $authManager->getRole($roleName);
        $authManager->assign($role, $user->id);
        $this->stdout('Done!' . PHP_EOL);
    }

    public function actionAssignBitrix()
    {
        $username = $this->prompt('Username:', ['required' => true]);
        $user = $this->findModel($username);
        $bitrix_id = $this->prompt('Id employee:', ['required' => true]);
        $user->assignProfile($bitrix_id);
        $user->save();
        $this->stdout('Done!' . PHP_EOL);
    }

    /**
     * Removes role from user
     */
    public function actionRevoke()
    {
        $username = $this->prompt('Username:', ['required' => true]);
        $user = $this->findModel($username);
        $roleName = $this->select('Role:', ArrayHelper::merge(
            ['all' => 'All Roles'],
            ArrayHelper::map(Yii::$app->authManager->getRolesByUser($user->id), 'name', 'description'))
        );
        $authManager = Yii::$app->getAuthManager();
        if ($roleName == 'all') {
            $authManager->revokeAll($user->id);
        } else {
            $role = $authManager->getRole($roleName);
            $authManager->revoke($role, $user->id);
        }
        $this->stdout('Done!' . PHP_EOL);
    }

    private function findModel($username)
    {
        if (!$model = User::findOne(['username' => $username])) {
            throw new Exception('User is not found');
        }
        return $model;
    }
}