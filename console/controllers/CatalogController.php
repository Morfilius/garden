<?php


namespace console\controllers;

use core\forms\api\CatalogForm;
use core\useCases\api\CatalogApiService;
use Yii;
use yii\console\Controller;

class CatalogController extends Controller
{
    public function actionInsert()
    {
        /* @var $service CatalogApiService*/

        $service = Yii::createObject(CatalogApiService::class);
        $names = [
            'Абрикос обыкновенный «Восточно-сибирский»',
            'Алыча гибридная «Злато Скифов»',
            'Азалия гибридная  оранжевая «Карат»',
            'Азалия гибридная  розовая «Карат»',
            'Брусника обыкновенная',
            'Брусника обыкновенная \'Koralle\'',
            'Черешня «Красная Биттнера»'
        ];
        $categories = [
            'Бонсаи и топиары',
            'Водные растения',
            'Вьющиеся растения',
            'Лиственные деревья',
            'Лиственные кустарники',
            'Многолетние растения',
            'Однолетние растения',
            'Плодовые растения',
            'Рододендроны',
            'Розы',
            'Хвойные растения',
            'Хвойные кустарники',
        ];
        for ($i = 0; $i < 20; $i++) {
            $index = rand(0, 6);
            $indexCat = rand(0, 11);
            $form = new CatalogForm();
            $form->load([
                'product_id' => rand(0, 5),
                'image' => 'product'.rand(0, 19).'.jpg',
                'title' =>  $names[$index].$i,
                'category' => $categories[$indexCat],
                'features' => [
                    ['Свет', 'светолюбивое'],
                    ['Период созревания плодов', 'июль-август'],
                    ['Вкус плодов', 'кисло-сладкий'],
                    ['Высота', '125-150 см'],
                ],
                'relations' => [rand(1, 7), rand(1, 7), rand(1, 7)]
            ],'');
            $service->create($form);
        }
    }
}

//
//*/10 * * * * cd /var/www/html && php yii bitrix/refresh
//*/50 * * * * cd /var/www/html && php yii gardener/today
//*/49 * * * * cd /var/www/html && php yii gardener/tomorrow
//*/49 * * * * cd /var/www/html && php yii delivery/notify
