<?php


namespace console\controllers;

use core\entities\Catalog\Category;
use yii\console\Controller;

class CategoryController extends Controller
{
    public function actionInsert()
    {
        $categories = [
            'Бонсаи и топиары',
            'Водные растения',
            'Вьющиеся растения',
            'Лиственные деревья',
            'Лиственные кустарники',
            'Многолетние растения',
            'Однолетние растения',
            'Плодовые растения',
            'Рододендроны',
            'Розы',
            'Хвойные растения',
            'Хвойные кустарники',
        ];
        $i = 0;
        foreach ($categories as $category) { ++$i;
            $category = Category::create($category, 'category_'. $i . '.jpg');
            $category->save();
        }
    }
}