<?php


namespace console\controllers;


use core\useCases\ArriveGardenerService;
use yii\console\Controller;

class GardenerController extends Controller
{
    /**
     * @var ArriveGardenerService
     */
    private $arriveGardenerService;

    public function __construct($id, $module, ArriveGardenerService $arriveGardenerService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->arriveGardenerService = $arriveGardenerService;
    }

    public function actionToday()
    {
        $this->arriveGardenerService->today();
    }

    public function actionTomorrow()
    {
        $this->arriveGardenerService->tomorrow();
    }

}