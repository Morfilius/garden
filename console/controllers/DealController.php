<?php


namespace console\controllers;


use core\services\Bitrix24\BitrixApi;
use yii\console\Controller;

class DealController extends Controller
{
    /**
     * @var BitrixApi
     */
    private $bitrixApi;
    private $handlerUrl = 'https://api.igsad.ru/from-bitrix/deal-status';


    public function __construct($id, $module, BitrixApi $bitrixApi, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->bitrixApi = $bitrixApi;
    }

    public function actionInstall()
    {
        BitrixApi::call(
            'event.bind',
            [
                'event' => 'onCrmDealUpdate',
                'handler' => $this->handlerUrl,
            ]
        );
    }

    public function actionRebind()
    {
        BitrixApi::call(
            'event.unbind',
            [
                'event' => 'onCrmDealUpdate',
                'handler' => 'https://siteforyou.of.by/rest/handlers/order-change.php',
            ]
        );

        BitrixApi::call(
            'event.bind',
            [
                'event' => 'onCrmDealUpdate',
                'handler' => $this->handlerUrl,
            ]
        );
    }
    
    public function actionUninstall()
    {
        BitrixApi::call(
            'event.unbind',
            [
                'event' => 'onCrmDealUpdate',
                'handler' => $this->handlerUrl,
            ]
        );
    }
}