<?php


namespace console\controllers;


use core\services\Bitrix24\BitrixApi;
use yii\console\Controller;

class ManagerController extends Controller
{
    /**
     * @var BitrixApi
     */
    private $bitrixApi;
    private $handlerUrl = 'https://api.igsad.ru/from-bitrix/manager-change';


    public function __construct($id, $module, BitrixApi $bitrixApi, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->bitrixApi = $bitrixApi;
    }

    public function actionInstall()
    {
        BitrixApi::call(
            'event.bind',
            [
                'event' => 'onCrmContactUpdate',
                'handler' => $this->handlerUrl,
            ]
        );
    }

    public function actionUninstall()
    {
        BitrixApi::call(
            'event.unbind',
            [
                'event' => 'onCrmContactUpdate',
                'handler' => $this->handlerUrl,
            ]
        );
    }
}