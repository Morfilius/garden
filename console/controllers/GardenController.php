<?php


namespace console\controllers;

use core\entities\Land\Garden\Garden;
use Yii;
use yii\console\Controller;

class GardenController extends Controller
{
    public function actionInsert()
    {
        Yii::$app->db->createCommand()->batchInsert(Garden::tableName(), ['name', 'category', 'category_slug', 'image'], [
            ['Плодовые растения 1', 'Плодовые растения', 'plod', 'plod1.jpg'],
            ['Плодовые растения 2', 'Плодовые растения', 'plod', 'plod2.jpg'],
            ['Лиственные деревья 1', 'Лиственные деревья', 'list', 'list1.jpg'],
            ['Лиственные деревья 2', 'Лиственные деревья', 'list', 'list2.jpg'],
            ['Лиственные деревья 2', 'Лиственные деревья', 'list', 'list3.jpg'],
            ['Хвойные деревья 1', 'Хвойные деревья', 'hvoy', 'hvoy1.jpg'],
            ['Хвойные деревья 2', 'Хвойные деревья', 'hvoy', 'hvoy2.jpg'],
        ])->execute();
    }
}