<?php


namespace console\controllers;


use core\useCases\ProductDeliveryService;
use yii\console\Controller;

class DeliveryController extends Controller
{
    /**
     * @var ProductDeliveryService
     */
    private $service;

    public function __construct($id, $module, ProductDeliveryService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionNotify()
    {
        $this->service->notify();
    }
}