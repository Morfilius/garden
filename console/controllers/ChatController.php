<?php


namespace console\controllers;


use core\services\Bitrix24\Chat\Chat;
use yii\console\Controller;

class ChatController extends Controller
{
    public function actionInstall()
    {
        Chat::install('Чат в приложении');
    }

    public function actionUninstall()
    {
        Chat::uninstall();
    }

    public function actionActivate()
    {
        Chat::activate();
    }

    public function actionRebind()
    {
        Chat::rebind();
    }
}