<?php

use yii\db\Migration;

/**
 * Class m200514_073701_chat
 */
class m200514_073701_chat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%chats}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%chats_messages}}', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'text' => $this->text()->notNull(),
            'type' => $this->string()->notNull(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-chats_user_id', '{{%chats}}', 'user_id', '{{%users}}', 'id', "CASCADE");
        $this->addForeignKey('fk-chats_messages_chat_id', '{{%chats_messages}}', 'chat_id', '{{%chats}}', 'id', "CASCADE");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%chats}}');
        $this->dropTable('{{%chats_messages}}');
    }
}
