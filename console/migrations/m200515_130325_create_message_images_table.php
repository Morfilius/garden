<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%message_images}}`.
 */
class m200515_130325_create_message_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%message_images}}', [
            'id' => $this->primaryKey(),
            'message_id' => $this->integer()->notNull(),
            'path' => $this->string()->notNull(),
        ]);
        $this->addForeignKey('fk-message_images-message_id', '{{%message_images}}', 'message_id', '{{%chats_messages}}', 'id', "CASCADE");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%message_images}}');
    }
}
