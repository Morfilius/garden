<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%gardener_notified}}`.
 */
class m200523_162123_create_gardener_notified_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%gardener_notified}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
        ]);
        $this->addForeignKey('fk-gardener_notified-user_id', '{{%gardener_notified}}', 'user_id', '{{%users}}', 'id', "CASCADE");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gardener_notified}}');
    }
}
