<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%delivery_notified}}`.
 */
class m200525_072759_create_delivery_notified_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%delivery_notified}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
        ]);
        $this->addForeignKey('fk-delivery_notified-user_id', '{{%delivery_notified}}', 'user_id', '{{%users}}', 'id', "CASCADE");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%delivery_notified}}');
    }
}
