<?php

use yii\db\Migration;

/**
 * Class m200429_112826_add_column_calendar_id_to_lands_table
 */
class m200429_112826_add_column_calendar_id_to_lands_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%lands}}', 'calendar_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%lands}}', 'calendar_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200429_112826_add_column_calendar_id_to_lands_table cannot be reverted.\n";

        return false;
    }
    */
}
