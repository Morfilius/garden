<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%garden_info}}`.
 */
class m200421_140941_create_garden_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%gardens_info}}', [
            'id' => $this->primaryKey(),
            'land_points_id' => $this->integer()->notNull(),
            'catalog_id' => $this->integer(),
            'landing_date' => $this->date(),
            'text' => $this->text()
        ], $tableOptions);

        $this->addForeignKey('fk-garden_info-land_points_id', '{{%gardens_info}}', 'land_points_id', '{{%land_points}}', 'id', 'CASCADE');
        $this->addForeignKey('fk-garden_info-catalog_id', '{{%gardens_info}}', 'catalog_id', '{{%catalog}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%garden_info}}');
    }
}
