<?php

use yii\db\Migration;

/**
 * Class m200507_110433_change_catalog_category_type_column
 */
class m200507_110433_change_catalog_category_type_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%catalog}}', 'category');
        $this->addColumn('{{%catalog}}', 'category_id', $this->integer()->notNull());

        $this->addForeignKey('fk-category_category_id', '{{%catalog}}', 'category_id', 'category', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
