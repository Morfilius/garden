<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lands}}`.
 */
class m200415_080243_create_lands_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%lands}}', [
            'id' => $this->primaryKey(),
            'land_map_id' => $this->integer(),
            'title' => $this->string()->notNull(),
            'address' => $this->string(),
            'employee_id' => $this->integer()->notNull(),
            'owner_id' => $this->integer(),
            'land_area' => $this->integer(),
            'lawn_area' => $this->integer(),
            'deciduous_count' => $this->integer(),
            'pine_count' => $this->integer(),
            'garden_count' => $this->integer(),
            'age' => $this->integer(),
            'audit' => $this->text(),
            'rec' => $this->text(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lands}}');
    }
}
