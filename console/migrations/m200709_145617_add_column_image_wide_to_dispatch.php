<?php

use yii\db\Migration;

/**
 * Class m200709_145617_add_column_image_wide_to_dispatch
 */
class m200709_145617_add_column_image_wide_to_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%dispatch}}', 'banner_show', $this->smallInteger()->defaultValue(0));
        $this->addColumn('{{%dispatch}}', 'banner_title', $this->string());
        $this->addColumn('{{%dispatch}}', 'image_wide', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%dispatch}}', 'banner_show');
        $this->dropColumn('{{%dispatch}}', 'banner_title');
        $this->dropColumn('{{%dispatch}}', 'image_wide');
    }
}
