<?php

use yii\db\Migration;

/**
 * Class m200415_102854_editor_fk
 */
class m200415_102854_editor_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk-lands-lands_map_id', '{{%lands}}', 'land_map_id', '{{%lands_map}}', 'id', "CASCADE");
        $this->addForeignKey('fk-lands-owner_id', '{{%lands}}', 'owner_id', '{{%users}}', 'id', "CASCADE");

        $this->addForeignKey('fk-lands_map-land_id', '{{%lands_map}}', 'land_id', '{{%lands}}', 'id', "CASCADE");

        $this->addForeignKey('fk-land_points-garden_id', '{{%land_points}}', 'garden_id', '{{%gardens}}', 'id', "CASCADE");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200415_102854_editor_fk cannot be reverted.\n";

        return false;
    }
    */
}
