<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%gardens}}`.
 */
class m200415_101025_create_gardens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%gardens}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'category' => $this->string()->notNull(),
            'category_slug' => $this->string()->notNull(),
            'image' => $this->string()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gardens}}');
    }
}
