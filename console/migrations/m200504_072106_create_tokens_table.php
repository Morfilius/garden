<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tokens}}`.
 */
class m200504_072106_create_tokens_table extends Migration
{
    public function Up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tokens}}',[
            'id' => $this->primaryKey(),
            'token' => $this->string()->notNull()->unique(),
            'user_id' => $this->integer()->notNull(),
            'device_id' => $this->string()->notNull()
        ], $tableOptions);

        $this->createIndex('idx-token-user_id', '{{%tokens}}', 'user_id');
        $this->createIndex('idx-token-device_id', '{{%tokens}}', 'device_id');

        $this->addForeignKey('fk-token-user_id', '{{%tokens}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        $this->dropTable('{{%tokens}}');
    }
}
