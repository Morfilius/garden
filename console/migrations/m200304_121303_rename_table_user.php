<?php

use yii\db\Migration;

/**
 * Class m200304_121303_rename_table_user
 */
class m200304_121303_rename_table_user extends Migration
{
    public function up()
    {
        $this->renameTable('{{%user}}', '{{%users}}');
    }

    public function down()
    {
        $this->renameTable('{{%users}}', '{{%user}}');
    }
}
