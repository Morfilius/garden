<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dispatch}}`.
 */
class m200407_092423_create_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%dispatch}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'recipient_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'date_start' => $this->date()->notNull(),
            'date_end' => $this->date()->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

        $this->addForeignKey('fk-dispatch-recipient_id', '{{%dispatch}}', 'recipient_id', '{{%users}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dispatch}}');
    }
}
