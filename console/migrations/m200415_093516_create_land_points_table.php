<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%land_points}}`.
 */
class m200415_093516_create_land_points_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%land_points}}', [
            'id' => $this->primaryKey(),
            'lands_map_id' => $this->integer()->notNull(),
            'garden_id' => $this->integer()->notNull(),
            'radius' => $this->float()->notNull(),
            'xPx' => $this->float()->notNull(),
            'yPx' => $this->float()->notNull(),
            'xPerc' => $this->float()->notNull(),
            'yPerc' => $this->float()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%land_points}}');
    }
}
