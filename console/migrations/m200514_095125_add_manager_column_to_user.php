<?php

use yii\db\Migration;

/**
 * Class m200514_095125_add_manager_column_to_user
 */
class m200514_095125_add_manager_column_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%users}}', 'manager_id', $this->integer()->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%users}}', 'manager_id');
    }
}
