<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalog_relations}}`.
 */
class m200421_140408_create_catalog_relations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalog_relations}}', [
            'id' => $this->primaryKey(),
            'catalog_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('fk-catalog_relations-catalog_id', '{{%catalog_relations}}', 'catalog_id', '{{%catalog}}', 'id', "CASCADE");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalog_relations}}');
    }
}
