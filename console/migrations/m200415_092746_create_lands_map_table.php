<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lands}}`.
 */
class m200415_092746_create_lands_map_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%lands_map}}', [
            'id' => $this->primaryKey(),
            'land_id' => $this->integer()->notNull(),
            'image' => $this->string(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lands_map}}');
    }
}
