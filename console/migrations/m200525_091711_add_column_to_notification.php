<?php

use yii\db\Migration;

/**
 * Class m200525_091711_add_column_to_notification
 */
class m200525_091711_add_column_to_notification extends Migration
{
    public function up()
    {
        $this->addColumn('{{%notifications}}', 'read', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%notifications}}', 'read');
    }
}
