<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalog_features}}`.
 */
class m200421_135837_create_catalog_features_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%catalog_features}}', [
            'id' => $this->primaryKey(),
            'catalog_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-catalog_features-catalog_id', '{{%catalog_features}}', 'catalog_id', '{{%catalog}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalog_features}}');
    }
}
