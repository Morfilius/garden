<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m200428_142921_add_bitrix_id_column_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%users}}', 'bitrix_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%users}}', 'bitrix_id');
    }
}
