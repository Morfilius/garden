<?php

namespace common\bootstrap;


use api\pagination\RestPagination;
use backend\tests\app\mocks\BitrixApiMock;
use core\entities\Chat\Bitrix24\Chat;
use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\Config\RestConfig;
use core\services\Firebase\Firebase;
use Yii;
use yii\base\BootstrapInterface;
use yii\data\Pagination;
use yii\rest\Serializer;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;
        $container->setSingleton(BitrixApi::class, function () use ($app) {
            //return new BitrixApiMock(new RestConfig('local.5ea578456cd836.41828512', 'yQfBh5TGyYXYaL9e6VM8POfrqlMB6LjOEh6xGiPCGI4UYTMVry'));
            return new BitrixApi(new RestConfig('local.5ea578456cd836.41828512', 'yQfBh5TGyYXYaL9e6VM8POfrqlMB6LjOEh6xGiPCGI4UYTMVry'));
        });
        $container->setSingleton(Serializer::class, function () {
            $serializer = new Serializer();
            $serializer->collectionEnvelope = 'items';
            return $serializer;
        });
        $container->setSingleton(Chat::class, function () {
            return new Chat(\core\services\Bitrix24\Chat\Chat::$connector_id, \core\services\Bitrix24\Chat\Chat::$line_id);
        });
        $container->set(Pagination::class, function () {
            return new RestPagination();
        });
        $container->setSingleton(Firebase::class, function() {
            return new Firebase(
                ''
            );
        });
    }
}