<?php //*/10 * * * * cd /var/www/html/imperial && php yii bitrix/refresh
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'staticHostInfo' => 'http://static.imperial.loc',
    'staticPath' => dirname(__DIR__, 2) . '/static',

    'bitrix24' => [
        'url' => 'https://b24-mzjc4j.bitrix24.ru/rest/1/',
        'password' => 'ndrrpvig9k1ckgzw'
    ]
];
