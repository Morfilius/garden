<?php
namespace common\auth;

use core\entities\User\User;
use core\readRepositories\UserReadRepository;
use yii\web\IdentityInterface;

/***
 * @property User $user
*/

class Identity implements IdentityInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function findIdentity($id)
    {
        $user = static::getRepository()->findActiveById($id);
        return $user ? new self($user) : null;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = User::find()
            ->alias('u')
            ->joinWith('tokens t')
            ->andWhere(['t.token' => $token])
            ->andWhere(['>', 'u.status', User::STATUS_INACTIVE])
            ->one();

        if (!$user) return $user;
        return new self(User::find()
            ->alias('u')
            ->joinWith('tokens t')
            ->andWhere(['t.token' => $token])
            ->andWhere(['>', 'u.status', User::STATUS_INACTIVE])
            ->one());
    }

    public function getId() : int
    {
        return $this->user->id;
    }

    public function getAuthKey() : string
    {
        return $this->user->auth_key;
    }

    public function getUsername() : string
    {
        return $this->user->username;
    }

    public function validateAuthKey($authKey) : bool
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getUser() : User
    {
        return $this->user;
    }

    private static function getRepository() : UserReadRepository
    {
        return \Yii::$container->get(UserReadRepository::class);
    }
}