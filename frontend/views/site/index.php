<? use core\entities\Catalog\Catalog;
use yii\helpers\Url; ?>
<div class="container-fluid">
    <table class="table table-striped">
        <tr>
            <th>ID</th>
            <th>Картинка</th>
            <th>Название</th>
            <th>Свойства</th>
            <th>Аналоги</th>
        </tr>
    <? $catalog = Catalog::find()->with('features')->with('rels')->orderBy('product_id ASC')->all(); ?>
    <? $catalog = array_slice($catalog, 24)?>
        <?php foreach($catalog as $item):?>
            <tr>
                <td><?= $item->product_id ?></td>
                <td><img src="<?= $item->image ? $item->getUploadUrl('image') : Url::to('@static/default.png') ?>" alt=""></td>
                <td><?= $item->title ?></td>
                <td>
                    <?php if($item->features):?>
                        <?php foreach($item->features as $option):?>
                            <?= '<strong>'.$option->name . '</strong>' .' - '. $option->value.','?>
                        <?php endforeach;?>
                    <?php endif;?>
                </td>
                <td>
                    <?php if($item->features):?>
                        <?php foreach($item->rels as $option):?>
                            <?= $option->product_id.','?>
                        <?php endforeach;?>
                    <?php endif;?>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
</div>