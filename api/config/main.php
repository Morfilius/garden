<?php

use Swagger\Annotations\Swagger;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@staticRoot' => $params['staticPath'],
        '@static'   => $params['staticHostInfo'],
    ],
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => [
        'common\bootstrap\SetUp',
        'api\bootstrap\SetUp',
        'log',
        [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => 'json',
                'application/xml' => 'html',
            ],
        ],
    ],
    'modules' => [],
    'components' => [
        'request' => [
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response' => [
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null && $response->format !== 'html') {
                    $data = $response->data;

                    if ($data instanceof Swagger) return;

                    $error = '';

                    if( ! $response->isSuccessful) {
                        if(isset($data['message'])) {
                            $error = $data['message'];
                        } elseif(isset(current($data)['message'])) {
                            $error = current($data)['message'];
                        }
                    }

                    $response->data = [
                        'status' => $response->isSuccessful,
                        'code' => $response->statusCode,
                        'error' => (isset($data['stack-trace'])) ?$data : $error,
                    ];

                    if ($response->isSuccessful && is_array($data) && !empty($data)) {
                        $response->data['data'] = $data;
                    }

                    $response->statusCode = 200;
                }
            },
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\auth\Identity',
            'enableAutoLogin' => false,
            'enableSession' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'as access' => [
            'class' => 'yii\filters\AccessControl',
            'except' => ['auth/login'],
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',

                'api/doc' => 'swagger/swagger/docs',
                'api/json-schema' => 'swagger/swagger/json-schema',

                'GET map' => 'map/map',
                'GET map/test' => 'map/map/test',

                'POST auth/login' => 'auth/login',

                'GET user' => 'user/get',
                'PUT user' => 'user/edit',

                'GET news/list' => 'news/list',

                'GET dispatch/list' => 'dispatch/list',
                'GET dispatch/slider' => 'dispatch/slider',
                'POST dispatch/receive/<id:\d+>' => 'dispatch/receive',

                'GET deal/list' => 'deal/list',

                'GET land/base' => 'land/base-info',
                'GET land/text' => 'land/text',
                'GET land/plants' => 'land/plants',
                'GET land/plants-by-cat/<cat_id:\d+>' => 'land/plants-by-cat',

                'GET events' => 'events/index',
                'GET events/statistic' => 'events/statistic',

                'GET plant/<point_id:\d+>' => 'plant/index',

                'GET home' => 'home/index',

                'GET webview/news/<id:\d+>' => 'webview/news',
                'GET webview/dispatch/<id:\d+>' => 'webview/dispatch',
                'GET webview/land/audit/<id:\d+>' => 'webview/land-audit',
                'GET webview/land/rec/<id:\d+>' => 'webview/land-rec',

                'GET chat/list' => 'chat/list',
                'PUT chat/<chat_id:\d+>/message' => 'chat/add-message',
                'GET chat/<chat_id:\d+>/messages' => 'chat/get-messages',

                'POST from-bitrix/message' => 'from-bitrix/message',
                'POST from-bitrix/deal-status' => 'from-bitrix/deal-status',
                'POST from-bitrix/manager-change' => 'from-bitrix/manager-change',

                'POST catalog' => 'catalog/index',

                'GET notification/unread' => 'notification/unread',
                'GET notification/read' => 'notification/read',
            ],
        ]
    ],
    'params' => $params,
];
