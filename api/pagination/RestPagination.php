<?php


namespace api\pagination;


use yii\data\Pagination;
use yii\web\Link;

class RestPagination extends Pagination
{
    public function getLinks($absolute = false)
    {
        $currentPage = $this->getPage();
        $pageCount = $this->getPageCount();
        $links = [
            Link::REL_SELF => $this->createUrl($currentPage, null, $absolute),
        ];
        if ($currentPage > 0) {
            $links[self::LINK_FIRST] = $this->createUrl(0, null, $absolute);
            $links[self::LINK_PREV] = $this->createUrl($currentPage - 1, null, $absolute);
        } else {
            $links[self::LINK_FIRST] = '';
            $links[self::LINK_PREV] = '';
        }
        if ($currentPage < $pageCount - 1) {
            $links[self::LINK_NEXT] = $this->createUrl($currentPage + 1, null, $absolute);
            $links[self::LINK_LAST] = $this->createUrl($pageCount - 1, null, $absolute);
        } else {
            $links[self::LINK_NEXT] = '';
            $links[self::LINK_LAST] = '';
        }

        return $links;
    }
}