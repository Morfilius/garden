<?php

namespace api\bootstrap;


use api\pagination\RestPagination;
use core\entities\Chat\Bitrix24\Chat;
use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\Config\RestConfig;
use yii\base\BootstrapInterface;
use yii\data\Pagination;
use yii\rest\Serializer;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;

        $container->set(Pagination::class, function () {
            return new RestPagination();
        });
    }
}