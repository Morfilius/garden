<?php
namespace api\controllers;

use core\entities\Calendar\Bitrix24\Events;
use core\services\Bitrix24\BitrixApi;
use core\services\Bitrix24\Chat\Chat;
use core\useCases\ArriveGardenerService;
use core\useCases\ProductDeliveryService;
use DateTime;
use Yii;
use yii\helpers\Json;
use yii\rest\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @var BitrixApi
     */
    private $bitrixApi;

    public function __construct($id, $module, BitrixApi $bitrixApi, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->bitrixApi = $bitrixApi;
    }

    public function actionIndex()
    {
        //$d1 = BitrixApi::call('crm.deal.userfield.list', []);
//        $d = BitrixApi::call('crm.deal.list', [
//            "FILTER" => [
//                '>UF_CRM_1590393164808' => '2020-05-25T00:00:00+03:00',
//                '<UF_CRM_1590393164808' => '2020-05-25T23:59:00+03:00',
//            ],
//            "SELECT" => ['UF_CRM_1590393164808', 'TITLE'],
//        ]);
//        $test = Yii::$container->get(ProductDeliveryService::class);
//        $test->notify();
        $s = 1;
//        $d =  BitrixApi::call(
//            'event.bind',
//            [
//                'event' => 'onCrmContactUpdate',
//                'handler' => 'https://siteforyou.of.by/rest/handlers/manager-change.php',
//            ]
//        );
        $v = 1;


//        $ch = curl_init('http://test2finance.ru:7113/from-bitrix/message');
//        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($post) );
//        curl_setopt( $ch, CURLOPT_HTTPHEADER, [
//            'Expect:100-continue',
//            'Content-Type:application/x-www-form-urlencoded',
//            'Accept:*/*',
//            'User-Agent:Bitrix24 Webhook Engine',
//        ]);
//        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
//        $result = curl_exec($ch);
//        curl_close($ch);
//        return $result;
        $token = 'DwRdZMqh3z9Eq8fq8VDbUIU-4FblneHzs';
        $relations = [1];
        $features = [
            [
                "name" => "Влажность1",
                "value" => "Влаголюбивое1"
            ]
        ];
        $url = 'http://test2finance.ru:7113/catalog';
        $pathToFile = Yii::getAlias('@staticRoot/products/product1.jpg');

        function makePlant($url, $token, $productId, $title, $category, $pathToFile, array $relations = null, array $features = null) {
            $ext = preg_replace('/.*?\./', '', basename($pathToFile));
            $payload = json_encode(
                array_filter([
                    'product_id' => $productId,
                    'title'      => $title,
                    'category'   => $category,
                    'image'      => sprintf('data:image/%s;base64,%s', $ext, base64_encode(file_get_contents($pathToFile))),
                    'relations'  => $relations,
                    'features'   => $features
                ])
            );
            $ch = curl_init($url);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json', 'Authorization: Bearer ' . $token]);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }
        makePlant($url, $token, 1111112, "11Брусника необыкновенная123", "Бонсаи и топиары", $pathToFile, $relations, $features);



//        file_put_contents('1.txt', json_encode($_POST));
//        $request = Json::decode('{"event":"ONIMCONNECTORMESSAGEADD","data":{"CONNECTOR":"morf_connector","LINE":"2","MESSAGES":[{"im":{"chat_id":"150","message_id":"1626"},"message":{"user_id":"1","text":"[b]\u0414\u0435\u043d\u0438\u0441 \u0420\u0443\u0441\u0435\u0446\u043a\u0438\u0439:[\/b][br] fasdfasdfasdf"},"chat":{"id":"chat_imperial_garden4234234234234324234234234234234234231813"}}]},"ts":"1589554762","auth":{"access_token":"5abcbe5e0046d7700046d6e400000001000003dd8317f4f6a62e8c827319ea404afa4e","expires":"1589558362","expires_in":"3600","scope":"sale,im,imbot,placement,user,entity,pull,pull_channel,mobile,log,sonet_group,telephony,call,messageservice,forum,pay_system,mailservice,userconsent,smile,lists,faceid,landing,imopenlines,calendar,department,contact_center,documentgenerator,crm,task,tasks_extended,disk,intranet,salescenter,socialnetwork","domain":"b24-mzjc4j.bitrix24.ru","server_endpoint":"https:\/\/oauth.bitrix.info\/rest\/","status":"L","client_endpoint":"https:\/\/b24-mzjc4j.bitrix24.ru\/rest\/","member_id":"52762fda06c703c0c6d61cf89439fbf4","user_id":"1","application_token":"8e8f59751928dbce151213f53a8ccb3c"}}');
//        //Chat::$connector_id;
//        return $request;

        //        BitrixApi::callBatch([
//            'user_list' => [
//                'method' => 'crm.contact.get',
//                'params' => [
//                    'ID' => 44,
//                ]
//            ],
//            'send_messages' => [
//                'method' => 'imconnector.send.messages',
//                'params' => [
//                    'CONNECTOR' => Chat::$connector_id,
//                    'LINE' => Chat::$line_id,
//                    'MESSAGES' => [[
//                        'user' => [
//                            'id' => 'chat67a91281113111',
//                            'last_name' => '$result[user_list][LAST_NAME]',
//                            'name' => '$result[user_list][NAME]',
//                        ],
//                        'message' => [
//                            'id' => false,
//                            'date' => time(),
//                            'text' => '1вв123131test31111',
//                        ],
//                        'chat' => [
//                            'id' => 'chat67a9128113111',
//                        ],
//                    ]],
//                ]
//            ],
//            'last_id' => [
//                'method' => 'imopenlines.crm.chat.getLastId',
//                'params' => [
//                    'CRM_ENTITY_TYPE' => 'CONTACT',
//                    'CRM_ENTITY' => '44',
//                ]
//            ]
//        ]);

//        Chat::sendMessage(
//            'dsfasdftest',
//            'chat67a9128a29268477f5c3a717217f5eea898423ccd6bb60266c29ead57101b88d',
//            'chat67a9128a29268477f5c3a717217f5eea898423ccd6bb60266c29ead57101b88d',
//            '1',
//            'Пользователь',
//            'Пользователевич'
//        );
//        $result = BitrixApi::call(
//                'imconnector.send.messages',
//                [
//                    'CONNECTOR' => Chat::$connector_id,
//                    'LINE' => 1,
//                    'MESSAGES' => [[
//                        'user' => [
//                            'id' => 'chat67a9128a29268477f5c3a73123123117217f5eea898423ccd6bb60266c29ead57101b88d1234',
//                            'last_name' => 'Пользователевич',
//                            'name' => 'Пользователь',
//                        ],
//                        'message' => [
//                            'id' => false,
//                            'date' => time(),
//                            'text' => '1test311',
//                        ],
//                        'chat' => [
//                            'id' => 'chat67a9128a29268477f5c3a7173231231217f5eea898423ccd6bb60266c29ead57101b88d',
//                        ],
//                    ]],
//                ]
//            );
//        return $result;
    }
}