<?php


namespace api\controllers;


use api\providers\MapDataProvider;
use core\entities\News;
use core\readRepositories\NewsReadRepository;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Url;

class NewsController extends BaseApiController
{
    private $news;

    public function __construct($id, $module, NewsReadRepository $news, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->news = $news;
    }

    /**
     * @SWG\Get(
     *     path="/news/list",
     *     tags={"News"},
     *     description="Список новостей, поле 'text' содержит ссылку на WebView",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         description="",
     *         response="200",
     *         @SWG\Schema(ref="#/definitions/NewsList")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionList()
    {
        $dataProvider = $this->news->showAllProvider();
        return new MapDataProvider($dataProvider, [$this, 'serializeListItem']);
    }

    /**
     * @SWG\Definition(
     *      definition="NewsList",
     *      @SWG\Property(property="items", type="array",  @SWG\Items(type="object",
     *          @SWG\Property(property="title", type="string", example="Заголовок новости"),
     *          @SWG\Property(property="image", type="string", example="http://static.imperial.loc/cache/news/wide-5eaee11a98ba4.jpg"),
     *          @SWG\Property(property="text", type="string", example="http://api.imperial.loc/text/20"),
     *      )
     *  ),
     *  @SWG\Property(property="_links", type="object",
     *          @SWG\Property(property="self", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=2")),
     *          @SWG\Property(property="first", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="prev", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="next", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=3")),
     *          @SWG\Property(property="last", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=4")),
     *      ),
     *  @SWG\Property(property="_meta", type="object",
     *          @SWG\Property(property="totalCount", type="integer", example=4),
     *          @SWG\Property(property="pageCount", type="integer", example=4),
     *          @SWG\Property(property="currentPage", type="integer", example=2),
     *          @SWG\Property(property="perPage", type="integer", example=1),
     *      )
     * )
     */

    public function serializeListItem(News $news)
    {
        return [
            'title' => $news->title,
            'image' => $news->getUploadUrl('image'),
            'text' => Url::to(['webview/news', 'id' => $news->id], true),
        ];
    }
}