<?php

namespace api\controllers\swagger;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class SwaggerController extends Controller
{
    /**
     * @SWG\Swagger(
     *     basePath="/",
     *     produces={"application/json"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Info(
     *          version="1.0",
     *          title="ImperialGarden API",
     *          description="URL для запросов к API - https://api.igsad.ru. Токен для доступа можно получить по адресу /auth/login, в инструкции есть описание метода. 'Дорогие' запросы - это запросы к api bitrix24, у нас есть ограничение 2 запроса/сек со ВСЕХ устройств + админка. Поэтому из крайне желательно как то кэшировать на устройстве (хотя бы пользователя и календарь)"
     *      ),
     * )
     */

    /**
     *  @SWG\SecurityScheme(
     *      securityDefinition="Bearer",
     *      name="Authorization",
     *      type="apiKey",
     *      description="В Value токен задается в формате - Bearer <токен>, например Bearer 16LlFGR2...",
     *      in="header",
     *  )
     */

    public function actions(): array
    {
        return [
            'docs' => [
                'class' => 'yii2mod\swagger\SwaggerUIRenderer',
                'restUrl' => Url::to(['api/json-schema']),
            ],
            'json-schema' => [
                'class' => 'yii2mod\swagger\OpenAPIRenderer',
                // Тhe list of directories that contains the swagger annotations.
                'scanDir' => [
                    Yii::getAlias('@api/controllers'),
                    //Yii::getAlias('@app/models'),
                ],
                'cacheDuration' => 1
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}