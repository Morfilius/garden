<?php


namespace api\controllers;


use api\providers\MapDataProvider;
use core\entities\News;
use core\entities\Notification\Notification;
use core\readRepositories\NewsReadRepository;
use core\useCases\NotificationService;
use DateTime;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Url;

class NotificationController extends BaseApiController
{
    /**
     * @var NotificationService
     */
    private $notificationService;

    public function __construct($id, $module, NotificationService $notificationService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->notificationService = $notificationService;
    }

    /**
     * @SWG\Get(
     *     path="/notification/read",
     *     tags={"Notification"},
     *     description="Список уведомлений",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         description="",
     *         response="200",
     *         @SWG\Schema(ref="#/definitions/NotificationsList")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionUnread()
    {
        return new MapDataProvider($this->notificationService->unreadList(Yii::$app->user->id), [$this, 'serializeList']);
    }

    /**
     * @SWG\Get(
     *     path="/notification/unread",
     *     tags={"Notification"},
     *     description="Список уведомлений",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         description="",
     *         response="200",
     *         @SWG\Schema(ref="#/definitions/NotificationsList")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionRead()
    {
        return new MapDataProvider($this->notificationService->readList(Yii::$app->user->id), [$this, 'serializeList']);
    }

    public function serializeList(Notification $notification)
    {
        return [
            'text' => $notification->text,
            'date' => Yii::$app->formatter->asDatetime($notification->updated_at, 'php:d F Y'),
        ];
    }

    /**
     * @SWG\Definition(
     *      definition="NotificationsList",
     *      @SWG\Property(property="items", type="array",  @SWG\Items(type="object",
     *          @SWG\Property(property="text", type="string", example="Ваш заказ будет доставлен сегодня4 в 14:05"),
     *          @SWG\Property(property="date", type="string", example="25 мая 2020"),
     *      )
     *  ),
     *  @SWG\Property(property="_links", type="object",
     *          @SWG\Property(property="self", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=2")),
     *          @SWG\Property(property="first", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="prev", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="next", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=3")),
     *          @SWG\Property(property="last", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=4")),
     *      ),
     *  @SWG\Property(property="_meta", type="object",
     *          @SWG\Property(property="totalCount", type="integer", example=4),
     *          @SWG\Property(property="pageCount", type="integer", example=4),
     *          @SWG\Property(property="currentPage", type="integer", example=2),
     *          @SWG\Property(property="perPage", type="integer", example=1),
     *      )
     * )
     */
}