<?php


namespace api\controllers;

use api\providers\MapDataProvider;
use core\entities\Chat\Chat;
use core\entities\Chat\Image;
use core\entities\Chat\Message;
use core\entities\User\Bitrix24\Employee;
use core\entities\User\User;
use core\forms\api\MessageForm;
use core\repositories\ChatRepository;
use core\useCases\api\Chat\ChatService;
use Yii;
use yii\helpers\StringHelper;

class ChatController extends BaseApiController
{
    protected $excludedActions = ['message-from-bitrix'];
    /**
     * @var ChatService
     */
    private $service;
    /**
     * @var ChatRepository
     */
    private $repository;

    public function __construct($id, $module,
                                ChatService $service,
                                ChatRepository $repository,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * @SWG\Get(
     *     path="/chat/list",
     *     tags={"Chat"},
     *     description="Список чатов",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         description="",
     *         response="200",
     *         @SWG\Schema(ref="#/definitions/ChatList")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionList()
    {
        $manager = $this->getManager();
        $this->service->createChats(Yii::$app->user->id);

        return ['chats' => array_map(function (Chat $chat) use($manager) {
            $lastMessage = $chat->getLastMessage();
            return [
                'chat_id' => $chat->id,
                'name' => $chat->isWithManager() ? StringHelper::truncate($manager->getFullName(), 20) : 'Чат с садовником',
                'image' => $chat->isWithManager() ? $manager->image : $chat->getDefaultImage(),
                'lastMessage' => $lastMessage ? StringHelper::truncate($lastMessage->text, 20) : ''
            ];
        }, $this->repository->getList(Yii::$app->user->id))];
    }

    /**
     * @SWG\Put(
     *     path="/chat/{id}/message",
     *     tags={"Chat"},
     *     description="Новое сообщение в чат. Пишите адекватные сообщения, их видит клиент.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Если есть картинки, то можно оставлять поле message пустым, если картинок нет то оно обязательно для заполнения (минимум 1 символ)",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", example="Тестовое сообщение"),
     *              @SWG\Property(property="images", type="array", @SWG\Items(
     *                          type="string",
     *                          enum={"data:image/jpeg;base64,/9j/4AAQSkZ..."}
     *                      )
     *              ),
     *          )
     *      ),
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *     ),
     *     security={{"Bearer": {}}}
     * )
     */

    public function actionAddMessage($chat_id)
    {
        $form = new MessageForm();
        if ($form->load(Yii::$app->request->bodyParams, '') && $form->validate()) {
            $this->service->addMessage($form, $chat_id, Yii::$app->user->id);
            return true;
        }

        return $form;
    }

    /**
     * @SWG\Get(
     *     path="/chat/{id}/messages",
     *     tags={"Chat"},
     *     description="Список сообщений чата",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         description="У поля type есть 2 значения: employee - сообщения сотрудника, client - сообщения клиента. (ваш Кэп (^.^))",
     *         response="200",
     *         @SWG\Schema(ref="#/definitions/ChatMessages")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */


    public function actionGetMessages($chat_id)
    {
        /** @var User $user*/
        $employee = $this->getEmployee($chat_id);
        $isPrevEmployee = false;

        return new MapDataProvider($this->repository->showMessagesProvider($chat_id), function (Message $message) use($employee, &$isPrevEmployee) {
            $isNeedInfo = ($message->isEmployee() && !$isPrevEmployee);
            $message->isEmployee() ? $isPrevEmployee = true : $isPrevEmployee = false;
            return [
                'image' => $isNeedInfo ? $employee->image : '',
                'name' => $isNeedInfo ? $employee->first_name . ' ' . $employee->last_name : '',
                'text' => $message->text,
                'isMy' => $message->isUserMessage(),
                'time' => date('H:i', $message->created_at),
                'images' => array_map(function (Image $image) {
                    return $image->getThumbnail('small');
                }, $message->image)
            ];
        });
    }

    private function getEmployee($chat_id)
    {
        /** @var User $user*/
        $chat = $this->repository->get($chat_id);
        if ($chat->isWithManager()) {
            $user = Yii::$app->user->identity->getUser();
            return $user->getManager();
        } else {
            $user = $chat->owner;
            $land = $user->getLandOrFail();
            return $land->gardener;
        }
    }

    private function getManager() : Employee
    {
        /** @var User $user*/
        $user = Yii::$app->user->identity->getUser();
        return $user->getManager();
    }

    /**
     * @SWG\Definition(
     *      definition="ChatMessages",
     *      @SWG\Property(property="items", type="array",  @SWG\Items(type="object",
     *          @SWG\Property(property="image", type="string", example="https://cdn-ru.bitrix24.ru/b13799034/main/d6d/d6dcc2c169dfeba3d6b38e210fa9243f/kot_seryj_polosatyj_149407_1920x1080.jpg"),
     *          @SWG\Property(property="name", type="string", example="Денис Потапов"),
     *          @SWG\Property(property="text", type="string", example="Тестовое сообщение"),
     *          @SWG\Property(property="isMy", type="boolean", example=true),
     *          @SWG\Property(property="time", type="string", example="10:06"),
     *          @SWG\Property(property="images", type="array", @SWG\Items(
     *                          type="string",
     *                          enum={"https://b24-mzjc4j.bitrix24.ru/~62Bt4#img.jpg"}
     *                      )
     *          )
     *      )
     *  ),
     *  @SWG\Property(property="_links", type="object",
     *          @SWG\Property(property="self", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=2")),
     *          @SWG\Property(property="first", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="prev", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="next", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=3")),
     *          @SWG\Property(property="last", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=4")),
     *      ),
     *  @SWG\Property(property="_meta", type="object",
     *          @SWG\Property(property="totalCount", type="integer", example=4),
     *          @SWG\Property(property="pageCount", type="integer", example=4),
     *          @SWG\Property(property="currentPage", type="integer", example=2),
     *          @SWG\Property(property="perPage", type="integer", example=1),
     *      )
     * )
     */

    /**
     * @SWG\Definition(
     *      definition="ChatList",
     *      @SWG\Property(property="chats", type="array",  @SWG\Items(type="object",
     *          @SWG\Property(property="chat_id", type="integer", example=12),
     *          @SWG\Property(property="name", type="string", example="Чат с садовником"),
     *          @SWG\Property(property="image", type="string", example="http://static.imperial.loc/garden-chat.png"),
     *          @SWG\Property(property="lastMessage", type="string", example="Тестовое сообщение")
     *      )
     *  )
     * )
     */
}