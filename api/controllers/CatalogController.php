<?php


namespace api\controllers;


use core\forms\api\CatalogForm;
use core\useCases\api\CatalogApiService;
use Yii;
use yii\rest\Controller;

class CatalogController extends BaseApiController
{
    /**
     * @var CatalogApiService
     */
    private $service;

    public function __construct($id, $module, CatalogApiService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex()
    {
        $form = new CatalogForm();
        if($form->load(Yii::$app->request->bodyParams, '') && $form->validate()) {
            return $this->service->create($form);
        }
        return $form;
    }
}