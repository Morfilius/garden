<?php


namespace api\controllers;


use core\traits\RestControllerTrait;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

class BaseApiController extends Controller
{
    use RestControllerTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['authMethods'] = [
            HttpBearerAuth::className(),
        ];

        return $behaviors;
    }
}