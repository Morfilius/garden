<?php


namespace api\controllers;


use api\providers\MapDataProvider;
use core\entities\Banners;
use core\entities\Dispatch;
use core\readRepositories\DispatchReadRepository;
use core\useCases\api\DealApiService;
use Yii;
use yii\helpers\Url;

class DispatchController extends BaseApiController
{
    protected $excludedActions = ['receive'];

    private $dispatch;
    /**
     * @var DealApiService
     */
    private $dealApiService;

    public function __construct($id, $module,
                                DispatchReadRepository $dispatch,
                                DealApiService $dealApiService,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->dispatch = $dispatch;
        $this->dealApiService = $dealApiService;
    }

    /**
     * @SWG\Get(
     *     path="/dispatch/list",
     *     tags={"Dispatch"},
     *     description="Список рассылок",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Список с пагинацией, поле text содержит ссылку на WebView",
     *         @SWG\Schema(ref="#/definitions/DispatchList")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionList()
    {
        $dataProvider = $this->dispatch->showAllProvider(Yii::$app->user->id);
        return new MapDataProvider($dataProvider, [$this, 'serializeListItem']);
    }


    /**
     * @SWG\POST(
     *     path="/dispatch/receive/{id}",
     *     tags={"Dispatch"},
     *     description="Сделать заказ, id рассылки можно взять из списка dispatch/list",
     *     consumes={"application/json"},
     *     @SWG\Parameter(name="id", in="path", required=true, type="integer"),
     *     @SWG\Response(
     *          description="",
     *         response="200"
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    /* todo добавить проверку, что рассылка принадлежит юзеру, если понадобится */
    public function actionReceive($id)
    {
        $this->dealApiService->create(Yii::$app->user->id, $id);
        return true;
    }

    /**
     * @SWG\Definition(
     *      definition="DispatchList",
     *      @SWG\Property(property="items", type="array",  @SWG\Items(type="object",
     *          @SWG\Property(property="id", type="integer", example="1"),
     *          @SWG\Property(property="title", type="string", example="Заголовок рассылки"),
     *          @SWG\Property(property="bannerShow", type="bool", example="true"),
     *          @SWG\Property(property="bannerTitle", type="string", example="Заголовок баннера"),
     *          @SWG\Property(property="bannerImage", type="string", example="http://static.imperial.loc/cache/dispatch/wide-5eaee11a98ba4.jpg"),
     *          @SWG\Property(property="image", type="string", example="http://static.imperial.loc/cache/dispatch/wide-5eaee11a98ba4.jpg"),
     *          @SWG\Property(property="text", type="string", example="http://api.imperial.loc/text/20"),
     *      )
     *  ),
     *  @SWG\Property(property="_links", type="object",
     *          @SWG\Property(property="self", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=2")),
     *          @SWG\Property(property="first", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="prev", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="next", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=3")),
     *          @SWG\Property(property="last", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=4")),
     *      ),
     *  @SWG\Property(property="_meta", type="object",
     *          @SWG\Property(property="totalCount", type="integer", example=4),
     *          @SWG\Property(property="pageCount", type="integer", example=4),
     *          @SWG\Property(property="currentPage", type="integer", example=2),
     *          @SWG\Property(property="perPage", type="integer", example=1),
     *      )
     * )
     */

    public function serializeListItem(Dispatch $dispatch)
    {
        return [
            'id' => $dispatch->id,
            'title' => $dispatch->title,
            'bannerShow' => (bool)$dispatch->banner_show,
            'bannerTitle' => $dispatch->banner_title,
            'bannerImage' => $dispatch->getUploadUrl('image_wide'),
            'image' => $dispatch->getUploadUrl('image'),
            'text' => Url::to(['webview/dispatch', 'id' => $dispatch->id], true),
        ];
    }
}