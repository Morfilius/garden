<?php


namespace api\controllers;


use core\entities\Catalog\Catalog;
use core\entities\Catalog\CatalogFeatures;
use core\entities\Catalog\CatalogRelations;
use core\repositories\CatalogRepository;
use core\repositories\Land\PointsRepository;
use Yii;
use yii\helpers\Url;

class PlantController extends BaseApiController
{
    /**
     * @var PointsRepository
     */
    private $pointsRepository;
    /**
     * @var CatalogRepository
     */
    private $catalogRepository;

    public function __construct($id, $module,
                                PointsRepository $pointsRepository,
                                CatalogRepository $catalogRepository,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->pointsRepository = $pointsRepository;
        $this->catalogRepository = $catalogRepository;
    }

    /**
     * @SWG\Get(
     *     path="/plant/{id}",
     *     tags={"Plant"},
     *     description="Подробная информация о растении, id - point_id из land/plants-by-cat/",
     *     consumes={"application/json"},
     *     @SWG\Parameter(name="id", in="path", required=true, type="integer"),
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *         @SWG\Schema(ref="#/definitions/PlantsByPoint")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionIndex($point_id)
    {
        $point = $this->pointsRepository->get($point_id);
        $catalog = $point->getCatalogOrDie();
        $info = $point->getInfoOrDie();
        return $this->serializePlant($catalog, ['personal_rec' => $info->text]);
    }



    /**
     * @SWG\Definition(
     *      definition="PlantsByPoint",
     *      @SWG\Property(property="image", type="string", example="http://static.imperial.loc/products/product9.jpg"),
     *      @SWG\Property(property="name", type="string", example="Брусника обыкновенная0"),
     *      @SWG\Property(property="personal_rec", type="string", example="Персональные рекомендации по уходу 123123"),
     *      @SWG\Property(property="features", type="array", @SWG\Items(
     *         type="object",
     *         @SWG\Property(property="name", type="string", example="Свет"),
     *         @SWG\Property(property="value", type="string", example="светолюбивое"),
     *     )),
     *      @SWG\Property(property="relations", type="array", @SWG\Items(
     *         type="object",
     *         @SWG\Property(property="image", type="string", example="http://static.imperial.loc/products/product14.jpg"),
     *         @SWG\Property(property="name", type="string", example="Брусника обыкновенная 'Koralle'9"),
     *     )),
     * )
     */

    public function serializePlant(Catalog $catalog, $additional = [])
    {
        return array_merge([
            'image' => $catalog->image ? $catalog->getThumbUploadUrl('image', 'wide') : Url::to('@static/default.png'),
            'name' => $catalog->title,
            'features' => array_map(function (CatalogFeatures $item) {
                return [
                    'name' => $item->name,
                    'value' => $item->value
                ];
            }, $catalog->features),
            'relations' => array_filter(array_reduce($catalog->rels, function ($result, CatalogRelations $item) {
                if (!$item->catalog) return false;
                $result[] = [
                    'image' => $item->catalog->image ? $item->catalog->getThumbUploadUrl('image', 'grid') : Url::to('@static/default.png'),
                    'name' => $item->catalog->title
                ];
                return $result;
            }, []))
        ], $additional);
    }

}