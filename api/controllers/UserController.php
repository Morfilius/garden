<?php


namespace api\controllers;


use core\entities\User\Bitrix24\Profile;
use core\entities\User\User;
use core\forms\manage\User\Rest\UserEditForm;
use Yii;

class UserController extends BaseApiController
{
    /**
     * @SWG\Get(
     *     path="/user",
     *     tags={"User"},
     *     description="Информация о текущем пользователе, 'дорогой' запрос",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="image в base64",
     *         @SWG\Schema(ref="#/definitions/User")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionGet()
    {
        return $this->serializeUser(User::findOne(Yii::$app->user->id));
    }

    /**
     * @SWG\Put(
     *     path="/user",
     *     tags={"User"},
     *     description="Редактировать текущего пользователя 'дорогой' запрос",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="image отсылать в base64, строго в формате как в примере (форматы jpg/png) - data:image/jpg;base64,картинка в base64 ",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="first_name", type="string", example="Денис"),
     *              @SWG\Property(property="last_name", type="string", example="Потапов"),
     *              @SWG\Property(property="image", type="string", example="data:image/jpeg;base64,/9j/4AAQSkZJR..."),
     *              @SWG\Property(property="email", type="string", example="den@tut.by"),
     *              @SWG\Property(property="phone", type="string", example="7683008822"),
     *          )
     *
     *      ),
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *     ),
     *     security={{"Bearer": {}}}
     * )
     */

    public function actionEdit()
    {
        $form = new UserEditForm();
        if ($form->load(Yii::$app->request->bodyParams, '') && $form->validate()) {
            $user = User::findOne(Yii::$app->user->id);
            $profile = Profile::editRest(
                $user->bitrix_id,
                $form->first_name,
                $form->last_name,
                $form->phone,
                $form->email,
                $form->image
            );
            $profile->update();
            return true;
        }
        return $form;
    }

    /**
     * @SWG\Definition(
     *      definition="User",
     *       @SWG\Property(property="id", type="integer", example=37),
     *       @SWG\Property(property="first_name", type="string", example="Денис"),
     *       @SWG\Property(property="last_name", type="string", example="Потапов"),
     *       @SWG\Property(property="email", type="string", example="user@user.com"),
     *       @SWG\Property(property="phone", type="string", example="74234234234"),
     *       @SWG\Property(property="image", type="string", example="data:image/jpg;base64,/9j/4AAQSk..."),
     * )
     */

    private function serializeUser(User $user)
    {
        $profile = $user->profile;
        return [
            'id' => $user->id,
            'first_name' => $profile->first_name,
            'last_name' => $profile->last_name,
            'email' => $profile->email,
            'phone' => $profile->phone,
            'image' => $profile->image
        ];
    }

}