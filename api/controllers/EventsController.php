<?php


namespace api\controllers;


use core\entities\Calendar\Bitrix24\Calendar;
use core\entities\Calendar\Bitrix24\Events;
use core\entities\User\User;
use Yii;

class EventsController extends BaseApiController
{
    /**
     * @SWG\Get(
     *     path="/events",
     *     tags={"Events"},
     *     description="Календарь с событиями, 'дорогой' запрос",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Для получения событий определенного дня, необходимо распарсить и отфильтровать данный json",
     *         @SWG\Schema(ref="#/definitions/Calendar")
     *     ),
     *     security={{"Bearer": {}}}
     * )
     */

    public function actionIndex()
    {
        $land = Yii::$app->user->identity->getUser()->getLandOrFail();
        $events = new Events($land->calendar_id);
        return $events->getListPerMonthWithInfo();
    }


    /**
     * @SWG\Get(
     *     path="/events/statistic",
     *     tags={"Events"},
     *     description="Статистика, 'дорогой' запрос",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *         @SWG\Schema(ref="#/definitions/CalendarStatistic")
     *     ),
     *     security={{"Bearer": {}}}
     * )
     */

    public function actionStatistic()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity->getUser();
        $land = $user->getLandOrFail();
        $events = new Events($land->calendar_id);
         return [
             'total_statistic' => $events->getTotalStatistic(),
             'work_list' => $events->getWorkList()
         ];
    }

    /**
     * @SWG\Definition(
     *      definition="Calendar",
     *      @SWG\Property(property="month", type="array",  @SWG\Items(type="object",
     *          @SWG\Property(property="month", type="integer", example=5),
     *          @SWG\Property(property="days", type="array", @SWG\Items(
     *              type="object",
     *                  @SWG\Property(property="day", type="integer", example=4),
     *                  @SWG\Property(property="events", type="array", @SWG\Items(
     *                      type="object",
     *                          @SWG\Property(property="gardener", type="string", example="Алексей Иванович"),
     *                          @SWG\Property(property="staff", type="string", example="бригадир"),
     *                          @SWG\Property(property="work", type="string", example="полив, посадка"),
     *                          @SWG\Property(property="price", type="integer", example=5000),
     *                          @SWG\Property(property="date_from", type="object",
     *                              @SWG\Property(property="hour", type="integer", example=17),
     *                              @SWG\Property(property="minute", type="integer", example=20)
     *                          ),
     *                          @SWG\Property(property="date_to", type="object",
     *                              @SWG\Property(property="hour", type="integer", example=19),
     *                              @SWG\Property(property="minute", type="integer", example=20)
     *                          ),
     *                     ),
     *                  )
     *              )
     *          ),
     *      )
     *  )
     * )
     */

    /**
     * @SWG\Definition(
     *      definition="CalendarStatistic",
     *  @SWG\Property(property="total_statistic", type="object",
     *          @SWG\Property(property="visits_per_month", type="integer", example=3),
     *          @SWG\Property(property="work_per_month", type="integer", example=7),
     *          @SWG\Property(property="price_per_month", type="integer", example=9200),
     *      ),
     *  @SWG\Property(property="work_list", type="array", @SWG\Items(
     *         type="object",
     *              @SWG\Property(property="work", type="string", example="Удобрение газона, полив, обрезка, высадка 10 туй, удобрение кустарников."),
     *              @SWG\Property(property="price", type="integer", example="4200"),
     *              @SWG\Property(property="date", type="string", example="08.05.2020"),
     *              @SWG\Property(property="gardener", type="string", example="Алексей Иванович"),
     *              @SWG\Property(property="staff", type="string", example="бригадир"),
     *          )
     *      ),
     * )
     */

}