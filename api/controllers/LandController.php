<?php


namespace api\controllers;


use core\entities\Catalog\Catalog;
use core\entities\Catalog\Category;
use core\entities\Land\Points;
use core\readRepositories\CatalogReadRepository;
use core\readRepositories\CategoryReadRepository;
use core\readRepositories\PointsReadRepository;
use core\repositories\Land\LandRepository;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class LandController extends BaseApiController
{
    /**
     * @var LandRepository
     */
    private $repository;
    /**
     * @var PointsReadRepository
     */
    private $pointsReadRepository;
    /**
     * @var CategoryReadRepository
     */
    private $categoryReadRepository;

    public function __construct($id, $module,
                                LandRepository $repository,
                                PointsReadRepository $pointsReadRepository,
                                CategoryReadRepository $categoryReadRepository,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->repository = $repository;
        $this->pointsReadRepository = $pointsReadRepository;
        $this->categoryReadRepository = $categoryReadRepository;
    }

    /**
     * @SWG\Get(
     *     path="/land/text",
     *     tags={"Land"},
     *     description="Текстовая информациия об участке",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Поля содержат ссылки на WebView",
     *         @SWG\Schema(ref="#/definitions/LandText")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionText()
    {
        $land = Yii::$app->user->identity->getUser()->getLandOrFail();
        return [
            'audit' => Url::to(['webview/land-audit', 'id' => $land->id], true),
            'rec' => Url::to(['webview/land-rec', 'id' => $land->id], true),
        ];

    }

    /**
     * @SWG\Get(
     *     path="/land/base",
     *     tags={"Land"},
     *     description="Базовая информациия об участке",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *         @SWG\Schema(ref="#/definitions/LandBaseInfo")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionBaseInfo()
    {
        $land = Yii::$app->user->identity->getUser()->getLandOrFail();

        if (empty($land->map)) {
            throw new NotFoundHttpException('Land map not found.');
        }

        return [
            'image' => $land->map->getThumbUploadUrl('image', 'report'),
            'gardener_last' => $land->events->getPrev(),
            'gardener_next' => $land->events->getNext(),
            'address' => $land->address,
            'gardener' => $land->gardener->first_name
        ];
    }

    /**
     * @SWG\Get(
     *     path="/land/plants",
     *     tags={"Land"},
     *     description="Категории растений участка",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *         @SWG\Schema(ref="#/definitions/Plants")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionPlants()
    {
        $land = $this->repository->getByUser(Yii::$app->user->id);
        $map = $land->getMapOrDie();
        $categoryIds = $this->pointsReadRepository->getCategoryIdsByRecNotNull($map->id);
        return ['plants' => array_map(function (Category $category) {
            return [
                'category_id' => $category->id,
                'image' => $category->image ? Yii::getAlias('@static/category/' . $category->image) : Url::to('@static/default.png'),
                'name' => $category->name,

            ];
        }, $this->categoryReadRepository->showByIds($categoryIds))];
    }

    /**
     * @SWG\Get(
     *     path="/land/plants-by-cat/{cat_id}",
     *     tags={"Land"},
     *     description="Информация о растениях в категории, {cat_id} - id категории, можно получить в /land/plants",
     *     consumes={"application/json"},
     *     @SWG\Parameter(name="cat_id", in="path", required=true, type="integer"),
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *         @SWG\Schema(ref="#/definitions/PlantsByCat")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     * @param $cat_id
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */

    public function actionPlantsByCat($cat_id)
    {
        $land = $this->repository->getByUser(Yii::$app->user->id);
        $map = $land->getMapOrDie();
        return ['plants' => array_map(function (Points $point) {
            return [
                'point_id' => $point->id,
                'image' => $point->catalog->image ? $point->catalog->getThumbUploadUrl('image', 'grid') : Url::to('@static/default.png'),
                'name' => $point->catalog->title
            ];
        }, $this->pointsReadRepository->showListMapCat($map->id, $cat_id))];
    }

    /**
     * @SWG\Definition(
     *      definition="LandBaseInfo",
     *     @SWG\Property(property="image", type="string", example="http://static.imperial.loc/cache/map/report-5eb408302a091.png"),
     *     @SWG\Property(property="gardener_last", type="string", example="08.05.2020"),
     *     @SWG\Property(property="gardener_next", type="string", example="10.05.2020"),
     *     @SWG\Property(property="address", type="string", example="Адрес участка"),
     *     @SWG\Property(property="gardener", type="string", example="Бригадир1")
     * )
     */

    /**
     * @SWG\Definition(
     *      definition="LandText",
     *     @SWG\Property(property="audit", type="string", example="http://example/text/20"),
     *     @SWG\Property(property="rec", type="string", example="http://example/text/20"),
     * )
     */

    /**
     * @SWG\Definition(
     *      definition="Plants",
     *     @SWG\Property(property="plants", type="array", @SWG\Items(
     *         type="object",
     *         @SWG\Property(property="category_id", type="integer", example=4),
     *         @SWG\Property(property="image", type="string", example="http://static.imperial.loc/category/category_4.jpg"),
     *         @SWG\Property(property="name", type="string", example="Лиственные деревья"),
     *     )),
     * )
     */

    /**
     * @SWG\Definition(
     *      definition="PlantsByCat",
     *     @SWG\Property(property="plants", type="array", @SWG\Items(
     *         type="object",
     *         @SWG\Property(property="point_id", type="integer", example=15),
     *         @SWG\Property(property="image", type="string", example="http://static.imperial.loc/category/category_4.jpg"),
     *         @SWG\Property(property="name", type="string", example="Лиственные деревья"),
     *     )),
     * )
     */

}