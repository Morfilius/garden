<?php

namespace api\controllers\map;

use core\entities\Banners;
use core\entities\Land\Points;
use core\readRepositories\LandReadRepository;
use core\readRepositories\PointsReadRepository;
use DateTime;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

class MapController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['authMethods'] = [
            HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    /**
     * @var LandReadRepository
     */
    private $landReadRepository;
    /**
     * @var PointsReadRepository
     */
    private $pointsReadRepository;

    public function __construct($id, $module,
                                LandReadRepository $landReadRepository,
                                PointsReadRepository $pointsReadRepository, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->landReadRepository = $landReadRepository;
        $this->pointsReadRepository = $pointsReadRepository;
    }

    /**
     * @SWG\Get(
     *     path="/map",
     *     tags={"Map"},
     *     description="Получить карту участка, растения на карте, общую информацию об участке",
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *     @SWG\Schema(ref="#/definitions/Map")
     *     ),
     *     security={{"Bearer": {}}}
     * )
     */

    public function actionIndex()
    {
        $land = $this->landReadRepository->getByUser(Yii::$app->user->id);
        $map = $land->getMapOrDie();
        $points = $this->pointsReadRepository->showListWithClass($map->id);
        $banner = Banners::findOne(['number' => 7]);
        return [
            'map' => $map->getUploadUrl('image'),
            'banner' => [
                'image' => $banner->getThumbUploadUrl('image', 'banner'),
                'text' => $banner->text
            ],
            'info' => [
                'totalArea' => $land->getTotalAreaWithWord() ?? '-',
                'totalPlants' => $land->getTotalPlants() ?? '-',
                'age' => $land->getAgeWithWord(),
                'deciduousCount' => $land->deciduous_count ?? '-',
                'pinesCount' => $land->pine_count ?? '-',
                'lawnArea' => $land->lawn_area ? $land->getLawnAreaWithWord() : '-'
            ],
            'points' => array_filter(array_map(function (Points $point) {
                    if (!$point->info || !$point->catalog) return false;
                    return [
                        'id' => $point->id,
                        'top' => $point->xPerc,
                        'left' => $point->yPerc,
                        'image' => Yii::getAlias('@static/gardens/' . $point->garden->image),
                        'radius' => $point->radius,
                        'info' => [
                            'name' => $point->catalog->title,
                            'age' => $point->info->getAge(),
                            'date' => (new DateTime($point->info->landing_date))->format('d.m.Y')
                        ]
                    ];
                }, $points))
        ];
    }

    /**
     * @SWG\Definition(
     *      definition="Map",
     *      @SWG\Property(property="map", type="string", example="http://example.com/map.png"),
     *      @SWG\Property(property="info", type="array", @SWG\Items(
     *         type="object",
     *         @SWG\Property(property="totalArea", type="string", example="10 соток"),
     *         @SWG\Property(property="totalPlants", type="integer", example=78),
     *         @SWG\Property(property="age", type="string", example="3 года"),
     *         @SWG\Property(property="deciduousCount", type="integer", example=27),
     *         @SWG\Property(property="pinesCount", type="integer", example=13),
     *         @SWG\Property(property="lawnArea", type="string", example="5 соток"),
     *     )),
     *      @SWG\Property(property="points", type="array", @SWG\Items(
     *         type="object",
     *         @SWG\Property(property="id", type="integer", example="1"),
     *         @SWG\Property(property="top", type="float", example="11.83"),
     *         @SWG\Property(property="left", type="float", example="9.43"),
     *         @SWG\Property(property="image", type="string", example="http://static.imperial.loc/gardens/plod2.jpg"),
     *         @SWG\Property(property="radius", type="float", example="15.56"),
     *         @SWG\Property(property="info", type="array", @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Брусника обыкновенная"),
     *              @SWG\Property(property="age", type="string", example="3 года"),
     *              @SWG\Property(property="date", type="string", example="1584870697"),
     *         ))
     *     )),
     * )
     */
}