<?php


namespace api\controllers;


use core\useCases\api\Chat\ChatService;
use core\useCases\api\DealApiService;
use core\useCases\manage\UserManageService;
use Yii;
use yii\web\Controller;

class FromBitrixController extends Controller
{
    /**
     * @var ChatService
     */
    private $chatService;
    /**
     * @var DealApiService
     */
    private $dealApiService;
    /**
     * @var UserManageService
     */
    private $userManageService;

    public function __construct($id, $module,
                                ChatService $chatService,
                                DealApiService $dealApiService,
                                UserManageService $userManageService,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->chatService = $chatService;
        $this->dealApiService = $dealApiService;
        $this->userManageService = $userManageService;
    }

    public function actionMessage()
    {
        $this->chatService->messageFromBitrix(Yii::$app->request->post());
    }

    public function actionDealStatus()
    {
        $this->dealApiService->statusFromBitrix(Yii::$app->request->post());
    }

    public function actionManagerChange()
    {
        $this->userManageService->managerFromBitrix(Yii::$app->request->post());
    }
}