<?php


namespace api\controllers;


use core\entities\Dispatch;
use core\entities\Land\Land;
use core\entities\News;
use yii\web\Controller;

class WebviewController extends Controller
{
    public function actionNews($id)
    {
        $content = News::findOne($id);
        return $this->render('index', ['text' => $content->text ?? '']);
    }

    public function actionDispatch($id)
    {
        $content = Dispatch::findOne($id);
        return $this->render('index', ['text' => $content->text ?? '']);
    }

    public function actionLandAudit($id)
    {
        $content = Land::findOne($id);
        return $this->render('index', ['text' => $content->audit ?? '']);
    }

    public function actionLandRec($id)
    {
        $content = Land::findOne($id);
        return $this->render('index', ['text' => $content->rec ?? '']);
    }
}