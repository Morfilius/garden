<?php


namespace api\controllers;


use core\entities\User\User;
use core\forms\auth\Rest\LoginForm;
use core\traits\RestControllerTrait;
use Yii;
use yii\rest\Controller;

class AuthController extends Controller
{
    use RestControllerTrait;

    /**
     * @SWG\Post(
     *     path="/auth/login",
     *     tags={"Auth"},
     *     description="Аутентификация",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="device_id - это device_id из firebase",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="username", type="string", example="user"),
     *              @SWG\Property(property="password", type="string", example="User123#"),
     *              @SWG\Property(property="device_id", type="string", example="ios"),
     *          )
     *
     *      ),
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *         @SWG\Schema(ref="#/definitions/Auth")
     *     ),
     *
     * )
     */

    public function actionLogin()
    {
        $form = new LoginForm();
        if ($form->load(Yii::$app->request->bodyParams, '') && $form->validate()) {
            return [
                'token' => User::restLogin(
                    $form->username,
                    $form->password,
                    $form->device_id
                )
            ];
        }
        return $form;
    }

    /**
     * @SWG\Definition(
     *      definition="Auth",
     *      @SWG\Property(property="token", type="string", example="BLL5QXPJgDBjWIFjhzReMG4ADuBXQWP_")
     * )
     */
}