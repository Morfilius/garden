<?php


namespace api\controllers;


use core\useCases\api\DealApiService;
use Yii;
use yii\filters\auth\HttpBearerAuth;

class DealController extends BaseApiController
{
    /**
     * @var DealApiService
     */
    private $dealApiService;

    public function __construct($id, $module, DealApiService $dealApiService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->dealApiService = $dealApiService;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['authMethods'] = [
            HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    /**
     * @SWG\Get(
     *     path="/deal/list",
     *     tags={"Deal"},
     *     description="Список заказов, 'дорогой' запрос",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Список с пагинацией",
     *         @SWG\Schema(ref="#/definitions/DealList")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionList()
    {
        return $this->dealApiService->showList(Yii::$app->user->id);
    }


    /**
     * @SWG\Definition(
     *      definition="DealList",
     *      @SWG\Property(property="items", type="array",  @SWG\Items(type="object",
     *          @SWG\Property(property="title", type="string", example="Туя со скидкой 20%"),
     *          @SWG\Property(property="stage", type="string", example="Доставка"),
     *          @SWG\Property(property="date", type="string", example="05.05.2020"),
     *          @SWG\Property(property="price", type="string", example="1000.00 руб."),
     *      )
     *  ),
     *  @SWG\Property(property="_links", type="object",
     *          @SWG\Property(property="self", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=2")),
     *          @SWG\Property(property="first", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="prev", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=1")),
     *          @SWG\Property(property="next", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=3")),
     *          @SWG\Property(property="last", type="object", @SWG\Property(property="href", type="string", example="http://example.com/pages/list?page=4")),
     *      ),
     *  @SWG\Property(property="_meta", type="object",
     *          @SWG\Property(property="totalCount", type="integer", example=4),
     *          @SWG\Property(property="pageCount", type="integer", example=4),
     *          @SWG\Property(property="currentPage", type="integer", example=2),
     *          @SWG\Property(property="perPage", type="integer", example=1),
     *      )
     * )
     */
}