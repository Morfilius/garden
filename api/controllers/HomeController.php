<?php


namespace api\controllers;


use core\entities\Banners;
use core\entities\Calendar\Bitrix24\Events;
use core\entities\Notification\Notification;
use core\entities\User\User;
use core\readRepositories\NotificationReadRepository;
use DateTime;
use Yii;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;

class HomeController extends BaseApiController
{
    /**
     * @var NotificationReadRepository
     */
    private $notificationReadRepository;

    public function __construct($id, $module, NotificationReadRepository $notificationReadRepository, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->notificationReadRepository = $notificationReadRepository;
    }

    /**
     * @SWG\Get(
     *     path="/home",
     *     tags={"Homepage"},
     *     description="Данные для главной страницы",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="",
     *         @SWG\Schema(ref="#/definitions/HomeData")
     *     ),
     *     security={{"Bearer": {}}}
     *
     * )
     */

    public function actionIndex()
    {
        /** @var User $user*/
        $user = Yii::$app->user->identity->getUser();
        $manager = $user->getManager();
        $land = $user->getLandOrFail();
        $calendar = new Events($land->calendar_id);
        $eventList = $calendar->getListPerMonth($calendar->getListNextDates());

        if (empty($land->map)) {
            throw new NotFoundHttpException('Land map not found.');
        }

        $leftBanner = Banners::findOne(['number' => 1]);
        $rightBanner = Banners::findOne(['number' => 2]);

        return [
            'banners' => [
                'left' => [
                    'text' => $leftBanner->text,
                    'image' => $leftBanner->getThumbUploadUrl('image', 'home')
                ],
                'right' => [
                    'text' => $rightBanner->text,
                    'image' => $rightBanner->getThumbUploadUrl('image', 'home')
                ]
            ],
            'land' => [
                'image' => $land->map->getThumbUploadUrl('image', 'report'),
                'name' => $land->title,
                'area' => $land->getTotalAreaWithWord(),
                'plants_count' => $land->getTotalPlants(),
                'age' => $land->getAgeWithWord(),
            ],
            'manager' => [
                'bitrix_id' => 1,
                'image' => $manager->image,
                'name' => $manager->getFullName(),
                'staff' => 'Ваш персональный менеджер'
            ],
            'calendar' => array_reduce($eventList, function ($result, $month) {
                $current = reset($month);
                $currentDate = new DateTime($current[0]['DATE_FROM']);
                $result[] = [
                    'month' => (int)$currentDate->format('m'),
                    'days' => array_reduce($month, function ($res, $day) {
                        $res[] = (int)(new DateTime($day[0]['DATE_FROM']))->format('d');
                        return $res;
                    }, []),
                    'works' => array_filter(array_reduce($month, function ($res, $day) {
                        return array_map(function ($event) {
                            $data = explode('|', $event['NAME']);
                            if (isset($data[1])) {
                                return (new DateTime($event['DATE_FROM']))->format('d.m.Y') . ' - ' . StringHelper::truncate($data[1], 27);
                            }

                            return false;
                        }, $day);
                    }, []))
                ];
                return $result;
            }, []),
            'notifications' => array_map(function (Notification $notification) {
                return $notification->text;
            }, $this->notificationReadRepository->showUnreadListLimit(Yii::$app->user->id))
        ];
    }

    /**
     * @SWG\Definition(
     *      definition="HomeData",
     *  @SWG\Property(property="land", type="object",
     *          @SWG\Property(property="image", type="string", example="http://static.imperial.loc/cache/map/report-5eb408302a091.png"),
     *          @SWG\Property(property="name", type="string", example="Название участка"),
     *          @SWG\Property(property="area", type="string", example="23 сотки"),
     *          @SWG\Property(property="plants_count", type="string", example=44),
     *          @SWG\Property(property="age", type="string", example="14 лет"),
     *      ),
     *  @SWG\Property(property="manager", type="object",
     *          @SWG\Property(property="bitrix_id", type="integer", example=1),
     *          @SWG\Property(property="image", type="string", example="http://static.imperial.loc/default.png"),
     *          @SWG\Property(property="name", type="string", example="Анастасия Константинопольская"),
     *          @SWG\Property(property="staff", type="string", example="Ваш персональный менеджер"),
     *      ),
     *  @SWG\Property(property="calendar", type="array", @SWG\Items(
     *         type="object",
     *              @SWG\Property(property="month", type="integer", example=6),
     *              @SWG\Property(property="days", type="array",
     *                   @SWG\Items(
     *                          type="integer",
     *                          enum={10}
     *                      )
     *                  ),
     *              @SWG\Property(property="works", type="array",
     *                   @SWG\Items(
     *                          type="string",
     *                          enum={"23.03.2020 — Удобрение газона, полив, обрезка..."}
     *                      )
     *                  ),
     *               ),
     *          ),
     *   @SWG\Property(property="notifications", type="array", @SWG\Items(
     *                          type="string",
     *                          enum={"У вас новое сообщение в чате"}
     *                      )
     *          ),
     *     ),
     *  ),
     * )
     */
}