#!/usr/bin/env bash

source /app/vagrant/provision/common.sh

#== Provision script ==

info "Provision-script user: `whoami`"

info "Restart web-stack"
rm /etc/nginx/sites-enabled/default
service php7.2-fpm restart
service nginx restart
service mysql restart
systemctl --system daemon-reload
systemctl start xvfb
systemctl start selenium